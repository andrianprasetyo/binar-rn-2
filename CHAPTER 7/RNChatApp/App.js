import React from 'react';

import {Provider} from 'react-redux';

import {persistor, store} from './src/store';

import Router from './src/router';

import {PersistGate} from 'redux-persist/integration/react';

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Router />
      </PersistGate>
    </Provider>
  );
};

export default App;
