import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  user: {},
  contacts: [],
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser: (state, action) => {
      state.user = action.payload;
    },
    clearUser: state => {
      state.user = {};
    },
    setContacts: (state, action) => {
      state.contacts = action.payload;
    },
    clearContacts: state => {
      state.contacts = [];
    },
    clearAllUserData: state => {
      state.user = {};
      state.contacts = [];
    },
  },
});

export const {
  setUser,
  clearUser,
  setContacts,
  clearContacts,
  clearAllUserData,
} = userSlice.actions;

export default userSlice.reducer;
