import React from 'react';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import {Dashboard, Contacts, Settings} from '../screens';

import {BottomTabNavigation} from '../components';

const Tab = createBottomTabNavigator();

const BottomTab = () => {
  return (
    <Tab.Navigator
      tabBar={props => <BottomTabNavigation {...props} />}
      screenOptions={{
        headerShown: false,
      }}>
      <Tab.Screen name="Dashboard" component={Dashboard} />
      <Tab.Screen name="Contacts" component={Contacts} />
      <Tab.Screen name="Settings" component={Settings} />
    </Tab.Navigator>
  );
};

export default BottomTab;
