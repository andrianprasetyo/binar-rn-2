import React from 'react';

import {StyleSheet} from 'react-native';

import Toast, {BaseToast, ErrorToast} from 'react-native-toast-message';
import {Colors} from './Colors';
import {Fonts} from './Fonts';

export const toastConfig = {
  /*
      Overwrite 'success' type,
      by modifying the existing `BaseToast` component
    */
  success: props => (
    <BaseToast
      {...props}
      style={styles.successStyle}
      contentContainerStyle={styles.contentCard}
      text1Style={styles.contentText}
      text2Style={styles.contentText}
    />
  ),
  /*
      Overwrite 'error' type,
      by modifying the existing `ErrorToast` component
    */
  error: props => (
    <ErrorToast
      {...props}
      style={styles.errorStyle}
      contentContainerStyle={styles.contentCard}
      text1Style={styles.contentText}
      text2Style={styles.contentText}
    />
  ),
};

const styles = StyleSheet.create({
  successStyle: {
    borderLeftColor: Colors.FILL,
    width: '95%',
    borderRadius: 12,
  },
  errorStyle: {
    borderLeftColor: Colors.ERROR,
    width: '95%',
    borderRadius: 12,
  },
  contentCard: {
    paddingHorizontal: 15,
  },
  contentText: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    color: Colors.TEXT,
    fontSize: 14,
  },
});

export const showSuccess = ({header = 'Success', message}) => {
  Toast.show({
    type: 'success',
    text1: header ? header : 'Success',
    text2: message,
  });
};

export const showError = ({header = 'Error', message}) => {
  Toast.show({
    type: 'error',
    text1: header,
    text2: message,
  });
};
