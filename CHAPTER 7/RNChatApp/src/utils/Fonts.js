export const Fonts = {
  PRIMARY: {
    LIGHT: 'Montserrat-Light',
    REGULAR: 'Montserrat-Regular',
    MEDIUM: 'Montserrat-Medium',
    SEMIBOLD: 'Montserrat-SemiBold',
    BOLD: 'Montserrat-Bold',
    BLACK: 'Montserrat-Black',
  },
};
