import IconBack from './ic-back.svg';
import IconBackWhite from './ic-back-white.svg';
import IconBtnSend from './ic-btn-send.svg';
import IconChatTabActive from './ic-chat-tab-active.svg';
import IconChatTab from './ic-chat-tab.svg';
import IconClose from './ic-close.svg';
import IconGoogle from './ic-google.svg';
import IconNotification from './ic-notification.svg';
import IconPeopleTabActive from './ic-people-tab-active.svg';
import IconPeopleTab from './ic-people-tab.svg';
import IconPhoneTabActive from './ic-phone-tab-active.svg';
import IconPhoneTab from './ic-phone-tab.svg';
import IconSearch from './ic-search.svg';
import IconSend from './ic-send.svg';
import IconSettingsTabActive from './ic-settings-tab-active.svg';
import IconSettingsTab from './ic-settings-tab.svg';
import IconEye from './ic-eye.svg';
import IconEyeActive from './ic-eye-active.svg';
import IconLogout from './ic-logout.svg';
import IconEdit from './ic-edit.svg';
import IconShare from './ic-share.svg';
import LogoChatApp from './logo-chat-app.svg';

export {
  IconEdit,
  IconShare,
  IconLogout,
  IconBack,
  IconBackWhite,
  IconBtnSend,
  IconChatTabActive,
  IconChatTab,
  IconClose,
  IconGoogle,
  IconNotification,
  IconPeopleTabActive,
  IconPeopleTab,
  IconPhoneTab,
  IconPhoneTabActive,
  IconSearch,
  IconSend,
  IconSettingsTab,
  IconSettingsTabActive,
  IconEye,
  IconEyeActive,
  LogoChatApp,
};
