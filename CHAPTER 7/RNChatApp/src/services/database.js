import {firebase} from '@react-native-firebase/database';

const URL =
  'https://rnchatapp-ikbal-default-rtdb.asia-southeast1.firebasedatabase.app';

const database = firebase.app().database(URL);

export default database;
