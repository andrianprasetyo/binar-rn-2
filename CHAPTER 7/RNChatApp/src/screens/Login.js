import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  SafeAreaView,
  StatusBar,
  View,
  TouchableOpacity,
} from 'react-native';

import auth from '@react-native-firebase/auth';

import database from '../services/database';

import {IconGoogle} from '../assets';

import {BaseButton, BaseInput, Gap} from '../components';

import {Colors, Fonts, navigate, showError, useForm} from '../utils';

import {useDispatch} from 'react-redux';
import {setUser} from '../store/userSlice';
import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';

const Login = props => {
  const {navigation} = props;

  const dispatch = useDispatch();

  const [isLoading, setIsLoading] = useState(false);

  const [formValue, setFormValue] = useForm({
    email: '',
    password: '',
  });

  const handleLogin = async () => {
    if (formValue.email === '' || formValue.password === '') {
      return showError({
        header: 'Invalid Form Value',
        message: 'Please complete all the form',
      });
    }

    setIsLoading(true);

    try {
      // Response from Auth Firebase
      const response = await auth().signInWithEmailAndPassword(
        formValue.email,
        formValue.password,
      );

      if (response.user) {
        const {uid} = response.user;

        await database
          .ref('/users/')
          .orderByChild('id')
          .equalTo(uid)
          .once('value')
          .then(snapshot => {
            setIsLoading(false);

            // Data from Realtime Database
            const data = Object.values(snapshot.val())[0];

            dispatch(setUser(data));

            navigate('Main', {screen: 'Dashboard'});
          });
      }
    } catch (error) {
      setIsLoading(false);
      showError({header: 'Error', message: error?.code});
    }
  };

  useEffect(() => {
    GoogleSignin.configure({
      webClientId:
        '1064917413691-ijkm5glqso377uqju03vs3d7m90o4smg.apps.googleusercontent.com',
      androidClientId:
        '1064917413691-dapb3cqp6ngo8bdhqjsv088u3ap4b8d8.apps.googleusercontent.com',
    });
  }, []);

  const handleGoogleLogin = async () => {
    try {
      await GoogleSignin.hasPlayServices();

      const {idToken} = await GoogleSignin.signIn();

      console.log(idToken);

      // Create a Google credential with the token
      const googleCredential = auth.GoogleAuthProvider.credential(idToken);

      // Sign-in the user with the credential
      return auth().signInWithCredential(googleCredential);
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        showError({
          message: 'Seems Like your play services not available or outdated',
        });
      } else {
        // some other error happened
        showError({
          message: 'Whoops something has wrong',
        });
        console.log(error);
      }
    }
  };

  return (
    <SafeAreaView style={styles.screen}>
      <StatusBar backgroundColor={Colors.FILL} />
      <Gap height={80} />
      <Text style={styles.headingTitle}>Log in</Text>
      <Gap height={22} />
      <BaseInput
        label="Email"
        placeholder="Enter Email here"
        value={formValue.email}
        onChangeText={value => setFormValue('email', value)}
      />
      <Gap height={24} />
      <BaseInput
        label="Password"
        type="password"
        placeholder="Enter Password here"
        value={formValue.password}
        onChangeText={value => setFormValue('password', value)}
      />
      <Gap height={48} />
      <BaseButton
        isLoading={isLoading}
        disable={isLoading}
        title="Login"
        style={styles.btnLogin}
        onPress={handleLogin}
      />
      <Gap height={48} />
      {/* <View style={styles.dividerContainer}>
        <View style={styles.divider} />
        <View>
          <Text style={styles.socialSub}>Or Sign in with</Text>
        </View>
        <View style={styles.divider} />
      </View>
      <Gap height={48} />
      <View style={styles.socialContainer}>
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.socialBtn}
          onPress={handleGoogleLogin}>
          <IconGoogle />
        </TouchableOpacity>
      </View>
      <Gap height={48} /> */}
      <View style={styles.registerContainer}>
        <Text style={styles.registerText}>Don't have an account? </Text>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => {
            navigation.navigate('Register');
          }}>
          <Text style={styles.registerText}>Register</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default Login;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
    padding: 24,
  },
  headingTitle: {
    fontSize: 32,
    color: Colors.TEXT,
    fontFamily: Fonts.PRIMARY.BOLD,
  },
  socialSub: {
    width: 150,
    fontSize: 12,
    textAlign: 'center',
    color: Colors.SUB_TEXT,
    fontFamily: Fonts.PRIMARY.MEDIUM,
  },
  dividerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  divider: {
    flex: 1,
    height: 1,
    backgroundColor: Colors.GREY_MEDIUM,
  },
  btnLogin: {
    borderRadius: 100,
    shadowColor: Colors.SUB_TEXT,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.22,
    shadowRadius: 2,
    elevation: 2,
  },
  socialContainer: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  socialBtn: {
    width: 64,
    height: 64,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 16,
    backgroundColor: Colors.BACKGROUND,
    shadowColor: Colors.SUB_TEXT,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.22,
    shadowRadius: 2,
    elevation: 2,
  },
  registerContainer: {
    width: '100%',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  registerText: {
    fontSize: 12,
    color: Colors.SUB_TEXT,
    fontFamily: Fonts.PRIMARY.MEDIUM,
  },
});
