import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  StatusBar,
  TextInput,
  ScrollView,
} from 'react-native';

import {IconBackWhite, IconBtnSend} from '../assets';
import {Gap, MessageItem} from '../components';
import {Colors, Fonts, getChatTime, setDateChat, showError} from '../utils';

import {useSelector} from 'react-redux';

import {useIsFocused} from '@react-navigation/native';

import database from '../services/database';

const Chat = props => {
  const {navigation, route} = props;

  const {receiverData} = route.params;

  let isFocused = useIsFocused();

  const userState = useSelector(state => state.user.user);

  const [chatMessage, setChatMessage] = useState('');

  const [listChat, setListChat] = useState([]);

  const scrollViewRef = useRef();

  useEffect(() => {
    let chatID = `${receiverData.id}_${userState.id}`;

    let refDatabase = `/chatting/${chatID}/allChat/`;

    const onValueChange = database.ref(refDatabase).on('value', snapshot => {
      // console.log(snapshot.val());
      if (snapshot.val()) {
        const data = snapshot.val();
        const allChatData = [];

        // LOOP Every Chat Date
        Object.keys(data).map(key => {
          const dataChat = data[key];
          const itemChat = [];

          // LOOP Every ID/REFERENCE CHAT
          Object.keys(dataChat).map(item => {
            // Push Every Item Chat
            itemChat.push({
              id: item,
              data: dataChat[item],
            });
          });

          // Push All Data Chat from Item Chat
          allChatData.push({
            id: key,
            data: itemChat,
          });
        });
        setListChat(allChatData);
      }
    });

    return () => database.ref(refDatabase).off('value', onValueChange);
  }, [receiverData.id, receiverData.authorRoom, userState.id, isFocused]);

  const sendChat = async () => {
    if (chatMessage === '') {
      return;
    }

    const today = new Date();

    const payload = {
      sendBy: userState.id,
      chatDate: setDateChat(today),
      chatTime: getChatTime(today),
      chatMessage: chatMessage,
      authorRoom: userState.id,
    };

    let chatID = `${receiverData.id}_${userState.id}`;

    const refDatabase = `/chatting/${chatID}/allChat/${setDateChat(today)}`;
    const urlChatUser = `/messages/${userState.id}/${chatID}`;
    const urlChatReceiver = `/messages/${receiverData.id}/${chatID}`;

    const dataHistoryChatForUser = {
      lastChatMessage: chatMessage,
      lastChatDate: setDateChat(today),
      idPartner: receiverData.id,
      authorRoom: userState.id,
    };

    const dataHistoryChatForReceiver = {
      lastChatMessage: chatMessage,
      lastChatDate: setDateChat(today),
      idPartner: userState.id,
      authorRoom: userState.id,
    };

    try {
      await database
        .ref(refDatabase)
        .push(payload)
        .then(() => {
          setChatMessage('');

          // set history for user
          Promise.all([
            database.ref(urlChatUser).set(dataHistoryChatForUser),
            database.ref(urlChatReceiver).set(dataHistoryChatForReceiver),
          ]);
        });
    } catch (error) {
      showError({message: error?.message});
    }
  };

  return (
    <SafeAreaView style={styles.screen}>
      <StatusBar backgroundColor={Colors.FILL} />
      <View style={styles.headerContainer}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => navigation.goBack()}>
          <IconBackWhite />
        </TouchableOpacity>
        <Gap height={28} />
        <View style={styles.userContainer}>
          <View style={styles.userAvatar}>
            <Text style={styles.userAvatarText}>
              {receiverData.name.toUpperCase().split('')[0]}
            </Text>
          </View>
          <Gap width={16} />
          <Text style={styles.userName}>{receiverData.name}</Text>
        </View>
      </View>
      <View style={styles.card}>
        <ScrollView
          // ref={scrollViewRef}
          // onContentSizeChange={() =>
          //   scrollViewRef.current.scrollToTop({animated: true})
          // }
          showsVerticalScrollIndicator={false}>
          {listChat?.map(chat => {
            return chat.data.map(item => {
              const isSender = item.data.sendBy === userState.id;
              return (
                <MessageItem
                  key={item.id}
                  sender={isSender}
                  message={item.data.chatMessage}
                  time={item.data.chatTime}
                />
              );
            });
          })}
        </ScrollView>
        <Gap height={24} />
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputMessage}
            placeholder="type your message..."
            multiline={true}
            value={chatMessage}
            onChangeText={value => setChatMessage(value)}
          />
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.btnSend}
            onPress={sendChat}>
            <IconBtnSend />
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Chat;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Colors.FILL,
  },
  headerContainer: {
    padding: 24,
    height: '25%',
  },
  userContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  userName: {
    fontSize: 18,
    color: Colors.WHITE,
    fontFamily: Fonts.PRIMARY.BOLD,
  },
  userAvatar: {
    width: 55,
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.GREY_MEDIUM,
    borderRadius: 100,
  },
  userAvatarText: {
    fontSize: 16,
    color: Colors.GREY_DARK,
    fontFamily: Fonts.PRIMARY.SEMIBOLD,
  },
  card: {
    flex: 1,
    backgroundColor: Colors.WHITE,
    marginTop: -25,
    borderTopLeftRadius: 32,
    borderTopRightRadius: 32,
    padding: 24,
  },
  listMessageCard: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
  },
  inputContainer: {
    width: '100%',
    position: 'relative',
    backgroundColor: Colors.GREY_LIGHT,
    borderRadius: 100,
  },
  inputMessage: {
    fontSize: 11,
    fontFamily: Fonts.PRIMARY.REGULAR,
    borderColor: Colors.WHITE,
    color: Colors.TEXT,
    paddingLeft: 15,
    paddingRight: 56,
    paddingVertical: 15,
  },
  btnSend: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
  },
});
