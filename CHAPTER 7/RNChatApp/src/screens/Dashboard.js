import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  FlatList,
} from 'react-native';

import {Gap} from '../components';

import database from '../services/database';

import {Colors, Fonts, setDateChat, showSuccess} from '../utils';

import {useSelector, useDispatch} from 'react-redux';
import {clearUser} from '../store/userSlice';
import {useIsFocused} from '@react-navigation/native';

const Dashboard = props => {
  const {navigation} = props;

  const isFocused = useIsFocused();
  const dispatch = useDispatch();

  const userState = useSelector(state => state.user.user);

  const [historyChat, setHistoryChat] = useState([]);

  useEffect(() => {
    const URLHistory = `/messages/${userState.id}`;

    const onValueChange = database
      .ref(URLHistory)
      .on('value', async snapshot => {
        if (snapshot.val()) {
          const snapshotData = snapshot.val();

          const data = [];

          const promises = await Object.keys(snapshotData).map(async key => {
            const URLIdPartner = `/users/${snapshotData[key].idPartner}`;

            const detailPartner = await database
              .ref(URLIdPartner)
              .once('value');

            data.push({
              id: key,
              dataPartner: detailPartner.val(),
              dataHistory: snapshotData[key],
            });
          });

          await Promise.all(promises);

          setHistoryChat(data);
        }
      });

    return () => database.ref(URLHistory).off('value', onValueChange);
  }, [userState.id, isFocused]);

  const renderItem = ({item}) => (
    <>
      <Gap height={24} />
      <TouchableOpacity
        activeOpacity={0.7}
        style={styles.chatContainer}
        onPress={() => {
          navigation.navigate('Chat', {
            receiverData: {
              id: item.dataPartner.id,
              name: item.dataPartner.name,
              authorRoom: item.dataHistory.authorRoom,
            },
          });
        }}>
        <View style={styles.contentContainer}>
          <View style={styles.userAvatar}>
            <Text style={styles.userAvatarText}>
              {item.dataPartner.name.toUpperCase().split('')[0]}
            </Text>
          </View>
          <Gap width={16} />
          <View>
            <Text
              ellipsizeMode="tail"
              numberOfLines={1}
              style={styles.userTitle}>
              {item.dataPartner.name}
            </Text>
            <Gap height={8} />
            <Text
              ellipsizeMode="tail"
              numberOfLines={1}
              style={styles.userMessage}>
              {item.dataHistory.lastChatMessage}
            </Text>
          </View>
        </View>
        <Text style={styles.lastSent}>{item.dataHistory.lastChatDate}</Text>
      </TouchableOpacity>
      <Gap height={24} />
      <View style={styles.divider} />
    </>
  );

  return (
    <SafeAreaView style={styles.screen}>
      <StatusBar backgroundColor={Colors.FILL} />
      <View style={styles.headerContainer}>
        <Text style={styles.headingTitle}>Message</Text>
      </View>
      <Gap height={28} />
      {historyChat.length > 0 ? (
        <FlatList
          showsVerticalScrollIndicator={false}
          keyExtractor={(item, index) => index.toString()}
          data={historyChat}
          renderItem={renderItem}
        />
      ) : (
        <View style={styles.emptyContainer}>
          <Text style={styles.emptyText}>No Message</Text>
        </View>
      )}
    </SafeAreaView>
  );
};

export default Dashboard;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
    padding: 24,
  },
  headingTitle: {
    fontSize: 32,
    color: Colors.TEXT,
    fontFamily: Fonts.PRIMARY.BOLD,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  signOutText: {
    fontSize: 12,
    color: Colors.WHITE,
    fontFamily: Fonts.PRIMARY.BOLD,
    paddingVertical: 12,
    paddingHorizontal: 16,
    backgroundColor: Colors.FILL,
    borderRadius: 100,
  },
  emptyContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyText: {
    fontSize: 14,
    color: Colors.GREY_DARK,
    fontFamily: Fonts.PRIMARY.MEDIUM,
  },
  chatContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  contentContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    maxWidth: '75%',
  },
  userAvatar: {
    width: 55,
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.GREY_MEDIUM,
    borderRadius: 100,
  },
  userAvatarText: {
    fontSize: 16,
    color: Colors.GREY_DARK,
    fontFamily: Fonts.PRIMARY.SEMIBOLD,
  },
  userTitle: {
    fontSize: 16,
    color: Colors.GREY_DARK,
    fontFamily: Fonts.PRIMARY.SEMIBOLD,
  },
  userMessage: {
    fontSize: 14,
    color: Colors.GREY_MEDIUM,
    fontFamily: Fonts.PRIMARY.SEMIBOLD,
    maxWidth: '80%',
  },
  lastSent: {
    fontSize: 11,
    color: Colors.GREY_MEDIUM,
    fontFamily: Fonts.PRIMARY.MEDIUM,
  },
  divider: {
    height: 1,
    backgroundColor: Colors.GREY_MEDIUM,
  },
});
