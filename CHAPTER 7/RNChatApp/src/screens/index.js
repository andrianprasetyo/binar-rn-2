import Splash from './Splash';
import Login from './Login';
import Register from './Register';
import Dashboard from './Dashboard';
import Contacts from './Contacts';
import Settings from './Settings';
import Chat from './Chat';

export {Splash, Login, Register, Dashboard, Chat, Contacts, Settings};
