import React, {useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';

import {LogoChatApp} from '../assets';

import {Colors, Fonts} from '../utils';

import {useSelector} from 'react-redux';

const Splash = props => {
  const {navigation} = props;

  const userState = useSelector(state => state.user.user);

  useEffect(() => {
    setTimeout(() => {
      if (userState.hasOwnProperty('id')) {
        navigation.reset({index: 0, routes: [{name: 'Main'}]});
      } else {
        navigation.reset({index: 0, routes: [{name: 'Login'}]});
      }
    }, 2000);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View style={styles.screen}>
      <View style={styles.container}>
        <LogoChatApp />
      </View>
      <Text style={styles.title}>RNChatApp</Text>
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    paddingVertical: 24,
    textAlign: 'center',
    fontSize: 16,
    color: Colors.TEXT,
    fontFamily: Fonts.PRIMARY.LIGHT,
  },
});
