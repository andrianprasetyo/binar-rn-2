import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  Share,
  TextInput,
} from 'react-native';
import {Gap} from '../components';
import {Colors, Fonts, showError, showSuccess} from '../utils';

import {useSelector, useDispatch} from 'react-redux';
import {IconLogout, IconEdit, IconShare} from '../assets';
import {clearAllUserData, setUser} from '../store/userSlice';
import database from '../services/database';

const Settings = props => {
  const {navigation} = props;
  const dispatch = useDispatch();
  const userState = useSelector(state => state.user.user);

  const [bioInput, setBioInput] = useState(userState.bio);

  const [isOnEditing, setIsOnEditing] = useState(false);

  const handleShare = () => {
    const shareOptions = {
      title: `Chat with ${userState.name}`,
      message: `Hey, you can chat me on app. here my ID: ${userState.id}`,
      subject: 'Chat',
    };

    Share.share(shareOptions);
  };

  const handleChangeInputBio = event => {
    setBioInput(event);
  };

  const toggleBio = () => {
    setIsOnEditing(!isOnEditing);
  };

  const sendDataBio = () => {
    database
      .ref(`/users/${userState.id}`)
      .update({bio: bioInput})
      .then(() => {
        console.log('Bio Updated');
        dispatch(setUser({...userState, bio: bioInput}));
        toggleBio();
      })
      .catch(err => showError({message: err.message}));
  };

  const handleLogout = () => {
    dispatch(clearAllUserData());
    showSuccess({header: 'Sign Out Success', message: 'You are signed out'});
    navigation.replace('Login');
  };

  return (
    <SafeAreaView style={styles.screen}>
      <StatusBar backgroundColor={Colors.FILL} />
      <View style={styles.headerContainer}>
        <View style={styles.userAvatar}>
          <Text style={styles.userAvatarText}>
            {userState.name.toUpperCase().split('')[0]}
          </Text>
        </View>
      </View>
      <View style={styles.contentCard}>
        <View style={styles.userHeaderContainer}>
          <Text style={styles.userName}>{userState.name}</Text>
          <Gap height={18} />
          <Text style={styles.userEmail}>{userState.email}</Text>
        </View>
        <View style={styles.listMenuContainer}>
          <TouchableOpacity
            style={styles.listMenuWrapper}
            activeOpacity={0.7}
            onPress={() => toggleBio()}>
            <IconEdit />
            <Gap width={12} />
            {isOnEditing ? (
              <TextInput
                style={styles.textInputBio}
                value={bioInput}
                multiline={true}
                onChangeText={handleChangeInputBio}
                onSubmitEditing={sendDataBio}
                onBlur={() => toggleBio()}
              />
            ) : (
              <Text style={styles.listMenu}>
                {userState.bio && userState.bio !== ''
                  ? `Bio - ${userState.bio}`
                  : 'Add Bio'}
              </Text>
            )}
          </TouchableOpacity>
          <Gap height={24} />
          <View style={styles.divider} />
          <Gap height={24} />
          <TouchableOpacity
            style={styles.listMenuWrapper}
            activeOpacity={0.7}
            onPress={handleShare}>
            <IconShare />
            <Gap width={12} />
            <Text style={styles.listMenu}>Share Profile</Text>
          </TouchableOpacity>
          <Gap height={24} />
          <View style={styles.divider} />
          <Gap height={24} />
          <TouchableOpacity
            style={styles.listMenuWrapper}
            activeOpacity={0.7}
            onPress={handleLogout}>
            <IconLogout />
            <Gap width={12} />
            <Text style={styles.listMenu}>Logout</Text>
          </TouchableOpacity>
          <Gap height={24} />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Settings;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
  },
  userAvatar: {
    width: 120,
    height: 120,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.GREY_MEDIUM,
    borderRadius: 100,
  },
  userAvatarText: {
    fontSize: 32,
    color: Colors.GREY_DARK,
    fontFamily: Fonts.PRIMARY.SEMIBOLD,
  },
  contentCard: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
    marginTop: -25,
    borderTopLeftRadius: 32,
    borderTopRightRadius: 32,
    padding: 24,
  },
  userHeaderContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  userName: {
    textAlign: 'center',
    fontSize: 32,
    color: Colors.TEXT,
    fontFamily: Fonts.PRIMARY.BOLD,
  },
  userEmail: {
    textAlign: 'center',
    fontSize: 16,
    color: Colors.SUB_TEXT,
    fontFamily: Fonts.PRIMARY.REGULAR,
  },
  headerContainer: {
    padding: 24,
    height: '40%',
    backgroundColor: Colors.FILL,
    justifyContent: 'center',
    alignItems: 'center',
  },
  listMenuContainer: {
    flex: 2,
    justifyContent: 'center',
  },
  listMenuWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
  },
  listMenu: {
    fontSize: 16,
    color: Colors.GREY_DARK,
    fontFamily: Fonts.PRIMARY.SEMIBOLD,
    overflow: 'hidden',
  },
  textInputBio: {
    fontSize: 16,
    color: Colors.GREY_DARK,
    fontFamily: Fonts.PRIMARY.SEMIBOLD,
    width: '80%',
  },
  divider: {
    height: 1,
    backgroundColor: Colors.GREY_MEDIUM,
  },
});
