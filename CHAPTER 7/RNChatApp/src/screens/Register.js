import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  SafeAreaView,
  StatusBar,
  View,
  TouchableOpacity,
} from 'react-native';

import auth from '@react-native-firebase/auth';

import database from '../services/database';

import {BaseButton, BaseInput, Gap} from '../components';

import {Colors, Fonts, showError, showSuccess, useForm} from '../utils';

const Register = props => {
  const {navigation} = props;

  const [formValue, setFormValue] = useForm({
    name: '',
    email: '',
    password: '',
  });

  const [isLoading, setIsLoading] = useState(false);

  const handleRegister = async () => {
    if (
      formValue.name === '' ||
      formValue.email === '' ||
      formValue.password === ''
    ) {
      return showError({
        header: 'Invalid Form Value',
        message: 'Please complete all the form',
      });
    }

    setIsLoading(true);

    try {
      const response = await auth().createUserWithEmailAndPassword(
        formValue.email,
        formValue.password,
      );

      if (response.user) {
        const {uid} = response.user;

        await database
          .ref('/users/' + uid)
          .set({id: uid, email: formValue.email, name: formValue.name})
          .then(() => {
            setIsLoading(false);
            showSuccess({
              header: 'Register Success',
              message: 'Now you can login with your id',
            });
            navigation.navigate('Login');
          });
      }
    } catch (error) {
      setIsLoading(false);
      showError({message: error?.code});
    }
  };

  return (
    <SafeAreaView style={styles.screen}>
      <StatusBar backgroundColor={Colors.FILL} />
      <Gap height={80} />
      <Text style={styles.headingTitle}>Register</Text>
      <Gap height={22} />
      <BaseInput
        label="Name"
        placeholder="Enter Name here"
        value={formValue.name}
        onChangeText={value => setFormValue('name', value)}
      />
      <Gap height={24} />
      <BaseInput
        label="Email"
        placeholder="Enter Email here"
        value={formValue.email}
        onChangeText={value => setFormValue('email', value)}
      />
      <Gap height={24} />
      <BaseInput
        label="Password"
        type="password"
        placeholder="Enter Password here"
        value={formValue.password}
        onChangeText={value => setFormValue('password', value)}
      />
      <Gap height={48} />
      <BaseButton
        isLoading={isLoading}
        disable={isLoading}
        title="Register"
        style={styles.btnLogin}
        onPress={handleRegister}
      />
      <Gap height={48} />
      <View style={styles.registerContainer}>
        <Text style={styles.registerText}>Already have an account? </Text>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => {
            navigation.replace('Login');
          }}>
          <Text style={styles.registerText}>Login</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default Register;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
    padding: 24,
  },
  headingTitle: {
    fontSize: 32,
    color: Colors.TEXT,
    fontFamily: Fonts.PRIMARY.BOLD,
  },
  socialSub: {
    width: 150,
    fontSize: 12,
    textAlign: 'center',
    color: Colors.SUB_TEXT,
    fontFamily: Fonts.PRIMARY.MEDIUM,
  },
  dividerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  divider: {
    flex: 1,
    height: 1,
    backgroundColor: Colors.GREY_MEDIUM,
  },
  btnLogin: {
    borderRadius: 100,
    shadowColor: Colors.SUB_TEXT,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.22,
    shadowRadius: 2,
    elevation: 2,
  },
  socialContainer: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  socialBtn: {
    width: 64,
    height: 64,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 16,
    backgroundColor: Colors.BACKGROUND,
    shadowColor: Colors.SUB_TEXT,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.22,
    shadowRadius: 2,
    elevation: 2,
  },
  registerContainer: {
    width: '100%',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  registerText: {
    fontSize: 12,
    color: Colors.SUB_TEXT,
    fontFamily: Fonts.PRIMARY.MEDIUM,
  },
});
