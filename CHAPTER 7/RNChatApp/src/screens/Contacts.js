import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  FlatList,
} from 'react-native';

import database from '../services/database';

import {Gap, SearchInput} from '../components';

import {Colors, Fonts} from '../utils';

import {useSelector, useDispatch} from 'react-redux';

import {setContacts} from '../store/userSlice';

const Contacts = props => {
  const {navigation} = props;

  const dispatch = useDispatch();

  const userState = useSelector(state => state.user.user);

  const contactsState = useSelector(state => state.user.contacts);

  const [tempState, setTempState] = useState([]);

  const [keywordSearch, setKeywordSearch] = useState('');

  const handleChangeSearch = event => {
    setKeywordSearch(event);
  };

  const handleSearchSubmit = () => {
    const filteredData = contactsState.filter(item => {
      return item.name.toLowerCase().match(keywordSearch);
    });

    dispatch(setContacts(filteredData));
  };

  const handleOnBlur = () => {
    if (keywordSearch === '') {
      dispatch(setContacts(tempState));
    }
  };

  useEffect(() => {
    const onValueChange = database.ref('/users/').on('value', snapshot => {
      const value = snapshot.val();
      const data = Object.values(value);
      const filteredData = data.filter(item => item.id !== userState.id);

      dispatch(setContacts(filteredData));

      setTempState(filteredData);
    });

    return () => database.ref('/users/').off('value', onValueChange);
  }, [dispatch, userState.id]);

  const renderItem = ({item}) => (
    <>
      <Gap height={24} />
      <TouchableOpacity
        activeOpacity={0.7}
        style={styles.chatContainer}
        onPress={() => {
          navigation.navigate('Chat', {
            receiverData: {
              id: item.id,
              name: item.name,
              authorRoom: null,
            },
          });
        }}>
        <View style={styles.contentContainer}>
          <View style={styles.userAvatar}>
            <Text style={styles.userAvatarText}>
              {item.name.toUpperCase().split('')[0]}
            </Text>
          </View>
          <Gap width={16} />
          <View style={styles.userContainer}>
            <Text
              ellipsizeMode="tail"
              numberOfLines={1}
              style={styles.userTitle}>
              {item.name}
            </Text>
            <Text ellipsizeMode="tail" numberOfLines={1} style={styles.userBio}>
              {item.bio ? item.bio : '-'}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
      <Gap height={24} />
      <View style={styles.divider} />
    </>
  );

  return (
    <SafeAreaView style={styles.screen}>
      <StatusBar backgroundColor={Colors.FILL} />
      <View style={styles.headerContainer}>
        <Text style={styles.headingTitle}>Contacts</Text>
      </View>
      <Gap height={28} />
      <SearchInput
        placeholder="Search Contact"
        value={keywordSearch}
        onChangeText={handleChangeSearch}
        onSubmit={handleSearchSubmit}
        onBlur={handleOnBlur}
      />
      <Gap height={28} />
      {contactsState?.length > 0 ? (
        <FlatList
          showsVerticalScrollIndicator={false}
          keyExtractor={item => item.id}
          data={contactsState}
          renderItem={renderItem}
        />
      ) : (
        <View style={styles.emptyContainer}>
          <Text style={styles.emptyText}>No Message</Text>
        </View>
      )}
    </SafeAreaView>
  );
};

export default Contacts;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
    padding: 24,
  },
  headingTitle: {
    fontSize: 32,
    color: Colors.TEXT,
    fontFamily: Fonts.PRIMARY.BOLD,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  signOutText: {
    fontSize: 12,
    color: Colors.WHITE,
    fontFamily: Fonts.PRIMARY.BOLD,
    paddingVertical: 12,
    paddingHorizontal: 16,
    backgroundColor: Colors.FILL,
    borderRadius: 100,
  },
  emptyContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyText: {
    fontSize: 14,
    color: Colors.GREY_DARK,
    fontFamily: Fonts.PRIMARY.MEDIUM,
  },
  chatContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  contentContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  userAvatar: {
    width: 55,
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.GREY_MEDIUM,
    borderRadius: 100,
  },
  userAvatarText: {
    fontSize: 16,
    color: Colors.GREY_DARK,
    fontFamily: Fonts.PRIMARY.SEMIBOLD,
  },
  userTitle: {
    fontSize: 16,
    color: Colors.GREY_DARK,
    fontFamily: Fonts.PRIMARY.SEMIBOLD,
    maxWidth: '100%',
  },
  userBio: {
    fontSize: 14,
    color: Colors.SUB_TEXT,
    fontFamily: Fonts.PRIMARY.REGULAR,
    maxWidth: '100%',
  },
  userMessage: {
    fontSize: 14,
    color: Colors.GREY_MEDIUM,
    fontFamily: Fonts.PRIMARY.SEMIBOLD,
    maxWidth: '100%',
  },
  lastSent: {
    fontSize: 11,
    color: Colors.GREY_MEDIUM,
    fontFamily: Fonts.PRIMARY.MEDIUM,
  },
  divider: {
    height: 1,
    backgroundColor: Colors.GREY_MEDIUM,
  },
});
