import React from 'react';

import {Text, TouchableOpacity, StyleSheet} from 'react-native';

import {
  IconChatTab,
  IconChatTabActive,
  IconPeopleTab,
  IconPeopleTabActive,
  IconSettingsTab,
  IconSettingsTabActive,
} from '../../assets';

import {Colors, Fonts} from '../../utils';
import Gap from '../Gap';

const TabItem = props => {
  const {title, isActive, onPress, onLongPress} = props;

  const Icon = () => {
    switch (title) {
      case 'Dashboard':
        return isActive ? <IconChatTabActive /> : <IconChatTab />;
      case 'Contacts':
        return isActive ? <IconPeopleTabActive /> : <IconPeopleTab />;
      case 'Settings':
        return isActive ? <IconSettingsTabActive /> : <IconSettingsTab />;
      default:
        return <IconChatTab />;
    }
  };

  const titleShow = () => {
    let displayTitle;

    switch (title) {
      case 'Dashboard':
        displayTitle = 'Chat';
        break;
      case 'Contacts':
        displayTitle = 'Contacts';
        break;
      case 'Settings':
        displayTitle = 'Settings';
        break;
      default:
        displayTitle = 'Other';
        break;
    }

    return displayTitle;
  };

  return (
    <TouchableOpacity
      activeOpacity={0.7}
      style={styles.container}
      onPress={onPress}
      onLongPress={onLongPress}>
      <Icon />
      <Gap height={8} />
      <Text style={styles.title(isActive)}>{titleShow()}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    paddingVertical: 15,
  },
  title: isActive => ({
    fontFamily: Fonts.PRIMARY.SEMIBOLD,
    fontSize: 12,
    color: isActive ? Colors.FILL : Colors.SUB_TEXT,
  }),
});

export default TabItem;
