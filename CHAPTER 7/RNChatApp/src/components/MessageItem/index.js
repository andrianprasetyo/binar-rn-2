import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import {Colors, Fonts} from '../../utils';
import Gap from '../Gap';

const MessageItem = props => {
  const {sender, message, time} = props;

  return (
    <View style={styles.container}>
      <Text style={styles.time(sender)}>{time}</Text>
      <Gap height={8} />
      <View style={[styles.chatBox(sender), styles.textCard(sender)]}>
        <Text style={styles.textMessage}>{message}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginVertical: 32,
  },
  chatBox: sender => ({
    alignSelf: sender ? 'flex-end' : 'flex-start',
    minWidth: 80,
    maxWidth: '80%',
    borderBottomRightRadius: sender ? 0 : 24,
    borderBottomLeftRadius: sender ? 24 : 0,
    borderRadius: 24,
  }),
  time: sender => ({
    alignSelf: sender ? 'flex-end' : 'flex-start',
    fontSize: 12,
    fontFamily: Fonts.PRIMARY.MEDIUM,
    color: Colors.GREY_MEDIUM,
  }),
  textCard: sender => ({
    color: sender ? Colors.FILL : Colors.WHITE,
    backgroundColor: sender ? Colors.GREY_MEDIUM : Colors.GREY_LIGHT,
    padding: 16,
  }),
  textMessage: {
    fontFamily: Fonts.PRIMARY.MEDIUM,
    fontSize: 12,
    color: Colors.TEXT,
  },
});

export default MessageItem;
