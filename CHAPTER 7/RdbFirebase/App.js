import React from 'react';

import {NavigationContainer} from '@react-navigation/native';

import Router from './src/router';

import {navigationRef} from './src/utils';

const App = () => {
  return (
    <NavigationContainer ref={navigationRef}>
      <Router />
    </NavigationContainer>
  );
};

export default App;
