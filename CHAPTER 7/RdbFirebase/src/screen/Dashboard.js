import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
} from 'react-native';

import {useRoute} from '@react-navigation/native';

import {firebase} from '@react-native-firebase/database';

import {v4 as uuidv4} from 'uuid';

import {Colors} from '../utils';

const Dashboard = props => {
  const {navigation} = props;
  const route = useRoute();
  const userData = route.params.userData;
  const UUID = uuidv4();

  const [allUser, setAllUser] = useState([]);

  const getAllUser = () => {
    firebase
      .app()
      .database(
        'https://rdbfirebase-ikbal-default-rtdb.asia-southeast1.firebasedatabase.app',
      )
      .ref('/users/')
      .once('value')
      .then(snapshot => {
        const value = snapshot.val();
        const data = [];

        for (const property in value) {
          data.push(value[property]);
        }

        setAllUser(data.filter(item => item.id !== userData.id));
      });
  };

  useEffect(() => {
    getAllUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const createChatList = data => {
    firebase
      .app()
      .database(
        'https://rdbfirebase-ikbal-default-rtdb.asia-southeast1.firebasedatabase.app',
      )
      .ref(`/chatlist/${userData.id}/${data.id}`)
      .once('value')
      .then(snapshot => {
        console.log(snapshot.val());

        const value = snapshot.val();

        if (value === null) {
          let roomId = UUID;
          let myData = {
            roomId,
            id: userData.id,
            name: userData.name,
            emailId: userData.emailId,
            lastMsg: '',
          };

          firebase
            .app()
            .database(
              'https://rdbfirebase-ikbal-default-rtdb.asia-southeast1.firebasedatabase.app',
            )
            .ref(`/chatlist/${userData.id}/${data.id}`)
            .update(myData)
            .then(() => console.log('Data Updated!'));

          delete data.password;
          data.lastMsg = '';
          data.roomId = roomId;

          firebase
            .app()
            .database(
              'https://rdbfirebase-ikbal-default-rtdb.asia-southeast1.firebasedatabase.app',
            )
            .ref(`/chatlist/${userData.id}/${data.id}`)
            .update(data)
            .then(() => console.log('Data Updated!'));

          navigation.navigate('ChatScreen', {
            receiverData: data,
            userData: userData,
          });
        } else {
          navigation.navigate('ChatScreen', {
            receiverData: snapshot.val(),
            userData: userData,
          });
        }
      });
  };

  const renderItem = ({item}) => (
    <TouchableOpacity
      style={styles.container}
      activeOpacity={0.7}
      onPress={() => createChatList(item)}>
      <View style={styles.card}>
        <Text style={styles.nameCard}>{item.name}</Text>
      </View>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={styles.screen}>
      <FlatList
        showsVerticalScrollIndicator={false}
        keyExtractor={(_, index) => index.toString()}
        data={allUser}
        renderItem={renderItem}
      />
    </SafeAreaView>
  );
};

export default Dashboard;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Colors.WHITE,
  },
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  card: {
    marginTop: 20,
    width: '75%',
    paddingVertical: 10,
    paddingHorizontal: 16,
    borderWidth: 2,
    borderRadius: 10,
    borderColor: Colors.GREEN,
  },
  nameCard: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 14,
    paddingVertical: 10,
    paddingHorizontal: 16,
  },
});
