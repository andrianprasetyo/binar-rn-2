import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {Colors} from '../utils';

import {firebase} from '@react-native-firebase/database';

const Login = props => {
  const {navigation} = props;
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const onLoginDB = () => {
    try {
      firebase
        .app()
        .database(
          'https://rdbfirebase-ikbal-default-rtdb.asia-southeast1.firebasedatabase.app',
        )
        .ref('/users/')
        .orderByChild('emailId')
        .equalTo(email)
        .once('value')
        .then(async snapshot => {
          if (snapshot.val() === null) {
            Alert.alert('Invalid Email ID');
            return false;
          }
          let userData = Object.values(snapshot.val())[0];
          if (userData?.password !== password) {
            Alert.alert('Error', 'Invalid Password');
            return false;
          }
          navigation.navigate('Dashboard', {userData: userData});
        });
    } catch (error) {
      Alert.alert('Error', 'User Not Found');
    }
  };

  return (
    <View style={styles.screen}>
      <View style={styles.container}>
        <TextInput
          style={styles.inputs}
          placeholder="Enter Email ID"
          placeholderTextColor="#444444"
          underlineColorAndroid="transparent"
          onChangeText={value => {
            setEmail(value);
          }}
          value={email}
        />
      </View>
      <View style={styles.container}>
        <TextInput
          style={styles.inputs}
          placeholder="Enter Password"
          placeholderTextColor="#444444"
          underlineColorAndroid="transparent"
          onChangeText={value => {
            setPassword(value);
          }}
          value={password}
          secureTextEntry
        />
      </View>
      <TouchableOpacity
        style={styles.btnLogin}
        activeOpacity={0.7}
        onPress={() => onLoginDB()}>
        <Text style={styles.textBtn}>Login Now</Text>
      </TouchableOpacity>
      <View style={styles.containerText}>
        <Text>Not Have Account? </Text>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => navigation.navigate('Register')}>
          <Text>Register here!</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.WHITE,
  },
  container: {
    marginTop: 15,
    width: '75%',
  },
  containerText: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputs: {
    width: '100%',
    paddingVertical: 10,
    paddingHorizontal: 16,
    borderWidth: 2,
    borderRadius: 10,
    borderColor: Colors.GREEN,
  },
  btnLogin: {
    marginVertical: 15,
    width: '75%',
    flexDirection: 'row',
    backgroundColor: Colors.GREEN,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textBtn: {
    fontWeight: 'bold',
    fontSize: 14,
    paddingVertical: 10,
    paddingHorizontal: 16,
  },
});
