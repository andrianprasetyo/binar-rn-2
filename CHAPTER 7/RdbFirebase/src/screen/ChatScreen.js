import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ImageBackground,
  FlatList,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';

import {useRoute} from '@react-navigation/native';

import {firebase} from '@react-native-firebase/database';

import {ChatHeader, MessageComponent} from '../components';

import {Colors} from '../utils';

const ChatScreen = props => {
  const route = useRoute();
  const {receiverData, userData} = route.params;

  const [msg, setMsg] = useState('');
  const [disabled, setDisabled] = useState(false);
  const [allChat, setallChat] = useState([]);

  useEffect(() => {
    const onChildAdd = firebase
      .app()
      .database(
        'https://rdbfirebase-ikbal-default-rtdb.asia-southeast1.firebasedatabase.app',
      )
      .ref(`/messages/${receiverData.roomId}`)
      .on('child_added', snapshot =>
        setallChat(state => [snapshot.val(), ...state]),
      );

    // Stop listening room for updates when no longer required
    return () =>
      firebase
        .app()
        .database(
          'https://rdbfirebase-ikbal-default-rtdb.asia-southeast1.firebasedatabase.app',
        )
        .ref(`/messages/${receiverData.roomId}`)
        .off('child_added', onChildAdd);
  }, [receiverData.roomId]);

  const sendMsg = () => {
    if (msg === '') {
      Alert.alert('Please enter your message');
      return false;
    }

    setDisabled(true);
    let msgData = {
      roomId: receiverData.roomId,
      message: msg,
      from: userData?.id,
      to: receiverData.id,
      sendTime: new Date().toLocaleDateString(),
      msgType: 'text',
    };

    const newReference = firebase
      .app()
      .database(
        'https://rdbfirebase-ikbal-default-rtdb.asia-southeast1.firebasedatabase.app',
      )
      .ref(`/messages/${receiverData.roomId}`)
      .push();

    msgData.id = newReference.key;
    newReference.set(msgData).then(() => {
      let chatListUpdate = {
        lastMsg: msg,
        sendTime: new Date().toLocaleDateString(),
      };

      firebase
        .app()
        .database(
          'https://rdbfirebase-ikbal-default-rtdb.asia-southeast1.firebasedatabase.app',
        )
        .ref(`/chatlist/${receiverData?.id}/${userData?.id}`)
        .update(chatListUpdate)
        .then(() => console.log('Data updated'));

      firebase
        .app()
        .database(
          'https://rdbfirebase-ikbal-default-rtdb.asia-southeast1.firebasedatabase.app',
        )
        .ref(`/chatlist/${userData?.id}/${receiverData?.id}`)
        .update(chatListUpdate)
        .then(() => console.log('Data updated'));

      setMsg('');
      setDisabled(false);
    });
  };

  return (
    <SafeAreaView style={styles.screen}>
      <View style={styles.container}>
        <ChatHeader data={receiverData} />
        <ImageBackground
          source={require('../assets/images/background-chat.jpg')}
          style={styles.imgBg}>
          <FlatList
            style={{flex: 1}}
            data={allChat}
            showsVerticalScrollIndicator={false}
            keyExtractor={(item, index) => index}
            inverted
            renderItem={({item}) => {
              return (
                <MessageComponent
                  sender={item.from === userData.id}
                  item={item}
                />
              );
            }}
          />
        </ImageBackground>
        <View style={styles.containerText}>
          <TextInput
            style={styles.textMessage}
            placeholder="type a message"
            placeholderTextColor={Colors.GREEN}
            multiline={true}
            value={msg}
            onChangeText={val => setMsg(val)}
          />

          <TouchableOpacity disabled={disabled} onPress={() => sendMsg()}>
            <Text style={{color: Colors.WHITE}}>Send!</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default ChatScreen;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Colors.WHITE,
  },
  container: {
    flex: 1,
  },
  containerText: {
    backgroundColor: Colors.GREEN,
    elevation: 5,
    // height: 60,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 7,
    justifyContent: 'space-evenly',
    paddingBottom: 20,
  },
  textMessage: {
    backgroundColor: Colors.WHITE,
    width: '80%',
    borderRadius: 25,
    borderWidth: 0.5,
    borderColor: Colors.WHITE,
    paddingHorizontal: 15,
    color: Colors.FILL,
    paddingBottom: 10,
  },
  imgBg: {
    flex: 1,
  },
  flatListStyle: {
    flex: 1,
  },
});
