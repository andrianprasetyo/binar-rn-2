import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  Alert,
} from 'react-native';

import {v4 as uuidv4} from 'uuid';

import {firebase} from '@react-native-firebase/database';

import {Colors} from '../utils';

const Register = props => {
  const {navigation} = props;
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [name, setName] = useState('');

  const UUID = uuidv4();

  const onRegisterWithRDB = async () => {
    if (name === '' || email === '' || password === '') {
      Alert.alert('Error', 'Please enter all the field');
      return false;
    }
    let data = {
      id: UUID,
      name: name,
      emailId: email,
      password: password,
    };

    try {
      firebase
        .app()
        .database(
          'https://rdbfirebase-ikbal-default-rtdb.asia-southeast1.firebasedatabase.app',
        )
        .ref('/users/' + data.id)
        .set(data)
        .then(() => {
          Alert.alert('Success', 'Register Successfully');
          setName('');
          setEmail('');
          setPassword('');
          navigation.navigate('Login');
        });
    } catch (error) {
      Alert.alert('Error', 'Oops Something has wrong');
      console.log(error);
    }
  };

  return (
    <View style={styles.screen}>
      <View style={styles.container}>
        <TextInput
          style={styles.inputs}
          placeholder="Enter Email ID"
          placeholderTextColor="#444444"
          underlineColorAndroid="transparent"
          onChangeText={value => {
            setEmail(value);
          }}
          value={email}
        />
      </View>
      <View style={styles.container}>
        <TextInput
          style={styles.inputs}
          placeholder="Enter Password"
          placeholderTextColor="#444444"
          underlineColorAndroid="transparent"
          onChangeText={value => {
            setPassword(value);
          }}
          value={password}
          secureTextEntry
        />
      </View>
      <View style={styles.container}>
        <TextInput
          style={styles.inputs}
          placeholder="Enter Username"
          placeholderTextColor="#444444"
          underlineColorAndroid="transparent"
          onChangeText={value => {
            setName(value);
          }}
          value={name}
        />
      </View>

      <TouchableOpacity
        style={styles.btnLogin}
        activeOpacity={0.7}
        onPress={() => onRegisterWithRDB()}>
        <Text style={styles.textBtn}>Register</Text>
      </TouchableOpacity>

      <View style={styles.containerText}>
        <Text>Already Have Account? </Text>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => navigation.navigate('Login')}>
          <Text>Login here!</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Register;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.WHITE,
  },
  container: {
    marginTop: 15,
    width: '75%',
  },
  containerText: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputs: {
    width: '100%',
    paddingVertical: 10,
    paddingHorizontal: 16,
    borderWidth: 2,
    borderRadius: 10,
    borderColor: Colors.GREEN,
  },
  btnLogin: {
    marginVertical: 15,
    width: '75%',
    flexDirection: 'row',
    backgroundColor: Colors.GREEN,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textBtn: {
    fontWeight: 'bold',
    fontSize: 14,
    paddingVertical: 10,
    paddingHorizontal: 16,
  },
});
