// import moment from 'moment';
import React from 'react';
import {View, Text, StyleSheet, Pressable} from 'react-native';

import {Colors} from '../utils';

const MessageComponent = props => {
  const {sender, item, sendTime} = props;

  return (
    <Pressable style={styles.container}>
      <View
        style={[styles.TriangleShapeCSS, sender ? styles.right : [styles.left]]}
      />
      <View style={[styles.masBox, styles.textCard(sender)]}>
        <Text style={styles.textMessage}>{item.message}</Text>
      </View>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    marginVertical: 0,
  },
  masBox: {
    alignSelf: 'flex-end',
    marginHorizontal: 10,
    minWidth: 80,
    maxWidth: '80%',
    paddingHorizontal: 10,
    marginVertical: 5,
    paddingTop: 5,
    borderRadius: 8,
  },
  timeText: {
    fontSize: 10,
  },
  dayview: {
    alignSelf: 'center',
    height: 30,
    width: 100,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    marginTop: 10,
  },
  iconView: {
    width: 42,
    height: 42,
    borderRadius: 21,
    alignItems: 'center',
    justifyContent: 'center',
  },
  TriangleShapeCSS: {
    position: 'absolute',
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: 15,
    borderRightWidth: 5,
    borderBottomWidth: 20,
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
  },
  left: {
    borderBottomColor: Colors.WHITE,
    left: 2,
    bottom: 10,
    transform: [{rotate: '0deg'}],
  },
  right: {
    borderBottomColor: Colors.WHITE,
    right: 2,
    // top:0,
    bottom: 5,
    transform: [{rotate: '105deg'}],
  },
  textCard: sender => ({
    paddingLeft: 5,
    color: sender ? Colors.GREEN : Colors.WHITE,
    fontSize: 12.5,
    backgroundColor: Colors.WHITE,
  }),
  textMessage: sender => ({
    paddingLeft: 5,
    color: sender ? Colors.WHITE : Colors.GREEN,
    fontSize: 12.5,
  }),
});

export default MessageComponent;
