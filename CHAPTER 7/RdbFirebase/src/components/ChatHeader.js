import React, {useState} from 'react';
import {View, Text, StyleSheet, StatusBar} from 'react-native';
import {Colors} from '../utils';

const ChatHeader = props => {
  const {data} = props;
  return (
    <View style={styles.container}>
      <StatusBar
        barStyle="light-content"
        backgroundColor={Colors.GREEN}
        translucent={false}
      />
      <View style={styles.card}>
        <Text numberOfLines={1} style={styles.cardTitle}>
          {data.name}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 70,
    backgroundColor: Colors.GREEN,
    elevation: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  card: {
    flex: 1,
    marginLeft: 10,
  },
  cardTitle: {
    color: Colors.WHITE,
    fontSize: 16,
    textTransform: 'capitalize',
  },
});

export default ChatHeader;
