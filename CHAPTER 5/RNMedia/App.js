import React from 'react';
import {View, StyleSheet} from 'react-native';

import Pdf from './src/Pdf';
import Video from './src/Video';

const App = () => {
  return (
    <View style={styles.container}>
      {/* Pdf Example */}
      <View style={styles.container}>
        <Pdf />
      </View>
      {/* Video Example */}
      <View style={styles.container}>
        <Video />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
