import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

import {data, dataLogin} from './homeDummyData';

describe('Api', () => {
  it('Api Pokemon test ', async () => {
    let mock = new MockAdapter(axios);
    mock
      .onGet('https://pokeapi.co/api/v2/pokemon?offset=100&limit=10')
      .reply(200, data);

    // Act
    let res = await axios.get(
      'https://pokeapi.co/api/v2/pokemon?offset=100&limit=10',
    );

    // Assert
    expect(res.status).toEqual(200);
    expect(res.data).toEqual(data);
  });

  // Post Login
  it('Api login ', async () => {
    let mock = new MockAdapter(axios);

    const loginBody = {
      email: 'hexivib245@karavic.com',
      password: '!Isthatyou99',
    };

    mock
      .onPost('http://code.aldipee.com/api/v1/auth/login')
      .reply(200, dataLogin);

    // Act
    let res = await axios.post(
      'http://code.aldipee.com/api/v1/auth/login',
      loginBody,
    );

    // Assert
    expect(res.data).toEqual(dataLogin);
    expect(res.status).toEqual(200);
  });
});
