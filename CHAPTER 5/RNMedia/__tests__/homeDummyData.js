export const data = {
  count: 1118,
  next: 'https://pokeapi.co/api/v2/pokemon?offset=110&limit=10',
  previous: 'https://pokeapi.co/api/v2/pokemon?offset=90&limit=10',
  results: [
    {
      name: 'electrode',
      url: 'https://pokeapi.co/api/v2/pokemon/101',
    },
    {
      name: 'exeggcute',
      url: 'https://pokeapi.co/api/v2/pokemon/102',
    },
    {
      name: 'exeggutor',
      url: 'https://pokeapi.co/api/v2/pokemon/103',
    },
    {
      name: 'cubone',
      url: 'https://pokeapi.co/api/v2/pokemon/104',
    },
  ],
};

export const dataLogin = {
  user: {
    role: 'user',
    isEmailVerified: true,
    email: 'hexivib245@karavic.com',
    name: 'User Dev',
    id: '6243cd40fc78840f1e9e3865',
  },
  tokens: {
    access: {
      token:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI2MjQzY2Q0MGZjNzg4NDBmMWU5ZTM4NjUiLCJpYXQiOjE2NTAwMTI2MDIsImV4cCI6MTY1MDAxNDQwMiwidHlwZSI6ImFjY2VzcyJ9.RUuIbmFN5UwwicGgA_XazPgNCZ9maoA4A6ZkSdQov5c',
      expires: '2022-04-15T09:20:02.500Z',
    },
    refresh: {
      token:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI2MjQzY2Q0MGZjNzg4NDBmMWU5ZTM4NjUiLCJpYXQiOjE2NTAwMTI2MDIsImV4cCI6MTY1MjYwNDYwMiwidHlwZSI6InJlZnJlc2gifQ.HS_1aVktgQt9DLTQVwhhE0332WbM9FSDNZJlWb9lDSw',
      expires: '2022-05-15T08:50:02.500Z',
    },
  },
};
