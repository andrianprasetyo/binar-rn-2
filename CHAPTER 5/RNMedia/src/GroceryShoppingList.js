import React, {useState, useCallback} from 'react';
import {View, Text, TextInput, Button} from 'react-native';

const GroceryShoppingList = () => {
  const [groceryItem, setGroceryItem] = useState('');
  const [items, setItems] = useState([]);

  const addNewItemToShoppingList = useCallback(() => {
    setItems([groceryItem, ...items]);
    setGroceryItem('');
  }, [groceryItem, items]);

  return (
    <View>
      <TextInput
        value={groceryItem}
        placeholder="Enter grocery item"
        onChangeText={text => setGroceryItem(text)}
      />
      <Button title="Add the item to list" onPress={addNewItemToShoppingList} />
      {items.map(item => (
        <Text key={item}>{item}</Text>
      ))}
    </View>
  );
};

export default GroceryShoppingList;
