import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default class Home extends Component {
  constructor() {
    super();
    this.state = {
      data: '',
    };
  }
  change(val) {
    this.setState({data: val * 10});
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Home Components</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 10,
  },
});
