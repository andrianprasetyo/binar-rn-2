import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import RNVideo from 'react-native-video';

const Video = () => {
  return (
    <View style={styles.container}>
      <Text>React Native Video</Text>
      <RNVideo
        source={{
          uri: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
        }}
        style={styles.rnVideo}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#696969',
  },
  rnVideo: {
    width: 300,
    height: 300,
  },
});

export default Video;
