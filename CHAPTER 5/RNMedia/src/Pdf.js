import React, {useState} from 'react';
import {View, Text, StyleSheet, Button, Dimensions} from 'react-native';

import RNPdf from 'react-native-pdf';

const {width, height} = Dimensions.get('window');

const PDF = () => {
  const [pdfValue, setPdfValue] = useState();

  const source = 'http://samples.leanpub.com/thereactnativebook-sample.pdf';

  return (
    <View style={styles.container}>
      <Text style={styles.text}>My document project!</Text>
      <Button onPress={() => setPdfValue(source)} title="Open PDF Document" />
      {pdfValue && (
        <View accessibilityHint="Document Show">
          <RNPdf source={{uri: pdfValue}} style={styles.pdf} />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#696969',
  },
  text: {
    color: '#FFFFFF',
    margin: 10,
  },
  pdf: {
    width: width * 0.5,
    height: height * 0.5,
  },
});

export default PDF;
