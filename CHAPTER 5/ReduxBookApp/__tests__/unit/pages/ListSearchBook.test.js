import React from 'react';
import {create} from 'react-test-renderer';
import {createStore} from 'redux';

import {ListSearchBook} from '../../../src/pages';
import ContainerTesting from '../../../src/utils/containerTesting/reduxTesting';

import {booksData} from '../../../src/utils';

describe('Unit: List Search Book', () => {
  it('should render list searched book with length 10 ', () => {
    const booksState = (state = booksData, action) => {
      switch (action.type) {
        default:
          return state;
      }
    };

    const store = createStore(booksState);

    const {root, toJSON} = create(ContainerTesting(<ListSearchBook />, store));

    const compJSON = toJSON();

    const state = root.props.store.getState();
    expect(root.props.store).toBeTruthy();
    expect(state).toHaveLength(10);
    expect(compJSON).toMatchSnapshot();
  });

  it('should render empty list searched book with length 0', () => {
    const booksState = (state = [], action) => {
      switch (action.type) {
        default:
          return state;
      }
    };

    const store = createStore(booksState);

    const {root, toJSON} = create(ContainerTesting(<ListSearchBook />, store));

    const compJSON = toJSON();

    const state = root.props.store.getState();
    expect(root.props.store).toBeTruthy();
    expect(state).toHaveLength(0);
    expect(compJSON).toMatchSnapshot();
  });
});
