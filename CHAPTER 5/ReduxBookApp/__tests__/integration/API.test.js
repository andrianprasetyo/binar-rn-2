import MockAdapter from 'axios-mock-adapter';
import apiClient from '../../src/services/api';
import {userData, booksData} from '../../src/utils';

describe('API Testing', () => {
  const dataUser = {
    name: 'Exivib',
    email: 'exivib245@karavic.com',
    password: '!FalsyTruthy',
  };

  const URL = 'http://code.aldipee.com/api/v1';

  it('Auth Register', async () => {
    let mock = new MockAdapter(apiClient);

    mock.onPost(`${URL}/auth/register`).reply(200, userData);

    const response = await apiClient.post('/auth/register', dataUser);

    expect(response.status).toEqual(200);
    expect(response.data.user).toEqual(userData.user);
  });

  it('Auth Login', async () => {
    let mock = new MockAdapter(apiClient);

    mock.onPost(`${URL}/auth/login`).reply(200, userData);

    const response = await apiClient.post('/auth/login', {
      email: dataUser.email,
      password: dataUser.password,
    });

    expect(response.status).toEqual(200);
    expect(response.data.user).toEqual(userData.user);
  });

  it('Get List Books', async () => {
    let mock = new MockAdapter(apiClient);

    mock.onGet(`${URL}/books`).reply(200, booksData);

    const response = await apiClient.get('/books');

    expect(response.status).toEqual(200);
    expect(response.data).toEqual(booksData);
  });

  it('Get Book Searched with keyword "Harry"', async () => {
    let mock = new MockAdapter(apiClient);

    mock.onGet(`${URL}/books?title=Harry&limit=1`).reply(200, booksData[0]);

    const response = await apiClient.get('/books?title=Harry&limit=1');

    expect(response.status).toEqual(200);
    expect(response.data).toEqual(booksData[0]);
  });
});
