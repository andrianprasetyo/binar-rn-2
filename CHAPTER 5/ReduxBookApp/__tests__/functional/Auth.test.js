import React from 'react';
import {fireEvent, render, cleanup} from '@testing-library/react-native';
import * as ReactRedux from 'react-redux';

import {Login, Register} from '../../src/pages';

describe('Functional: Auth Register & Login', () => {
  const mockOnPress = jest.fn();

  const useDispatchSpy = jest.spyOn(ReactRedux, 'useDispatch');
  const useSelectorSpy = jest.spyOn(ReactRedux, 'useSelector');
  useDispatchSpy.mockReturnValue(jest.fn());
  useSelectorSpy.mockReturnValue(jest.fn());

  const userData = {
    name: 'UserDevTest',
    email: 'userdevtest@mail.com',
    password: '!Thisisntprod99',
  };

  afterEach(() => {
    cleanup;
  });

  it('should be able to fill on field input "Name", "Email", and "Password" for Register', async () => {
    const {getByLabelText} = render(<Register navigation={mockOnPress} />);

    const component = {
      name: getByLabelText('Name'),
      email: getByLabelText('Email'),
      password: getByLabelText('Password'),
    };

    fireEvent.changeText(component.name, userData.name);
    fireEvent.changeText(component.email, userData.email);
    fireEvent.changeText(component.password, userData.password);

    expect(component.name.props.value).toEqual(userData.name);
    expect(component.email.props.value).toEqual(userData.email);
    expect(component.password.props.value).toEqual(userData.password);
  });

  it('should be able to fill on field input "Email" and "Password" for Login', async () => {
    const {getByLabelText} = render(<Login navigation={mockOnPress} />);

    const component = {
      email: getByLabelText('Email'),
      password: getByLabelText('Password'),
    };

    fireEvent.changeText(component.email, userData.email);
    fireEvent.changeText(component.password, userData.password);

    expect(component.email.props.value).toEqual(userData.email);
    expect(component.password.props.value).toEqual(userData.password);
  });
});
