import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {Gap, BaseInput, BaseButton} from '../../components';
import {useForm} from '../../hooks';
import {authRegister} from '../../store/actions/users';
import {Colors, Fonts, showSuccess} from '../../utils';

const Register = props => {
  const {navigation} = props;

  const dispatch = useDispatch();
  const userState = useSelector(state => state.users);

  const [formValue, setFormValue] = useForm({
    name: '',
    email: '',
    password: '',
  });

  const handleSubmit = () => {
    dispatch(authRegister(formValue)).then(response => {
      if (response.success && response.data.hasOwnProperty('id')) {
        showSuccess(response.message);
        setFormValue('reset');
        navigation.navigate('RegisterSuccess');
      }
    });
  };

  return (
    <View style={styles.page}>
      <Text style={styles.heading}>Introduce Yourself</Text>
      <Gap height={30} />
      <BaseInput
        label="Name"
        value={formValue.name}
        placeholder={'Enter Name here'}
        onChangeText={value => setFormValue('name', value)}
      />
      <Gap height={28} />
      <BaseInput
        label="Email"
        value={formValue.email}
        placeholder={'Enter Email here'}
        onChangeText={value => setFormValue('email', value)}
      />
      <Gap height={28} />
      <BaseInput
        type="password"
        label="Password"
        value={formValue.password}
        placeholder={'Enter password here'}
        onChangeText={value => setFormValue('password', value)}
      />
      <Gap height={28} />
      <BaseButton
        title={userState.isLoading ? 'Loading...' : 'Create Account'}
        isLoading={userState.isLoading}
        disable={userState.isLoading}
        onPress={handleSubmit}
      />
      <Gap height={28} />
      <View style={styles.loginContainer}>
        <Text style={styles.textInfo}>Already have an account?</Text>
        <TouchableOpacity
          accessibilityLabel="button-login-cta"
          activeOpacity={0.7}
          onPress={() => navigation.navigate('Login')}>
          <Text style={styles.textBtnLogin}>Login</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
    padding: 20,
  },
  heading: {
    fontFamily: Fonts.PRIMARY.BOLD,
    fontSize: 22,
    color: Colors.TEXT,
  },
  textBtnLogin: {
    fontFamily: Fonts.PRIMARY.BOLD,
    fontSize: 16,
    color: Colors.FILL,
  },
  loginContainer: {
    alignItems: 'center',
  },
  textInfo: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 14,
    color: Colors.SUB_TEXT,
  },
});

export default Register;
