import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {BookItem, Error, Gap, SearchInput} from '../../components';
import {clearBooks, fetchSearchBook} from '../../store/actions/books';
import {Colors, Fonts} from '../../utils';
import Icon from 'react-native-vector-icons/Ionicons';
import {authLogout} from '../../store/actions/users';

const ListSearchBook = props => {
  const {navigation, route} = props;
  const {keyword} = route.params;
  const dispatch = useDispatch();
  const searchBook = useSelector(state => state.books.searchBook);
  const isLoading = useSelector(state => state.books.isLoading);
  const error = useSelector(state => state.books.error);

  const [keywordSearch, setKeywordSearch] = useState('');

  const handleChangeSearch = event => {
    setKeywordSearch(event);
  };

  const handleSearchSubmit = () => {
    dispatch(fetchSearchBook(keywordSearch));
  };

  const handleClickDetail = id => {
    navigation.navigate('DetailBook', {id});
  };

  const handleRefresh = () => {
    dispatch(fetchSearchBook(keyword));
  };

  const handleBackLogin = () => {
    dispatch(authLogout());
    dispatch(clearBooks());
    navigation.replace('Login');
  };

  useEffect(() => {
    setKeywordSearch(keyword);
    dispatch(fetchSearchBook(keyword));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View style={styles.page}>
      <View style={styles.header}>
        <View style={styles.headingContainer}>
          <TouchableOpacity
            accessibilityLabel="btn-back"
            style={styles.btnContainer}
            activeOpacity={0.7}
            onPress={() => {
              navigation.goBack();
            }}>
            <Icon name="chevron-back" size={24} color={Colors.SUB_TEXT} />
            <Gap width={4} />
            <Text style={styles.backBtn}>Back</Text>
          </TouchableOpacity>
        </View>
        <Gap height={20} />
        <SearchInput
          placeholder="Search Books"
          value={keywordSearch}
          onChangeText={handleChangeSearch}
          onSubmit={handleSearchSubmit}
        />
        <Gap height={20} />
      </View>
      <Gap height={20} />
      {isLoading ? (
        <View style={styles.page}>
          <ActivityIndicator
            style={styles.loading}
            size={'large'}
            color={Colors.FILL}
          />
        </View>
      ) : error ? (
        <Error
          btnTitle={
            String(error).includes('authenticate') ? 'Back to Login' : 'Refresh'
          }
          message={error}
          onPress={
            String(error).includes('authenticate')
              ? handleBackLogin
              : handleRefresh
          }
        />
      ) : searchBook.length < 1 ? (
        <View style={styles.emptyContainer}>
          <Icon name="information-circle" size={60} color={Colors.TEXT} />
          <Gap height={12} />
          <Text style={styles.emptyText}>
            Looks like books what you search is empty
          </Text>
        </View>
      ) : (
        <FlatList
          numColumns={2}
          data={searchBook}
          keyExtractor={item => item.id}
          renderItem={({item}) => (
            <BookItem
              title={item.title}
              image={item.cover_image}
              author={item.author}
              price={item.price}
              onPress={() => handleClickDetail(item.id)}
            />
          )}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
  },
  header: {
    padding: 20,
    backgroundColor: Colors.BACKGROUND,
    shadowColor: Colors.SUB_TEXT,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.22,
    shadowRadius: 2,
    elevation: 3,
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headingContainer: {
    backgroundColor: Colors.WHITE,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  btnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  backBtn: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 17,
    lineHeight: 20,
    color: Colors.SUB_TEXT,
  },
  emptyContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default ListSearchBook;
