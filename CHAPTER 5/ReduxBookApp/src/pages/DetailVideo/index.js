import React from 'react';
import {StyleSheet, View, ActivityIndicator} from 'react-native';

import {Colors} from '../../utils';
import {WebView} from 'react-native-webview';

const DetailVideo = props => {
  const {route} = props;
  const {item} = route.params;

  return (
    <>
      <WebView
        style={styles.video}
        renderLoading={() => (
          <View style={styles.loading}>
            <ActivityIndicator color={Colors.FILL} size="large" />
          </View>
        )}
        source={{uri: `https://www.youtube.com/embed/${item.id.videoId}`}}
      />
    </>
  );
};

const styles = StyleSheet.create({
  video: {
    width: '100%',
    height: 230,
  },
  title: {
    position: 'absolute',
  },
  loading: {
    flex: 1,
    alignItems: 'center',
  },
});

export default DetailVideo;
