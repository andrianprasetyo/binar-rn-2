import React, {useEffect, Fragment} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  FlatList,
  ActivityIndicator,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {useIsFocused} from '@react-navigation/native';
import {useSelector, useDispatch, shallowEqual} from 'react-redux';
import {clearBooks, fetchBooks} from '../../store/actions/books';
import Icon from 'react-native-vector-icons/Ionicons';

import {Colors, Fonts} from '../../utils';
import {Gap, Header, CategoryItem, BookItem, Error} from '../../components';
import {authLogout} from '../../store/actions/users';

const numberOfColums = 2;

const {height} = Dimensions.get('window');

const Home = props => {
  const {navigation} = props;
  const {users} = useSelector(state => state.users);
  const isLoading = useSelector(state => state.books.isLoading);
  const error = useSelector(state => state.books.error);

  const isFocused = useIsFocused();

  const recommendsBooks = useSelector(
    state =>
      state.books.books
        .sort((a, b) => b.average_rating - a.average_rating)
        .slice(0, 6),
    shallowEqual,
  );

  const bestDealBooks = useSelector(
    state => state.books.books.sort((a, b) => a.price - b.price).slice(0, 6),
    shallowEqual,
  );

  const selectionBooks = useSelector(
    state => state.books.books.sort(() => Math.random() - 0.5),
    shallowEqual,
  );

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchBooks());

    if (isFocused) {
      dispatch(fetchBooks());
    }
  }, [dispatch, isFocused]);

  const handleClickDetail = id => {
    navigation.navigate('DetailBook', {id});
  };

  const handleRefresh = () => {
    dispatch(fetchBooks());
  };

  const handleBackLogin = () => {
    dispatch(authLogout());
    dispatch(clearBooks());
    navigation.replace('Login');
  };

  const renderRecommendsBooks = () => {
    return isLoading ? (
      <ActivityIndicator size={'large'} color={Colors.FILL} />
    ) : error !== null ? (
      <Text style={styles.error}>{error}</Text>
    ) : (
      recommendsBooks.map(recommend => (
        <Fragment key={recommend.id}>
          <CategoryItem
            key={recommend.id}
            image={recommend.cover_image}
            title={recommend.title}
            rating={recommend.average_rating}
            onPress={() => handleClickDetail(recommend.id)}
          />
          <Gap width={16} />
        </Fragment>
      ))
    );
  };

  const renderBestDealBooks = () => {
    return isLoading ? (
      <ActivityIndicator size={'large'} color={Colors.FILL} />
    ) : error !== null ? (
      <Text style={styles.error}>{error}</Text>
    ) : (
      bestDealBooks.map(book => (
        <Fragment key={book.id}>
          <CategoryItem
            key={book.id}
            image={book.cover_image}
            title={book.title}
            price={book.price}
            onPress={() => handleClickDetail(book.id)}
          />
          <Gap width={16} />
        </Fragment>
      ))
    );
  };

  const ButtonRefresh = () => {
    return (
      <TouchableOpacity
        style={styles.btnRefresh}
        activeOpacity={0.7}
        onPress={handleRefresh}>
        <Icon name="refresh-circle" size={36} color={Colors.FILL} />
      </TouchableOpacity>
    );
  };

  const HeaderSection = () => {
    return (
      <Fragment>
        <Header user={users.name} />
        <View style={styles.contentContainer}>
          <Text style={styles.headingCategory}>
            Recommends Book for this Week
          </Text>
          <Gap height={16} />
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            {renderRecommendsBooks()}
          </ScrollView>
          <Gap height={32} />
          <Text style={styles.headingCategory}>
            Best Cheapest Deal Books this Weeks
          </Text>
          <Gap height={16} />
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            {renderBestDealBooks()}
          </ScrollView>
          <Gap height={32} />
          <Text style={styles.headingCategory}>
            Selection Books Just for You
          </Text>
        </View>
      </Fragment>
    );
  };

  return (
    <View style={styles.page}>
      {isLoading ? (
        <ActivityIndicator
          style={styles.loading}
          size={'large'}
          color={Colors.FILL}
        />
      ) : error !== null ? (
        <Error
          btnTitle={
            String(error).includes('authenticate') ? 'Back to Login' : 'Refresh'
          }
          message={error}
          onPress={
            String(error).includes('authenticate')
              ? handleBackLogin
              : handleRefresh
          }
        />
      ) : (
        <Fragment>
          <ButtonRefresh />
          <FlatList
            style={styles.containerBooks}
            numColumns={numberOfColums}
            data={selectionBooks}
            ListHeaderComponent={HeaderSection}
            keyExtractor={item => item.id}
            renderItem={({item}) => (
              <BookItem
                title={item.title}
                image={item.cover_image}
                author={item.author}
                price={item.price}
                onPress={() => handleClickDetail(item.id)}
              />
            )}
          />
        </Fragment>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  page: {flex: 1, backgroundColor: Colors.BACKGROUND},
  loading: {
    marginTop: height * 0.5,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnRefresh: {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: Colors.WHITE,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 10,
    right: 10,
    shadowColor: Colors.SUB_TEXT,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.22,
    shadowRadius: 2,
    elevation: 2,
    zIndex: 10,
  },
  error: {
    marginTop: height * 0.5,
    fontFamily: Fonts.PRIMARY.SEMI_BOLD,
    fontSize: 14,
    color: Colors.TEXT,
    textAlign: 'center',
  },
  contentContainer: {
    marginHorizontal: 20,
    marginVertical: 30,
  },
  headingCategory: {
    fontFamily: Fonts.PRIMARY.BOLD,
    fontSize: 16,
    color: Colors.TEXT,
  },
  recommendContainer: {
    marginVertical: 20,
  },
  containerBooks: {
    flex: 1,
  },
});

export default Home;
