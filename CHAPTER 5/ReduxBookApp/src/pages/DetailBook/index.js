import React, {Fragment, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  Share,
} from 'react-native';
import {BaseButton, Error, Gap} from '../../components';
import {Colors, Fonts, showSuccess} from '../../utils';
import Icon from 'react-native-vector-icons/Ionicons';
import {IntlProvider, FormattedNumber} from 'react-intl';

import {clearBooks, fetchDetailBook} from '../../store/actions/books';
import {useDispatch, useSelector} from 'react-redux';
import {authLogout} from '../../store/actions/users';
import {notification} from '../../services/notification';

const DetailBook = props => {
  const {navigation, route} = props;
  const {id} = route.params;

  const dispatch = useDispatch();

  const detailBookState = useSelector(state => state.books.detailBook);
  const isLoading = useSelector(state => state.books.isLoading);
  const error = useSelector(state => state.books.error);

  const handleRefresh = () => {
    dispatch(fetchDetailBook(id));
  };

  const handleBackLogin = () => {
    dispatch(authLogout());
    dispatch(clearBooks());
    navigation.replace('Login');
  };

  const handleNotification = () => {
    notification.configure();
    notification.createChannel(
      id,
      detailBookState.title,
      detailBookState.synopsis,
    );
    notification.sendNotification(
      id,
      `You liked ${detailBookState.title}`,
      `Hurry Up...There are ${detailBookState.stock_available} in stock that you can Buy!`,
    );
  };

  const handleClickBuy = () => {
    showSuccess(`${detailBookState.title} has been added to cart for you buy`);
  };

  const handleShare = () => {
    const shareOptions = {
      title: detailBookState.title,
      message: 'Hey, please check out this book that maybe you love', // Note that according to the documentation at least one of "message" or "url" fields is required
      url: detailBookState.cover_image,
      subject: 'Books',
    };

    Share.share(shareOptions);
  };

  useEffect(() => {
    dispatch(fetchDetailBook(id));
  }, [dispatch, id]);

  const ButtonRefresh = () => {
    return (
      <TouchableOpacity
        accessibilityLabel={'button-refresh'}
        style={styles.btnRefresh}
        activeOpacity={0.7}
        onPress={handleRefresh}>
        <Icon name="refresh-circle" size={36} color={Colors.FILL} />
      </TouchableOpacity>
    );
  };

  return (
    <Fragment>
      {isLoading ? (
        <View style={styles.page}>
          <ActivityIndicator
            style={styles.loading}
            size={'large'}
            color={Colors.FILL}
          />
        </View>
      ) : error ? (
        <Error
          btnTitle={
            String(error).includes('authenticate') ? 'Back to Login' : 'Refresh'
          }
          message={error}
          onPress={
            String(error).includes('authenticate')
              ? handleBackLogin
              : handleRefresh
          }
        />
      ) : (
        <Fragment>
          <ButtonRefresh />
          <ScrollView style={styles.page}>
            <View style={styles.headingContainer}>
              <TouchableOpacity
                style={styles.btnContainer}
                activeOpacity={0.7}
                onPress={() => {
                  navigation.goBack();
                }}>
                <Icon name="chevron-back" size={24} color={Colors.SUB_TEXT} />
                <Gap width={4} />
                <Text style={styles.backBtn}>Back</Text>
              </TouchableOpacity>
              <View style={styles.btnContainer}>
                <TouchableOpacity
                  accessibilityLabel={'button-notification'}
                  activeOpacity={0.7}
                  onPress={() => {
                    handleNotification();
                  }}>
                  <Icon name="heart-circle" size={24} color={Colors.SUB_TEXT} />
                </TouchableOpacity>
                <Gap width={20} />
                <TouchableOpacity
                  accessibilityLabel={'button-share'}
                  activeOpacity={0.7}
                  onPress={() => {
                    handleShare();
                  }}>
                  <Icon name="share" size={24} color={Colors.SUB_TEXT} />
                </TouchableOpacity>
              </View>
            </View>
            <Image
              accessibilityLabel={`image-${detailBookState.title}`}
              style={styles.image}
              source={{
                uri: detailBookState.cover_image,
              }}
              resizeMethod="auto"
              resizeMode="cover"
            />
            <View style={styles.contentContainer}>
              <Text
                accessibilityLabel={detailBookState.title}
                style={styles.title}>
                {detailBookState.title}
              </Text>
              <Gap height={16} />
              <Text style={styles.author}>by {detailBookState.author}</Text>
              <Gap height={16} />
              <View style={styles.ratingAndTotalSale}>
                <View style={styles.ratingContainer}>
                  <Icon name="star" size={16} color={Colors.FILL} />
                  <Gap width={6} />
                  <Text style={styles.rating}>
                    {detailBookState.average_rating}/10
                  </Text>
                </View>
                <Gap width={30} />
                <View style={styles.totalSaleContainer}>
                  <Icon name="md-bookmarks" size={16} color={Colors.FILL} />
                  <Gap width={6} />
                  <Text style={styles.totalSale}>
                    {detailBookState.total_sale} Total Sale
                  </Text>
                </View>
              </View>
              <Gap height={30} />
              <IntlProvider locale="id" defaultLocale="id">
                <Text style={styles.price}>
                  <FormattedNumber
                    value={detailBookState.price}
                    style="currency"
                    currency="IDR"
                  />
                </Text>
              </IntlProvider>
              <Gap height={30} />
              <View style={styles.stockContainer}>
                <Text style={styles.stockTitle}>Stock Available</Text>
                <Text style={styles.stockNumber}>
                  {detailBookState.stock_available}
                </Text>
              </View>
              <Gap height={30} />
              <Text style={styles.overviewHeading}>Overview</Text>
              <Gap height={20} />
              <Text style={styles.overview}>{detailBookState.synopsis}</Text>
              <Gap height={20} />
              <BaseButton
                disable={detailBookState.stock_available < 1}
                title="Buy Now"
                onPress={() => {
                  handleClickBuy();
                }}
              />
            </View>
          </ScrollView>
        </Fragment>
      )}
    </Fragment>
  );
};

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
  },
  btnRefresh: {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: Colors.WHITE,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 10,
    right: 10,
    shadowColor: Colors.SUB_TEXT,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.22,
    shadowRadius: 2,
    elevation: 2,
    zIndex: 20,
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headingContainer: {
    backgroundColor: Colors.WHITE,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
  },
  btnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  backBtn: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 17,
    lineHeight: 20,
    color: Colors.SUB_TEXT,
  },
  image: {
    width: '100%',
    minHeight: 530,
  },
  contentContainer: {
    padding: 20,
  },
  title: {
    fontFamily: Fonts.PRIMARY.SEMI_BOLD,
    fontSize: 20,
    lineHeight: 24,
    color: Colors.TEXT,
  },
  ratingAndTotalSale: {
    flexDirection: 'row',
  },
  ratingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  totalSaleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rating: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 14,
    lineHeight: 16,
    color: Colors.SUB_TEXT,
  },
  totalSale: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 14,
    lineHeight: 16,
    color: Colors.SUB_TEXT,
  },
  price: {
    fontFamily: Fonts.PRIMARY.SEMI_BOLD,
    fontSize: 20,
    lineHeight: 24,
    color: Colors.TEXT,
  },
  stockContainer: {
    borderWidth: 2,
    borderColor: Colors.SUB_TEXT,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 15,
  },
  stockTitle: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 16,
    lineHeight: 20,
    color: Colors.SUB_TEXT,
  },
  stockNumber: {
    fontFamily: Fonts.PRIMARY.SEMI_BOLD,
    fontSize: 16,
    lineHeight: 20,
    color: Colors.TEXT,
  },
  overviewHeading: {
    fontFamily: Fonts.PRIMARY.SEMI_BOLD,
    fontSize: 20,
    lineHeight: 24,
    color: Colors.TEXT,
  },
  overview: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 14,
    lineHeight: 18,
    color: Colors.TEXT,
  },
});

export default DetailBook;
