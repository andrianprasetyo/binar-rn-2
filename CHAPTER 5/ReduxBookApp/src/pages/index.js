import Splash from './Splash';
import Login from './Login';
import Register from './Register';
import RegisterSuccess from './RegisterSuccess';
import Home from './Home';
import DetailBook from './DetailBook';
import DetailVideo from './DetailVideo';
import ListSearchBook from './ListSearchBook';
import ListSearchVideo from './ListSearchVideo';

export {
  Splash,
  Home,
  Register,
  RegisterSuccess,
  Login,
  DetailBook,
  DetailVideo,
  ListSearchBook,
  ListSearchVideo,
};
