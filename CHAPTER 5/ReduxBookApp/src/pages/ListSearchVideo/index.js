import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import {fetchSearchVideos} from '../../store/actions/videos';
import {Colors, Fonts} from '../../utils';
import {Error, Gap, SearchInput, VideoItem} from '../../components';

const ListSearchVideo = props => {
  const {navigation, route} = props;
  const {keyword} = route.params;
  const dispatch = useDispatch();
  const searchVideos = useSelector(state => state.videos.searchVideos);
  const isLoading = useSelector(state => state.videos.isLoading);
  const error = useSelector(state => state.videos.error);

  const [keywordSearch, setKeywordSearch] = useState('');

  const handleChangeSearch = event => {
    setKeywordSearch(event);
  };

  const handleSearchSubmit = () => {
    dispatch(fetchSearchVideos(keywordSearch));
  };

  const handleClickDetail = item => {
    navigation.navigate('DetailVideo', {item});
  };

  const handleRefresh = () => {
    dispatch(fetchSearchVideos(keywordSearch));
  };

  useEffect(() => {
    setKeywordSearch(keyword);
    dispatch(fetchSearchVideos(keyword));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View style={styles.page}>
      <View style={styles.header}>
        <View style={styles.headingContainer}>
          <TouchableOpacity
            accessibilityLabel="btn-back"
            style={styles.btnContainer}
            activeOpacity={0.7}
            onPress={() => {
              navigation.goBack();
            }}>
            <Icon name="chevron-back" size={24} color={Colors.SUB_TEXT} />
            <Gap width={4} />
            <Text style={styles.backBtn}>Back</Text>
          </TouchableOpacity>
        </View>
        <Gap height={20} />
        <SearchInput
          placeholder="Search Videos"
          value={keywordSearch}
          onChangeText={handleChangeSearch}
          onSubmit={handleSearchSubmit}
        />
        <Gap height={20} />
      </View>
      {isLoading ? (
        <View style={styles.page}>
          <ActivityIndicator
            style={styles.loading}
            size={'large'}
            color={Colors.FILL}
          />
        </View>
      ) : error ? (
        <Error btnTitle={'Refresh'} message={error} onPress={handleRefresh} />
      ) : searchVideos < 1 ? (
        <View style={styles.emptyContainer}>
          <Icon name="information-circle" size={60} color={Colors.TEXT} />
          <Gap height={12} />
          <Text style={styles.emptyText}>
            Looks like videos what you search is empty
          </Text>
        </View>
      ) : (
        <FlatList
          data={searchVideos}
          renderItem={({item}) => (
            <VideoItem
              key={item.id.videoId}
              title={item.snippet.title}
              channelTitle={item.snippet.channelTitle}
              thumbnail={item.snippet.thumbnails.high.url}
              onPress={() => handleClickDetail(item)}
            />
          )}
          keyExtractor={item => item.etag}
        />
      )}
    </View>
  );
};

export default ListSearchVideo;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
  },
  header: {
    padding: 20,
    backgroundColor: Colors.BACKGROUND,
    shadowColor: Colors.SUB_TEXT,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.22,
    shadowRadius: 2,
    elevation: 3,
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headingContainer: {
    backgroundColor: Colors.WHITE,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  btnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  backBtn: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 17,
    lineHeight: 20,
    color: Colors.SUB_TEXT,
  },
  emptyContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
