import Axios from 'axios';

const apiVideos = Axios.create({
  baseURL: 'https://youtube.googleapis.com/youtube/v3',
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

export default apiVideos;
