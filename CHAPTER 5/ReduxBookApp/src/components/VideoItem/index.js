import React from 'react';
import {StyleSheet, Text, TouchableOpacity, Image, View} from 'react-native';

import {Colors, Fonts} from '../../utils';
import Gap from '../Gap';

const VideoItem = props => {
  const {title, thumbnail, channelTitle, onPress} = props;
  return (
    <TouchableOpacity
      accessibilityLabel={`videoItem-${channelTitle}`}
      style={styles.container}
      activeOpacity={0.7}
      onPress={onPress}>
      <Image source={{uri: thumbnail}} style={styles.thumbnail} />
      <View style={styles.contentContainer}>
        <Text style={styles.title}>{title}</Text>
        <Gap height={8} />
        <Text style={styles.channelTitle}>{channelTitle}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {},
  thumbnail: {
    width: '100%',
    height: 215,
  },
  contentContainer: {
    paddingVertical: 20,
    paddingHorizontal: 12,
  },
  title: {
    fontFamily: Fonts.PRIMARY.SEMI_BOLD,
    fontSize: 14,
    lineHeight: 17,
    color: Colors.TEXT,
  },
  channelTitle: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 12,
    lineHeight: 15,
    color: Colors.SUB_TEXT,
  },
});

export default VideoItem;
