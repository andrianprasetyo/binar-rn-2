import React from 'react';
import propTypes from 'prop-types';
import {StyleSheet, TextInput, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {Colors, Fonts} from '../../utils';
import Gap from '../Gap';

const SearchInput = props => {
  const {value, placeholder, onSubmit, onChangeText} = props;
  return (
    <View style={styles.searchContainer}>
      <Icon name="search" size={24} color={Colors.TEXT} />
      <Gap width={5} />
      <TextInput
        accessibilityLabel="searchInput"
        value={value}
        onChangeText={onChangeText}
        style={styles.searchInput}
        placeholder={placeholder}
        onSubmitEditing={onSubmit}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  searchContainer: {
    width: '100%',
    backgroundColor: Colors.PLACEHOLDER,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    borderRadius: 10,
  },
  searchInput: {
    width: '90%',
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 16,
    color: Colors.TEXT,
    lineHeight: 20,
  },
});

SearchInput.propTypes = {
  placeholder: propTypes.string,
  onSubmit: propTypes.func,
  value: propTypes.string,
  onChangeText: propTypes.func,
};

export default SearchInput;
