import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useDispatch} from 'react-redux';
import {authLogout} from '../../store/actions/users';
import {clearBooks} from '../../store/actions/books';
import {Colors, Fonts, showSuccess} from '../../utils';
import Icon from 'react-native-vector-icons/Ionicons';
import Gap from '../Gap';
import SearchInput from '../SearchInput';

const Header = props => {
  const {user} = props;
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const handleSubmitSearch = ({nativeEvent}) => {
    navigation.navigate('ListSearchBook', {keyword: nativeEvent.text});
  };
  const handleLogout = () => {
    dispatch(authLogout());
    dispatch(clearBooks());
    showSuccess("You're logged out");
    navigation.reset({index: 0, routes: [{name: 'Login'}]});
  };
  const handleSearchVideos = () => {
    navigation.navigate('ListSearchVideo', {keyword: ''});
  };
  return (
    <View style={styles.headerContainer}>
      <View style={styles.greetingContainer}>
        <Text style={styles.greeting}>Hello, {user}</Text>

        <View style={styles.btnContainer}>
          <TouchableOpacity
            accessibilityLabel={'button-search-videos'}
            onPress={() => handleSearchVideos()}
            activeOpacity={0.7}>
            <Icon name="videocam" size={36} color={Colors.TEXT} />
          </TouchableOpacity>
          <Gap width={12} />
          <TouchableOpacity
            accessibilityLabel={'button-logout'}
            onPress={() => handleLogout()}
            activeOpacity={0.7}>
            <Icon name="log-out" size={36} color={Colors.TEXT} />
          </TouchableOpacity>
        </View>
      </View>
      <Gap height={20} />
      <SearchInput placeholder="Search Books" onSubmit={handleSubmitSearch} />
    </View>
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    paddingHorizontal: 20,
    paddingVertical: 30,
    minHeight: 150,
    backgroundColor: Colors.BACKGROUND,
    shadowColor: Colors.SUB_TEXT,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.22,
    shadowRadius: 2,
    elevation: 3,
  },
  greetingContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  greeting: {
    fontFamily: Fonts.PRIMARY.BOLD,
    fontSize: 22,
    color: Colors.TEXT,
    lineHeight: 27,
  },
  btnContainer: {
    flexDirection: 'row',
  },
});

export default Header;
