import {combineReducers} from 'redux';
import books from './books';
import users from './users';
import videos from './videos';

export default combineReducers({
  books,
  users,
  videos,
});
