import {
  REQUEST_FETCH,
  FETCH_SEARCH_VIDEOS_FAILURE,
  FETCH_SEARCH_VIDEOS_SUCCESS,
  FETCH_DETAIL_VIDEO_FAILURE,
  FETCH_DETAIL_VIDEO_SUCCESS,
  CLEAR_VIDEOS,
} from '../types';

const initialState = {
  searchVideos: [],
  detailVideo: {},
  error: null,
  isLoading: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case REQUEST_FETCH:
      return {
        ...state,
        isLoading: true,
      };

    case FETCH_SEARCH_VIDEOS_SUCCESS:
      return {
        ...state,
        searchVideos: action.payload,
        isLoading: false,
        error: null,
      };

    case FETCH_SEARCH_VIDEOS_FAILURE:
      return {
        ...state,
        error: action.error,
        isLoading: false,
      };

    case FETCH_DETAIL_VIDEO_SUCCESS:
      return {
        ...state,
        searchVideos: action.payload,
        isLoading: false,
        error: null,
      };

    case FETCH_DETAIL_VIDEO_FAILURE:
      return {
        ...state,
        error: action.error,
        isLoading: false,
      };

    case CLEAR_VIDEOS:
      return initialState;

    default:
      return state;
  }
}
