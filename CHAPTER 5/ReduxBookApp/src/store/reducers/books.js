import {
  REQUEST_FETCH,
  FETCH_BOOKS_FAILURE,
  FETCH_BOOKS_SUCCESS,
  FETCH_SEARCH_BOOKS_SUCCESS,
  FETCH_SEARCH_BOOKS_FAILURE,
  FETCH_DETAIL_BOOK_FAILURE,
  FETCH_DETAIL_BOOK_SUCCESS,
  CLEAR_BOOKS,
} from '../types';

const initialState = {
  books: [],
  searchBook: [],
  detailBook: {},
  error: null,
  isLoading: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case REQUEST_FETCH:
      return {
        ...state,
        isLoading: true,
      };

    case FETCH_BOOKS_SUCCESS:
      return {
        ...state,
        books: action.payload,
        isLoading: false,
        error: null,
      };

    case FETCH_BOOKS_FAILURE:
      return {
        ...state,
        error: action.error,
        isLoading: false,
      };

    case FETCH_SEARCH_BOOKS_SUCCESS:
      return {
        ...state,
        searchBook: action.payload,
        isLoading: false,
        error: null,
      };

    case FETCH_SEARCH_BOOKS_FAILURE:
      return {
        ...state,
        error: action.error,
        isLoading: false,
      };

    case FETCH_DETAIL_BOOK_SUCCESS:
      return {
        ...state,
        detailBook: action.payload,
        isLoading: false,
        error: null,
      };

    case FETCH_DETAIL_BOOK_FAILURE:
      return {
        ...state,
        error: action.error,
        isLoading: false,
      };

    case CLEAR_BOOKS:
      return initialState;

    default:
      return state;
  }
}
