import {
  REQUEST_FETCH,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGIN_FAILURE,
  AUTH_REGISTER_SUCCESS,
  AUTH_REGISTER_FAILURE,
  AUTH_LOGOUT,
} from '../types';

const initialState = {
  users: {},
  tokens: {},
  message: '',
  error: null,
  isLoggedIn: false,
  isLoading: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case REQUEST_FETCH:
      return {
        ...state,
        isLoading: true,
      };

    case AUTH_REGISTER_SUCCESS:
      return {
        ...state,
        users: action.payload.data,
        message: action.payload.message,
        error: null,
        isLoading: false,
      };

    case AUTH_REGISTER_FAILURE:
      return {
        ...state,
        error: action.error,
        isLoading: false,
      };

    case AUTH_LOGIN_SUCCESS:
      return {
        ...state,
        users: action.payload.user,
        tokens: action.payload.tokens,
        error: null,
        isLoggedIn: true,
        isLoading: false,
      };

    case AUTH_LOGIN_FAILURE:
      return {
        ...state,
        error: action.error,
        isLoading: false,
      };

    case AUTH_LOGOUT:
      return initialState;

    default:
      return state;
  }
}
