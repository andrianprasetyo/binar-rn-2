import {createStore, applyMiddleware} from 'redux';

import ReduxThunk from 'redux-thunk';

import ReduxLogger from 'redux-logger';

import AsyncStorage from '@react-native-async-storage/async-storage';
import {persistStore, persistReducer} from 'redux-persist';

import rootReducer from './reducers';

const middleware = [ReduxThunk];

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const configPersist = persistReducer(persistConfig, rootReducer);

export const Store = createStore(configPersist, applyMiddleware(...middleware));

Store.subscribe(() => Store.getState());

export const Persistor = persistStore(Store);
