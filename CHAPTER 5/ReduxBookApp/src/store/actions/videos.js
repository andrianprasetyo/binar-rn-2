import {
  REQUEST_FETCH,
  FETCH_SEARCH_VIDEOS_FAILURE,
  FETCH_SEARCH_VIDEOS_SUCCESS,
  FETCH_DETAIL_VIDEO_FAILURE,
  FETCH_DETAIL_VIDEO_SUCCESS,
  CLEAR_VIDEOS,
} from '../types';

import apiVideos from '../../services/apiVideos';
import {showError} from '../../utils';

const KEY = 'AIzaSyDP3m01VRAjztvkiiXpKfN-eQLycPC9qb4';

/*
  CREATOR START
*/
export const requestFetch = () => {
  return {
    type: REQUEST_FETCH,
  };
};

export const fetchSearchVideosSuccess = data => {
  return {
    type: FETCH_SEARCH_VIDEOS_SUCCESS,
    payload: data,
    error: null,
  };
};

export const fetchSearchVideosFailure = error => {
  return {
    type: FETCH_SEARCH_VIDEOS_FAILURE,
    error,
  };
};

export const fetchDetailVideoSuccess = data => {
  return {
    type: FETCH_DETAIL_VIDEO_SUCCESS,
    payload: data,
    error: null,
  };
};

export const fetchDetailVideoFailure = error => {
  return {
    type: FETCH_DETAIL_VIDEO_FAILURE,
    error,
  };
};

export const clearVideos = () => {
  return {
    type: CLEAR_VIDEOS,
  };
};

/*
  ACTION START
*/
export const fetchSearchVideos = keyword => {
  return async dispatch => {
    dispatch(requestFetch());
    try {
      const response = await apiVideos.get(
        `/search?part=snippet&channelType=any&maxResults=50&order=relevance&q=${keyword}&regionCode=ID&videoEmbeddable=any&prettyPrint=true&key=${KEY}`,
      );

      if (response.status === 200) {
        dispatch(fetchSearchVideosSuccess(response.data?.items));
      } else if (response.data) {
        showError(response.data.message);
      } else {
        showError(response);
      }
    } catch (error) {
      if (error.response) {
        showError(error.response.data.error.message);
        dispatch(fetchSearchVideosFailure(error.response.data.error.message));
        return error.response;
      } else {
        showError(error);
        dispatch(fetchSearchVideosFailure(error));
        return error;
      }
    }
  };
};
