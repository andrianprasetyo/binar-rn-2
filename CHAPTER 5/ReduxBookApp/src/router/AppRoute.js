import React from 'react';

import {createNativeStackNavigator} from '@react-navigation/native-stack';

import {
  Splash,
  Register,
  RegisterSuccess,
  Login,
  Home,
  DetailBook,
  DetailVideo,
  ListSearchBook,
  ListSearchVideo,
} from '../pages';

const Stack = createNativeStackNavigator();

const AppRoute = () => {
  return (
    <Stack.Navigator
      initialRouteName="Splash"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="Splash" component={Splash} />
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="DetailBook" component={DetailBook} />
      <Stack.Screen name="DetailVideo" component={DetailVideo} />
      <Stack.Screen name="ListSearchBook" component={ListSearchBook} />
      <Stack.Screen name="ListSearchVideo" component={ListSearchVideo} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="RegisterSuccess" component={RegisterSuccess} />
      <Stack.Screen name="Login" component={Login} />
    </Stack.Navigator>
  );
};

export default AppRoute;
