const mainColors = {
  red: {
    light: '#ff3b30',
    dark: '#ff473a',
  },
  orange: {
    light: '#ff9500',
    dark: '#ff9f0a',
  },
  green: {
    light: '#34c759',
    dark: '#30d158',
  },
  cyan: {
    light: '#32ade6',
    dark: '#64d2ff',
  },
  blue: {
    light: '#007aff',
    dark: '#0a84ff',
  },
  grey: {
    light: {
      1: '#6c6c70',
      2: '#8e8e93',
      3: '#aeaeb2',
      4: '#bcbcc0',
      5: '#d8d8dc',
      6: '#ebebf0',
    },
    dark: {
      1: '#8e8e93',
      2: '#636366',
      3: '#48484a',
      4: '#3a3a3c',
      5: '#2c2c2e',
      6: '#1c1c1e',
    },
  },
  white: '#FFFFFF',
  black: '#000000',
};

export const Colors = {
  TEXT: mainColors.grey.dark[6],
  SUB_TEXT: mainColors.grey.light[2],
  FILL: mainColors.blue.dark,
  DISABLE: mainColors.grey.light[2],
  PLACEHOLDER: mainColors.grey.light[6],
  BACKGROUND: mainColors.white,
  WHITE: mainColors.white,
  ERROR: mainColors.red.dark,
  SUCCESS: mainColors.green.dark,
  STARS: mainColors.orange.light,
};
