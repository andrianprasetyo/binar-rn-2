import React from 'react';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/lib/integration/react';

import {Store, Persistor} from '../../store';

export default function ContainerTesting(
  component,
  store = Store,
  persistor = Persistor,
) {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        {component}
      </PersistGate>
    </Provider>
  );
}
