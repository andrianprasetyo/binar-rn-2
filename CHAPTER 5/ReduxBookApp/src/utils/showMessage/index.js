import {showMessage} from 'react-native-flash-message';
import {Colors} from '../colors';

export const showSuccess = message =>
  showMessage({
    duration: 4000,
    hideOnPress: true,
    message: message,
    type: 'default',
    backgroundColor: Colors.SUCCESS,
    color: Colors.WHITE,
  });

export const showError = message =>
  showMessage({
    duration: 4000,
    hideOnPress: true,
    message: message,
    type: 'default',
    backgroundColor: Colors.ERROR,
    color: Colors.WHITE,
  });
