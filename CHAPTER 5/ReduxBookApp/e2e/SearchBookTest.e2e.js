/* eslint-disable eslint-comments/no-unlimited-disable */
/* eslint-disable */
import {device, element} from 'detox';

describe('Search Book Test Case', () => {
  const dataUser = {
    email: 'hexivib245@karavic.com',
    password: '!Isthatyou99',
  };

  const component = {
    searchInput: element(by.label('searchInput')),
    bookItem: element(by.text('Harry')),
    email: element(by.label('Email')),
    password: element(by.label('Password')),
    btnBack: element(by.text('Back')),
    btnLogin: element(by.text('Login')),
    btnCTALogin: element(by.text('Login')),
  };

  beforeAll(async () => {
    await device.launchApp().then(async () => {
      await component.btnCTALogin.tap();
      await component.email.replaceText(dataUser.email.toLowerCase());
      await component.password.typeText(dataUser.password);
      await component.btnLogin.tap();
    });
  });

  describe('Search book with specific keyword ', () => {
    it('should render empty list book with keyword "Unknown"', async () => {
      await expect(component.searchInput).toBeVisible();
      await component.searchInput.typeText('Unknown');
      await expect(component.bookItem).not.toBeVisible();
      await component.searchInput.clearText();
    });

    it('should render list book with keyword "Harry"', async () => {
      await expect(component.searchInput).toBeVisible();
      await component.searchInput.replaceText('Harry');
      await expect(component.bookItem).toBeVisible();
    });
  });
});
