
# Redux Book App

This Project was created to fulfill the challenge of Binar Academy React Native - Chapter 4


## Screenshots

![Highlight](src/assets/screenshots/Highlights.gif)


## Installation & Run The Project

1. Install NPM Package

```bash
  npm install
```

2. Run The Metro Server (Bundler)

```bash
  npm run start
```

3. Run Android Application

```bash
  npm run android
```
    