import {useCallback, useReducer, useRef} from 'react';

import useSafeDispatch from './useSafeDispatch';

const defaultState = {
  data: null,
  status: 'idle',
  error: null,
};

export default function useAsync(initialState) {
  const initialStateRef = useRef({
    ...defaultState,
    ...initialState,
  });

  const [{data, status, error}, setState] = useReducer((state, action) => {
    return {...state, ...action};
  }, initialStateRef.current);

  const safeSetState = useSafeDispatch(setState);

  const execute = useCallback(
    promise => {
      if (!promise || !promise.then) {
        throw new Error(
          'The argument passed to useAsync().execute must be a promise',
        );
      }

      safeSetState({status: 'pending'});

      return promise
        .then(response => {
          safeSetState({data: response.data, status: 'resolved'});

          return response.data;
        })
        .catch(err => {
          safeSetState({
            status: 'rejected',
            error: err,
          });
        });
    },
    [safeSetState],
  );

  const setData = useCallback(
    dataParams => {
      safeSetState({data: dataParams});
    },
    [safeSetState],
  );

  const setError = useCallback(
    err => {
      safeSetState({error: err});
    },
    [safeSetState],
  );

  const reset = useCallback(() => {
    safeSetState(initialStateRef.current);
  }, [safeSetState]);

  return {
    execute,
    data,
    status,
    error,
    setData,
    setError,
    reset,
    isIdle: status === 'idle',
    isLoading: status === 'idle' || status === 'pending',
    isError: status === 'rejected',
    isSuccess: status === 'resolved',
  };
}
