import Axios from 'axios';

const apiClient = Axios.create({
  baseURL: 'http://code.aldipee.com/api/v1',
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

export default apiClient;
