import React, {useEffect, Fragment} from 'react';

import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  ActivityIndicator,
  FlatList,
  Dimensions,
} from 'react-native';

import {ScrollView as ScrollViewNested} from 'react-native-virtualized-view';

import {RecommendedMovieItem, Gap, MovieItem} from '../../components';
import {useAsync} from '../../hooks';
import apiClient from '../../services/api';

import {Colors, Fonts} from '../../utils';

const {width} = Dimensions.get('window');

const Home = props => {
  const {navigation} = props;
  const {execute, data, isLoading, isError} = useAsync();

  useEffect(() => {
    execute(apiClient.get('/movies'));
  }, [execute]);

  const handleClick = idMovie => {
    navigation.push('DetailMovie', {idMovie});
  };

  const renderRecommendMovie = () => {
    const recommendData = [];

    data?.results?.map((item, index) => {
      if (index < 5) {
        recommendData.push(item);
      }
    });

    return isLoading ? (
      <View style={styles.loadingRecommended}>
        <ActivityIndicator size="large" color={Colors.HIGHLIGHT} />
      </View>
    ) : isError ? (
      <Text style={styles.errorTitle}>Oops... Something has wrong</Text>
    ) : (
      recommendData.map(item => {
        return (
          <Fragment key={item.id}>
            <RecommendedMovieItem
              key={item.id}
              title={item.title}
              imageUrl={item.poster_path}
              onPress={() => handleClick(item.id)}
            />
            <Gap width={15} height={15} />
          </Fragment>
        );
      })
    );
  };

  const renderLatestMovie = () => {
    const latestMovie = [];

    data?.results?.map((item, index) => {
      if (index > 5) {
        latestMovie.push(item);
      }
    });

    return isLoading ? (
      <View style={styles.loadingLatest}>
        <ActivityIndicator size="large" color={Colors.HIGHLIGHT} />
      </View>
    ) : isError ? (
      <Text style={styles.errorTitle}>Oops... Something has wrong</Text>
    ) : (
      <FlatList
        data={latestMovie}
        renderItem={MovieItem}
        keyExtractor={item => item.id}
      />
    );
  };

  return (
    <ScrollViewNested style={styles.page}>
      <Text style={styles.heading}>Recommended</Text>
      <Gap height={30} />
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        {renderRecommendMovie()}
      </ScrollView>
      <Gap height={30} />
      <Text style={styles.heading}>Latest Movie</Text>
      <Gap height={30} />
      <View style={styles.containerListMovie}>{renderLatestMovie()}</View>
    </ScrollViewNested>
  );
};

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
    padding: 20,
  },
  loadingRecommended: {
    width: width * 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loadingLatest: {
    width: width * 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  heading: {
    fontFamily: Fonts.PRIMARY.BOLD,
    color: Colors.PRIMARY,
    fontSize: 24,
  },
  containerListMovie: {
    flex: 1,
  },
  errorTitle: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    color: Colors.PRIMARY,
    fontSize: 12,
  },
});

export default Home;
