import Splash from './Splash';
import Home from './Home';
import DetailMovie from './DetailMovie';

export {Splash, Home, DetailMovie};
