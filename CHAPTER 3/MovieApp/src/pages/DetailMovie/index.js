import React, {useEffect, Fragment} from 'react';

import {
  Alert,
  Image,
  View,
  Text,
  Dimensions,
  ImageBackground,
  ScrollView,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
  Share,
} from 'react-native';
import {BaseButton, Gap} from '../../components';

import {useAsync} from '../../hooks';
import apiClient from '../../services/api';
import {Colors, Fonts, secondsToTime} from '../../utils';

const {width, height} = Dimensions.get('window');

const DetailMovie = props => {
  const {route, navigation} = props;

  const {execute, data, isLoading, isError} = useAsync();

  useEffect(() => {
    execute(apiClient.get(`/movies/${route.params.idMovie}`));
  }, [execute, route.params.idMovie]);

  const onShare = async () => {
    try {
      const result = await Share.share({
        title: 'Share Movie',
        message: 'Movie App | Hey please checkout this movie',
        url: data.poster_path,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }

        /*
        Example Direct WhatsApp Share

        Linking.openURL(
          `https://api.whatsapp.com/send?text=Hey%20please%20check%20out%20this%20movie%2C%0A${data.poster_path}`,
        ).then(res => console.log(res));
      */
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (err) {
      // eslint-disable-next-line no-alert
      alert(err.message);
    }
  };

  const renderDetailMovie = () => {
    return (
      <Fragment>
        <ImageBackground
          style={styles.backgroundImage}
          source={{uri: data.backdrop_path}}
        >
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.btnContainer}
            onPress={() => {
              navigation.goBack();
            }}
          >
            <Text style={styles.titleBtn}>Back</Text>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.btnContainer}
            onPress={() => {
              Alert.alert('Movie App', `${data.original_title} Liked!`);
            }}
          >
            <Text style={styles.titleBtn}>Like</Text>
          </TouchableOpacity>
        </ImageBackground>

        <View style={styles.contentContainer}>
          <View style={styles.headingContainer}>
            <View style={styles.titleContainer}>
              <Image style={styles.image} source={{uri: data.poster_path}} />
              <Gap width={14} />
              <View>
                <Text numberOfLines={3} style={styles.headingTitle}>
                  {data.original_title} ({data.release_date.split('-')[0]})
                </Text>
                <Gap height={8} />
                <Text style={styles.taglineTitle}>{data.tagline}</Text>
              </View>
            </View>
            <View style={styles.wrapperRating}>
              <Text style={styles.rating}>
                <Text style={styles.spanRating}>{data.vote_average}</Text> / 10
              </Text>
              <Text style={styles.status}>{data.status}</Text>
            </View>
          </View>

          <Gap height={24} />
          <Text style={styles.sectionHeading}>Genre : </Text>
          <Gap height={14} />
          <View style={styles.genreContainer}>
            {data.genres.map(genre => {
              return (
                <Fragment key={genre.id}>
                  <Text style={styles.genre}>{genre.name}</Text>
                  <Gap width={16} />
                </Fragment>
              );
            })}
          </View>

          <Gap height={24} />
          <Text style={styles.sectionHeading}>Available Languages : </Text>
          <Gap height={14} />
          <View style={styles.genreContainer}>
            {data.spoken_languages.map(language => {
              return (
                <Fragment key={language.english_name}>
                  <Text style={styles.language}>{language.english_name}</Text>
                  <Gap width={16} />
                </Fragment>
              );
            })}
          </View>

          <Gap height={24} />
          <Text style={styles.sectionHeading}>Length : </Text>
          <Gap height={14} />
          <Text style={styles.length}>{secondsToTime(data.runtime)}</Text>

          <Gap height={24} />
          <Text style={styles.sectionHeading}>Overview : </Text>
          <Gap height={14} />
          <Text style={styles.overview}>{data.overview}</Text>

          <Gap height={24} />
          <Text style={styles.sectionHeading}>Actors : </Text>
          <Gap height={14} />
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            {data.credits.cast.map(credit => {
              return (
                <Fragment key={credit.original_name}>
                  <View>
                    <Image
                      style={styles.image}
                      source={{uri: credit.profile_path}}
                    />
                    <Gap height={8} />
                    <Text style={styles.cast} numberOfLines={2}>
                      {credit.original_name}
                    </Text>
                  </View>
                  <Gap width={14} />
                </Fragment>
              );
            })}
          </ScrollView>

          <Gap height={24} />
          <BaseButton style={styles.btnShare} title="Share" onPress={onShare} />
          <Gap height={24} />
        </View>
      </Fragment>
    );
  };

  return (
    <ScrollView style={styles.page}>
      {isLoading ? (
        <View style={styles.loading}>
          <ActivityIndicator size="large" color={Colors.HIGHLIGHT} />
        </View>
      ) : isError ? (
        <Text style={styles.errorTitle}>Oops... Something has wrong</Text>
      ) : (
        renderDetailMovie()
      )}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
  },
  errorTitle: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    color: Colors.WHITE,
    fontSize: 12,
  },
  loading: {
    marginTop: height * 0.5,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  backgroundImage: {
    width: width * 1,
    height: height * 0.35,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
  },
  btnContainer: {
    width: 75,
    height: 36,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 24,
    borderWidth: 2,
    borderColor: Colors.HIGHLIGHT,
  },
  titleBtn: {
    color: Colors.LIGHT_GREY,
    fontFamily: Fonts.PRIMARY.BOLD,
    fontSize: 14,
  },
  contentContainer: {
    flex: 1,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: Colors.BACKGROUND,
    marginTop: -30,
    padding: 20,
  },
  headingContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleContainer: {
    flexDirection: 'row',
    maxWidth: '63%',
  },
  headingTitle: {
    fontFamily: Fonts.PRIMARY.BOLD,
    fontSize: 20,
    lineHeight: 32,
    color: Colors.PRIMARY,
  },
  taglineTitle: {
    fontFamily: Fonts.PRIMARY.LIGHT,
    color: Colors.GREY,
    fontSize: 14,
  },
  image: {
    backgroundColor: Colors.GREY,
    width: 85,
    height: 125,
    borderRadius: 8,
  },
  status: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    color: Colors.HIGHLIGHT,
    fontSize: 12,
    alignSelf: 'flex-end',
  },
  rating: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    color: Colors.GREY,
    fontSize: 14,
    alignSelf: 'flex-end',
  },
  spanRating: {
    fontFamily: Fonts.PRIMARY.BOLD,
    color: Colors.PRIMARY,
    fontSize: 14,
  },
  genreContainer: {
    flexDirection: 'row',
    alignItems: 'baseline',
  },
  sectionHeading: {
    fontFamily: Fonts.PRIMARY.BOLD,
    color: Colors.PRIMARY,
    fontSize: 14,
  },
  genre: {
    fontFamily: Fonts.PRIMARY.BOLD,
    color: Colors.HIGHLIGHT,
    fontSize: 12,
  },
  language: {
    fontFamily: Fonts.PRIMARY.LIGHT,
    color: Colors.GREY,
    fontSize: 12,
  },
  length: {
    fontFamily: Fonts.PRIMARY.LIGHT,
    color: Colors.GREY,
    fontSize: 12,
    lineHeight: 19,
  },
  overview: {
    fontFamily: Fonts.PRIMARY.LIGHT,
    color: Colors.GREY,
    fontSize: 12,
    lineHeight: 19,
  },
  cast: {
    maxWidth: 85,
    fontFamily: Fonts.PRIMARY.LIGHT,
    color: Colors.GREY,
    fontSize: 12,
    lineHeight: 16,
  },
  btnShare: {
    borderRadius: 25,
  },
});

export default DetailMovie;
