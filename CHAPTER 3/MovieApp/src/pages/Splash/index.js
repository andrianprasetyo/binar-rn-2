import React, {useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Gap} from '../../components';

import {Colors, Fonts} from '../../utils';

const Splash = props => {
  const {navigation} = props;

  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Home');
    }, 1000);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View style={styles.page}>
      <Text style={styles.title}>Movie App</Text>
      <Gap height={8} />
      <Text style={styles.subHeading}>
        The <Text style={styles.subHighlight('highlight')}>easy way</Text> to
        find your
        <Text style={styles.subHighlight('accent')}> favorite movie</Text>
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    textAlign: 'center',
    fontSize: 36,
    color: Colors.PRIMARY,
    fontFamily: Fonts.PRIMARY.BOLD,
  },
  subHeading: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    textAlign: 'center',
    fontSize: 12,
    lineHeight: 20,
    color: Colors.GREY,
  },
  subHighlight: type => ({
    color: type === 'highlight' ? Colors.HIGHLIGHT : Colors.ACCENT,
  }),
});

export default Splash;
