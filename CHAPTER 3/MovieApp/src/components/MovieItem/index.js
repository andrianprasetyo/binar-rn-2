import React, {Fragment} from 'react';
import Gap from '../Gap';

import LatestMovieItem from '../LatestMovieItem';

const MovieItem = props => {
  const {item} = props;
  return (
    <Fragment>
      <LatestMovieItem
        id={item.id}
        title={item.title}
        imageUrl={item.poster_path}
        rating={item.vote_average}
        overview={item.overview}
      />
      <Gap width={15} height={15} />
    </Fragment>
  );
};

export default MovieItem;
