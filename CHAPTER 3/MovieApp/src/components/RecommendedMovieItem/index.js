import React from 'react';
import propTypes from 'prop-types';

import {Text, TouchableOpacity, Image, StyleSheet} from 'react-native';

import {Colors, Fonts} from '../../utils';

const RecommendedMovieItem = props => {
  const {title, imageUrl, onPress} = props;
  return (
    <TouchableOpacity onPress={onPress} activeOpacity={0.8}>
      <Image style={styles.images} source={{uri: imageUrl}} />
      <Text numberOfLines={1} ellipsizeMode="tail" style={styles.title}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  images: {
    height: 210,
    width: 150,
    borderRadius: 5,
  },
  title: {
    maxWidth: 150,
    paddingTop: 10,
    fontFamily: Fonts.PRIMARY.REGULAR,
    color: Colors.PRIMARY,
    fontSize: 14,
    overflow: 'hidden',
  },
});

RecommendedMovieItem.propTypes = {
  title: propTypes.string.isRequired,
  imageUrl: propTypes.string.isRequired,
  onPress: propTypes.func.isRequired,
};

export default RecommendedMovieItem;
