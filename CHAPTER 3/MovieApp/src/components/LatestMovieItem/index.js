import React from 'react';
import propTypes from 'prop-types';

import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
} from 'react-native';

import {useNavigation} from '@react-navigation/native';

import {Colors, Fonts} from '../../utils';

const {width} = Dimensions.get('window');

const LatestMovieItem = props => {
  const navigation = useNavigation();

  const {id, title, imageUrl, rating, overview} = props;

  const handleClick = idMovie => {
    navigation.navigate('DetailMovie', {idMovie});
  };

  return (
    <TouchableOpacity
      onPress={() => handleClick(id)}
      activeOpacity={0.8}
      style={styles.container}
    >
      <View style={styles.imageContainer}>
        <Image
          style={styles.image}
          source={{uri: imageUrl}}
          resizeMode="center"
        />
      </View>
      <View style={styles.contentContainer}>
        <Text style={styles.title} numberOfLines={1} ellipsizeMode="tail">
          {title}
        </Text>
        <Text style={styles.rating}>{rating}/10</Text>
        <Text style={styles.overview} numberOfLines={2} ellipsizeMode="tail">
          {overview}
        </Text>
        <Text style={styles.cta}>See More</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
  },
  image: {
    width: 85,
    height: 125,
    borderRadius: 8,
  },
  contentContainer: {
    paddingHorizontal: 16,
    justifyContent: 'center',
  },
  title: {
    width: width * 0.65,
    fontFamily: Fonts.PRIMARY.BOLD,
    color: Colors.PRIMARY,
    fontSize: 14,
    overflow: 'hidden',
  },
  rating: {
    paddingVertical: 8,
    fontFamily: Fonts.PRIMARY.REGULAR,
    color: Colors.GREY,
    fontSize: 12,
  },
  overview: {
    width: width * 0.65,
    fontFamily: Fonts.PRIMARY.LIGHT,
    color: Colors.GREY,
    fontSize: 12,
    lineHeight: 19,
    overflow: 'hidden',
  },
  cta: {
    paddingVertical: 6,
    fontFamily: Fonts.PRIMARY.REGULAR,
    color: Colors.HIGHLIGHT,
    fontSize: 12,
  },
});

LatestMovieItem.propTypes = {
  id: propTypes.oneOfType([propTypes.string, propTypes.number]),
  title: propTypes.string.isRequired,
  imageUrl: propTypes.string.isRequired,
  onPress: propTypes.func,
  rating: propTypes.oneOfType([propTypes.string, propTypes.number]),
  overview: propTypes.string.isRequired,
};

export default LatestMovieItem;
