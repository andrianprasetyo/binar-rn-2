import React from 'react';
import propTypes from 'prop-types';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';
import {Colors, Fonts} from '../../utils';

const BaseButton = props => {
  const {title, onPress, style} = props;
  return (
    <TouchableOpacity
      style={[styles.container(style), style]}
      onPress={onPress}
    >
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: style => ({
    width: style ? style.width : '100%',
    flexDirection: 'row',
    backgroundColor: Colors.HIGHLIGHT,
    borderRadius: style ? style.borderRadius : 5,
    justifyContent: 'center',
    alignItems: 'center',
  }),
  title: {
    fontFamily: Fonts.PRIMARY.BOLD,
    fontSize: 14,
    paddingVertical: 6,
    paddingHorizontal: 16,
    color: Colors.WHITE,
  },
});

BaseButton.propTypes = {
  title: propTypes.string,
  styles: propTypes.object,
  onPress: propTypes.func.isRequired,
};

export default BaseButton;
