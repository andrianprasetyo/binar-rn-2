export const Fonts = {
  PRIMARY: {
    THIN: 'Inter-Thin',
    LIGHT: 'Inter-Light',
    REGULAR: 'Inter-Regular',
    BOLD: 'Inter-Bold',
  },
};
