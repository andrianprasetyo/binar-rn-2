export function secondsToTime(time) {
  const hours = Math.floor((time % 3600) / 60).toString();

  const minutes = Math.floor(time % 60).toString();

  return `${hours}h ${minutes}min`;
}
