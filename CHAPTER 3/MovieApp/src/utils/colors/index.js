const mainColors = {
  darkBlue: '#211D27',
  blue: '#0066CC',
  orange: '#E6444F',
  alert: {
    danger: '#FA2C5A',
    success: '#73CA5C',
    warning: '#F9CC00',
  },
  neutral: {
    white: '#FFFFFF',
    black: '#000000',
    grey: '#858488',
    lightGrey: '#F5F5F5',
  },
};

export const Colors = {
  PRIMARY: mainColors.darkBlue,
  ACCENT: mainColors.orange,
  HIGHLIGHT: mainColors.blue,
  BACKGROUND: mainColors.neutral.lightGrey,
  WHITE: mainColors.neutral.white,
  BLACK: mainColors.neutral.black,
  GREY: mainColors.neutral.grey,
  LIGHT_GREY: mainColors.neutral.lightGrey,
};
