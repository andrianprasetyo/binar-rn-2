import React from 'react';
import {View, Alert} from 'react-native';
import {useIsFocused} from '@react-navigation/native';

import {CameraScreen} from 'react-native-camera-kit';

const QRCode = () => {
  const isFocused = useIsFocused();

  const onBottomButtonPressed = event => {
    const captureImages = JSON.stringify(event.captureImages);
    Alert.alert(
      `"${event.type}" Button Pressed`,
      `${captureImages}`,
      [{text: 'OK', onPress: () => console.log('OK Pressed')}],
      {cancelable: false},
    );
  };
  return (
    <View style={{height: 740}}>
      {isFocused && (
        <CameraScreen
          actions={{rightButtonText: 'Done', leftButtonText: 'Cancel'}}
          onBottomButtonPressed={event => onBottomButtonPressed(event)}
          hideControls={false} // (default false) optional, hides camera controls
          showCapturedImageCount={false} // (default false) optional, show count for photos taken during that capture session
          scanBarcode={true}
          flashImages={{
            // optional, images for flash state
            on: require('../../assets/images/flashon.png'),
            off: require('../../assets/images/flashoff.png'),
            auto: require('../../assets/images/flashauto.png'),
          }}
          cameraFlipImage={require('../../assets/images/flip.png')} // optional, image for flipping camera button
          captureButtonImage={require('../../assets/images/capture.png')} // optional, image capture button
          torchOnImage={require('../../assets/images/icon.png')} // optional, image for toggling on flash light
          torchOffImage={require('../../assets/images/icon.png')} // optional, image for toggling off flash light
          onReadCode={event => Alert.alert(event.nativeEvent.codeStringValue)}
          showFrame={true} // (default false) optional, show frame with transparent layer (qr code or barcode will be read on this area ONLY), start animation for scanner,that stoped when find any code. Frame always at center of the screen
          laserColor="red" // (default red) optional, color of laser in scanner frame
          frameColor="white" // (default white) optional, color of border of scanner frame
        />
      )}
    </View>
  );
};

export default QRCode;
