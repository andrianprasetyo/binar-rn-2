import React, {useEffect} from 'react';
import {View, StyleSheet, Alert} from 'react-native';

import {Maps} from '../../components';
import {Colors} from '../../utils';

import messaging from '@react-native-firebase/messaging';

const Home = () => {
  useEffect(() => {
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
    });

    return unsubscribe;
  }, []);

  return (
    <View style={styles.screen}>
      <Maps />
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
  },
});

export default Home;
