import Home from './Home';
import QRCode from './QRCode';
import Login from './Login';
import Splash from './Splash';

export {Home, QRCode, Login, Splash};
