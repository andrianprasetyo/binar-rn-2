import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, Alert} from 'react-native';
import {BaseButton, BaseInput, Gap} from '../../components';
import {Colors, Fonts, navigate, useForm} from '../../utils';

import ReactNativeBiometrics from 'react-native-biometrics';

import auth from '@react-native-firebase/auth';

import analytics from '@react-native-firebase/analytics';

import crashlytics from '@react-native-firebase/crashlytics';

import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';

const Login = () => {
  const [isLoading, setIsLoading] = useState(false);

  const [isBiometricAvailable, setIsBiometricAvailable] = useState(true);

  const [formValue, setFormValue] = useForm({
    email: '',
    password: '',
  });

  GoogleSignin.configure({
    webClientId:
      '1011918496619-ftdj9h5ber36l643jrap3u8dcg0jphv2.apps.googleusercontent.com',
  });

  const handleSubmit = async () => {
    if (formValue.email !== '' && formValue.password !== '') {
      setIsLoading(true);
      auth()
        .createUserWithEmailAndPassword(formValue.email, formValue.password)
        .then(async response => {
          setIsLoading(false);
          console.log('User account created & signed in!');
          await Promise.all([
            analytics().logEvent('login', {
              uid: response.user.uid,
              email: response.user.email,
            }),
          ]);
          navigate('Main');
        })
        .catch(error => {
          setIsLoading(false);
          if (error.code === 'auth/email-already-in-use') {
            return Alert.alert('That email address is already in use!');
          }

          if (error.code === 'auth/invalid-email') {
            return Alert.alert('That email address is invalid!');
          }

          Alert.alert('Error:', error.message);
        });
    } else {
      Alert.alert('Please complete all forms');
    }
  };

  const handleSubmitGoogle = async () => {
    try {
      await GoogleSignin.hasPlayServices();

      // Get the users ID token
      const {idToken} = await GoogleSignin.signIn();

      // Create a Google credential with the token
      const googleCredential = auth.GoogleAuthProvider.credential(idToken);

      // Sign-in the user with the credential
      return auth().signInWithCredential(googleCredential);
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        Alert.alert('Seems like play services not installed or outdated');
      } else {
        // some other error happened
        crashlytics().recordError(error);
        Alert.alert('Oops... Something has wrong. Try again later');
      }
    }
  };

  const checkAvailableBiometric = () => {
    ReactNativeBiometrics.isSensorAvailable().then(resultObject => {
      const {available, biometryType} = resultObject;
      if (available && biometryType === ReactNativeBiometrics.Biometrics) {
        console.log('Biometrics is supported');
        setIsBiometricAvailable(true);
      } else {
        console.log('Biometrics not supported');
        setIsBiometricAvailable(false);
      }
    });
  };

  const handleTouchID = async () => {
    let {success, error} = await ReactNativeBiometrics.simplePrompt({
      promptMessage: 'Sign in with Touch ID',
      cancelButtonText: 'Close',
    }).catch(err => {
      console.log('Biometric Failed : ' + err);
    });

    if (success) {
      navigate('Main');
    } else {
      console.log('user cancelled biometric prompt');
      console.log(error);
    }
  };

  useEffect(() => {
    checkAvailableBiometric();
  }, []);

  return (
    <View style={styles.screen}>
      <Gap height={28} />
      <Text style={styles.heading}>RNMedia App</Text>
      <Gap height={28} />
      <BaseInput
        label="Email"
        placeholder="Enter Email here"
        value={formValue.email}
        onChangeText={value => setFormValue('email', value)}
      />
      <Gap height={28} />
      <BaseInput
        type="password"
        label="Password"
        placeholder="Enter Password here"
        value={formValue.password}
        onChangeText={value => setFormValue('password', value)}
      />
      <Gap height={28} />
      <BaseButton
        isLoading={isLoading}
        disable={isLoading}
        title={isLoading ? 'Loading...' : 'Create Account'}
        onPress={handleSubmit}
      />
      <Gap height={28} />
      <View style={styles.socialMediaBtn}>
        {isBiometricAvailable && (
          <>
            <BaseButton
              title={'Authenticate with TouchID'}
              onPress={handleTouchID}
            />
            <Gap height={12} />
          </>
        )}
        <BaseButton
          type={'Google'}
          style={styles.googleBtn}
          title={'Continue with Google'}
          onPress={handleSubmitGoogle}
        />
        <Gap height={14} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
    padding: 20,
  },
  heading: {
    textAlign: 'center',
    fontSize: 28,
    color: Colors.TEXT,
    fontFamily: Fonts.PRIMARY.BOLD,
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  subTitle: {
    textAlign: 'center',
    fontSize: 14,
    color: Colors.SUB_TEXT,
    fontFamily: Fonts.PRIMARY.SEMI_BOLD,
  },
  socialMediaBtn: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  googleBtn: {
    borderRadius: 5,
    width: '100%',
    backgroundColor: Colors.GREEN,
  },
  facebookBtn: {
    borderRadius: 5,
    width: '100%',
    backgroundColor: Colors.FILL,
  },
});

export default Login;
