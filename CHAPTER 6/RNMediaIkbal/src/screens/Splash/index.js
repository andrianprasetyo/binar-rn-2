import React, {useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Gap} from '../../components';

import Icon from 'react-native-vector-icons/Ionicons';
import {Colors, Fonts} from '../../utils';

const Splash = props => {
  const {navigation} = props;

  useEffect(() => {
    setTimeout(() => {
      navigation.reset({index: 0, routes: [{name: 'Login'}]});
      //   usersState?.tokens?.hasOwnProperty('access')
      //     ? navigation.reset({index: 0, routes: [{name: 'Home'}]})
      //     : navigation.replace('Register');
    }, 1000);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View style={styles.page}>
      <Icon name="aperture" size={60} color={Colors.FILL} />
      <Gap height={4} />
      <Text style={styles.title}>RNMedia App</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    textAlign: 'center',
    fontSize: 34,
    color: Colors.TEXT,
    fontFamily: Fonts.PRIMARY.BOLD,
  },
});

export default Splash;
