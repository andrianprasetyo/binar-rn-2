export const Fonts = {
  PRIMARY: {
    THIN: 'Inter-Thin',
    LIGHT: 'Inter-Light',
    REGULAR: 'Inter-Regular',
    SEMI_BOLD: 'Inter-SemiBold',
    BOLD: 'Inter-Bold',
  },
};
