import React from 'react';
import {Button} from 'react-native';
import crashlytics from '@react-native-firebase/crashlytics';

const CrashlyticsExample = () => {
  return <Button title="Test Crash" onPress={() => crashlytics().crash()} />;
};

export default CrashlyticsExample;
