import React from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

import {Colors, Fonts} from '../../utils';

const BaseButton = props => {
  const {title, onPress, style, isLoading, disable, type} = props;
  return (
    <TouchableOpacity
      accessibilityLabel={`button-${title}`}
      activeOpacity={0.7}
      style={[styles.container(style), style, disable && styles.disable]}
      onPress={onPress}
      disabled={disable}>
      {isLoading && (
        <ActivityIndicator
          size="small"
          accessibilityLabel="loading"
          color={Colors.WHITE}
        />
      )}
      {type === 'Google' && (
        <Icon name="logo-google" color={Colors.WHITE} size={24} />
      )}
      {type === 'Facebook' && (
        <Icon name="ios-logo-facebook" color={Colors.WHITE} size={24} />
      )}

      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: style => ({
    width: style ? style.width : '100%',
    flexDirection: 'row',
    backgroundColor: Colors.FILL,
    borderRadius: style ? style.borderRadius : 5,
    justifyContent: 'center',
    alignItems: 'center',
  }),
  title: {
    fontFamily: Fonts.PRIMARY.BOLD,
    fontSize: 16,
    paddingVertical: 14,
    paddingHorizontal: 16,
    color: Colors.WHITE,
  },
  disable: {
    backgroundColor: Colors.DISABLE,
  },
});

export default BaseButton;
