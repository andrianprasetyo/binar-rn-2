import React, {useState, useEffect} from 'react';
import {View, Dimensions, StyleSheet, Alert} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';

import Geolocation from 'react-native-geolocation-service';

import analytics from '@react-native-firebase/analytics';

const {width, height} = Dimensions.get('window');

const Maps = () => {
  const ASPECT_RATIO = width / height;
  const LATITUDE_DELTA = 0.0043;
  const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

  const [currentPosition, setCurrentPosition] = useState({
    latitude: -6.914864,
    longitude: 107.608238,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA,
  });

  useEffect(() => {
    Geolocation.getCurrentPosition(
      position => {
        setCurrentPosition({
          ...currentPosition,
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        });

        analytics().logEvent('userLocation', {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        });
      },
      error => Alert.alert(error.message),
      {enableHighAccuracy: false, timeout: 20000},
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View style={styles.container}>
      <MapView
        provider={PROVIDER_GOOGLE}
        style={styles.maps}
        zoomEnabled
        region={{
          latitude: currentPosition.latitude,
          longitude: currentPosition.longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        }}>
        <Marker
          coordinate={{
            latitude: currentPosition.latitude,
            longitude: currentPosition.longitude,
          }}
          title="Current Position"
          description="This is current position"
        />
      </MapView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: height * 1,
    width: width * 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  maps: {
    ...StyleSheet.absoluteFillObject,
  },
});

export default Maps;
