import React, {useEffect} from 'react';
import {StyleSheet, View, Alert, Button} from 'react-native';

import analytics from '@react-native-firebase/analytics';
import messaging from '@react-native-firebase/messaging';

const AnalyticsExample = () => {
  const onLogScreenView = async () => {
    try {
      await analytics().logScreenView({
        screen_name: 'Home',
        screen_class: 'Home',
      });
    } catch (error) {
      console.log(error);
    }
  };

  const getToken = async () => {
    const token = await messaging().getToken();
    Alert.alert(JSON.stringify(token));
  };

  const onSignIn = async user => {
    await Promise.all([
      analytics().setUserId(user.uid),
      analytics().setAttribute('credits', String(user.credits)),
      analytics().setAttributes({
        role: 'admin',
        followers: '13',
        email: user.email,
        username: user.username,
      }),
    ]);
  };

  useEffect(() => {
    onLogScreenView();
    getToken();

    // Foreground state messages
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
    });

    return unsubscribe;
  }, []);

  return (
    <>
      <Button
        title="Sign In"
        onPress={() => {
          onSignIn({
            uid: 'Aa0Bb1Cc2Dd3Ee4Ff5Gg6Hh7Ii8Jj9',
            username: 'User Test',
            email: 'usertest@example.com',
            credits: 99,
          });
          console.log('CLICKED');
        }}
      />

      <Button
        title="Add To Basket"
        onPress={async () =>
          await analytics().logEvent('basket', {
            id: 3745092,
            item: 'mens grey t-shirt',
            description: ['round neck', 'long sleeved'],
            size: 'L',
          })
        }
      />
    </>
  );
};

export default AnalyticsExample;

const styles = StyleSheet.create({});
