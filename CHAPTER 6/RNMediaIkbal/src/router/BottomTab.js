import React from 'react';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';

import {Home, QRCode} from '../screens';
import {Colors, Fonts} from '../utils';

const Tab = createBottomTabNavigator();

const BottomTab = () => {
  const RenderIcon = ({name, focused}) => (
    <Icon
      name={name}
      color={focused ? Colors.FILL : Colors.SUB_TEXT}
      size={24}
    />
  );

  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle: {
          backgroundColor: 'white',
          height: 60,
          paddingVertical: 4,
        },
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: ({focused}) => (
            <RenderIcon name="home" focused={focused} />
          ),
          title: 'Home',
          tabBarLabelStyle: {
            fontSize: 10,
            fontFamily: Fonts.PRIMARY.SEMI_BOLD,
          },
        }}
      />
      <Tab.Screen
        name="QRCode"
        component={QRCode}
        options={{
          tabBarIcon: ({focused}) => (
            <RenderIcon name="qr-code" focused={focused} />
          ),
          title: 'QRCode',
          tabBarLabelStyle: {
            fontSize: 10,
            fontFamily: Fonts.PRIMARY.SEMI_BOLD,
          },
        }}
      />
    </Tab.Navigator>
  );
};

export default BottomTab;
