import IconBack from './ic-back.svg';
import IconBackWhite from './ic-back-white.svg';
import IconBtnSend from './ic-btn-send.svg';
import IconClose from './ic-close.svg';
import IconGoogle from './ic-google.svg';
import IconNotification from './ic-notification.svg';
import IconSearch from './ic-search.svg';
import IconEye from './ic-eye.svg';
import IconEyeActive from './ic-eye-active.svg';
import IconLogout from './ic-logout.svg';
import IconEdit from './ic-edit.svg';
import IconShare from './ic-share.svg';
import IconPokemon from './ic-pokemon.svg';
import IconPokeball from './ic-pokeball.svg';
import IconPokeballActive from './ic-pokeball-active.svg';
import IconPokeBag from './ic-pokebag.svg';
import IconPokeBagActive from './ic-pokebag-active.svg';
import IconPokePhone from './ic-pokephone.svg';
import IconPokePhoneActive from './ic-pokephone-active.svg';
import IconLogoPoke from './ic-logo-poke.svg';
import LogoPoke from './logo-poke.png';
import IconPokemonBig from './ic-pokemon-big.svg';
import IconPokemonBigPNG from './ic-pokemon-big.png';

export {
  IconPokemonBig,
  IconPokemonBigPNG,
  IconPokePhone,
  IconPokePhoneActive,
  IconBack,
  IconBackWhite,
  IconBtnSend,
  IconClose,
  IconGoogle,
  IconNotification,
  IconSearch,
  IconEye,
  IconEyeActive,
  IconLogout,
  IconEdit,
  IconShare,
  IconPokeball,
  IconPokeballActive,
  IconPokeBagActive,
  IconPokeBag,
  IconPokemon,
  IconLogoPoke,
  LogoPoke,
};
