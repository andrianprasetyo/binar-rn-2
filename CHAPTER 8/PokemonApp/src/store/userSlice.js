import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  user: {},
  pokeBag: [],
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser: (state, action) => {
      state.user = action.payload;
    },
    clearUser: state => {
      state.user = {};
    },
    setPokeBag: (state, action) => {
      state.pokeBag = action.payload;
    },
    addItemPokeBag: (state, action) => {
      state.pokeBag.push(action.payload);
    },
    removeItemPokeBag: (state, action) => {
      const filteredData = state.pokeBag.filter(
        item => item.name != action.payload.name,
      );

      state.pokeBag = filteredData;
    },
    clearPokeBag: state => {
      state.pokeBag = [];
    },
    clearAllUserData: state => {
      state.user = {};
      state.pokeBag = [];
    },
  },
});

export const {
  setUser,
  clearUser,
  setPokeBag,
  addItemPokeBag,
  removeItemPokeBag,
  clearPokeBag,
  clearAllUserData,
} = userSlice.actions;

export default userSlice.reducer;
