import {showMessage} from 'react-native-flash-message';
import {Colors} from './Colors';
import {Fonts} from './Fonts';

export const showSuccess = ({message, description}) => {
  return showMessage({
    message: message,
    type: 'success',
    description: description,
    titleStyle: {
      fontSize: 14,
      color: Colors.WHITE,
      fontFamily: Fonts.PRIMARY.SEMIBOLD,
    },
    textStyle: {
      fontSize: 12,
      color: Colors.WHITE,
      fontFamily: Fonts.PRIMARY.REGULAR,
    },
  });
};

export const showError = ({message, description}) => {
  return showMessage({
    message: message,
    type: 'danger',
    description: description,
    titleStyle: {
      fontSize: 14,
      color: Colors.WHITE,
      fontFamily: Fonts.PRIMARY.SEMIBOLD,
    },
    textStyle: {
      fontSize: 12,
      color: Colors.WHITE,
      fontFamily: Fonts.PRIMARY.REGULAR,
    },
  });
};
