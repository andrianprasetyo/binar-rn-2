import React from 'react';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import {Dashboard, PokeBag} from '../screens';

import {BottomTabNavigation} from '../components';

const Tab = createBottomTabNavigator();

const BottomTabs = () => {
  return (
    <Tab.Navigator
      initialRouteName="Dashboard"
      tabBar={props => <BottomTabNavigation {...props} />}
      screenOptions={{
        headerShown: false,
      }}>
      <Tab.Screen name="Dashboard" component={Dashboard} />
      <Tab.Screen name="PokeBag" component={PokeBag} />
    </Tab.Navigator>
  );
};

export default BottomTabs;
