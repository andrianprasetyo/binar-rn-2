import React from 'react';

import {NavigationContainer} from '@react-navigation/native';

import AppRoute from './AppRoute';

import FlashMessage from 'react-native-flash-message';

const Router = () => {
  return (
    <NavigationContainer>
      <AppRoute />
      <FlashMessage />
    </NavigationContainer>
  );
};

export default Router;
