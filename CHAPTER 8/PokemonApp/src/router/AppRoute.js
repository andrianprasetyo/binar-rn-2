import React from 'react';

import {createNativeStackNavigator} from '@react-navigation/native-stack';

import {Login, Register, Splash, DetailPokemon} from '../screens';

import BottomTabs from './BottomTabs';

const Stack = createNativeStackNavigator();

const AppRoute = () => {
  return (
    <Stack.Navigator
      initialRouteName="Splash"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="Splash" component={Splash} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Main" component={BottomTabs} />
      <Stack.Screen name="DetailPokemon" component={DetailPokemon} />
    </Stack.Navigator>
  );
};

export default AppRoute;
