import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  SafeAreaView,
  StatusBar,
  View,
  ScrollView,
  TouchableOpacity,
} from 'react-native';

import {Formik} from 'formik';

import {object, string, ref} from 'yup';

import auth from '@react-native-firebase/auth';

import {BaseButton, BaseInput, Gap} from '../components';

import {Colors, Fonts, showSuccess, showError} from '../utils';

const Register = ({navigation}) => {
  const [isLoading, setIsLoading] = useState(false);

  const registerSchema = object().shape({
    email: string().email('Invalid email').required('Please enter your email'),
    password: string().min(5).required('Please enter your password'),
    confirmPassword: string()
      .min(5)
      .required('Please enter your confirm password')
      .when('password', {
        is: val => (val && val.length > 0 ? true : false),
        then: string().oneOf(
          [ref('password')],
          ' Password must to be the match',
        ),
      }),
  });

  const handleRegister = async values => {
    setIsLoading(true);

    try {
      const response = await auth().createUserWithEmailAndPassword(
        values.email,
        values.password,
      );

      if (response.user) {
        setIsLoading(false);

        showSuccess({
          message: 'Register Success',
          description: 'Now you can login with your account',
        });

        navigation.navigate('Login');
      }
    } catch (error) {
      setIsLoading(false);
      showError({
        message: 'Register Failed',
        description: error?.code,
      });
    }
  };

  return (
    <SafeAreaView style={styles.screen}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <StatusBar backgroundColor={Colors.FILL} />
        <Gap height={80} />
        <Text style={styles.headingTitle}>Register</Text>
        <Gap height={48} />
        <Formik
          initialValues={{email: '', password: '', confirmPassword: ''}}
          validationSchema={registerSchema}
          onSubmit={handleRegister}>
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            values,
            errors,
            touched,
          }) => (
            <View>
              <BaseInput
                label="Email"
                placeholder="Enter Your Email"
                disable={isLoading}
                value={values.email}
                onBlur={handleBlur('email')}
                onChangeText={handleChange('email')}
                errors={errors.email}
                touched={touched.email}
              />
              <Gap height={24} />
              <BaseInput
                label="Password"
                type="password"
                placeholder="Enter Password here"
                onBlur={handleBlur('password')}
                value={values.password}
                onChangeText={handleChange('password')}
                errors={errors.password}
                touched={touched.password}
              />
              <Gap height={24} />
              <BaseInput
                label="Confirm Password"
                type="password"
                placeholder="Enter Confirmation Password here"
                onBlur={handleBlur('confirmPassword')}
                value={values.confirmPassword}
                onChangeText={handleChange('confirmPassword')}
                errors={errors.confirmPassword}
                touched={touched.confirmPassword}
              />
              <Gap height={48} />
              <BaseButton
                isLoading={isLoading}
                disable={isLoading}
                title="Register"
                style={styles.btnLogin}
                onPress={handleSubmit}
              />
            </View>
          )}
        </Formik>
        <Gap height={48} />
        <View style={styles.registerContainer}>
          <Text style={styles.registerText}>Already have an account? </Text>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
              navigation.replace('Login');
            }}>
            <Text style={styles.registerText}>Login</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Register;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
    padding: 24,
  },
  headingTitle: {
    fontSize: 32,
    color: Colors.TEXT,
    fontFamily: Fonts.PRIMARY.BOLD,
  },
  socialSub: {
    width: 150,
    fontSize: 12,
    textAlign: 'center',
    color: Colors.SUB_TEXT,
    fontFamily: Fonts.PRIMARY.MEDIUM,
  },
  dividerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  divider: {
    flex: 1,
    height: 1,
    backgroundColor: Colors.GREY_MEDIUM,
  },
  btnLogin: {
    borderRadius: 100,
    shadowColor: Colors.SUB_TEXT,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.22,
    shadowRadius: 2,
    elevation: 2,
  },
  socialContainer: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  socialBtn: {
    width: 64,
    height: 64,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 16,
    backgroundColor: Colors.BACKGROUND,
    shadowColor: Colors.SUB_TEXT,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.22,
    shadowRadius: 2,
    elevation: 2,
  },
  registerContainer: {
    width: '100%',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  registerText: {
    fontSize: 12,
    color: Colors.SUB_TEXT,
    fontFamily: Fonts.PRIMARY.MEDIUM,
  },
});
