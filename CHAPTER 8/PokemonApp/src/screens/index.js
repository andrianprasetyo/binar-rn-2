import Login from './Login';
import Register from './Register';
import Splash from './Splash';
import Dashboard from './Dashboard';
import PokeBag from './PokeBag';
import DetailPokemon from './DetailPokemon';

export {Login, Register, Splash, Dashboard, PokeBag, DetailPokemon};
