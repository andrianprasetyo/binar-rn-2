import React, {useState, useEffect, useCallback, useMemo} from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  FlatList,
  StatusBar,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';

import {Gap, PokemonItem, SearchInput} from '../components';

import {Colors, Fonts} from '../utils';

import {useSelector, useDispatch} from 'react-redux';
import {IconLogout} from '../assets';
import {clearAllUserData} from '../store/userSlice';

const PokeBag = ({navigation}) => {
  const dispatch = useDispatch();

  const [listPokebag, setListPokebag] = useState([]);

  const pokeBagState = useSelector(state => state.user.pokeBag);

  const [tempState, setTempState] = useState([]);

  const [keywordSearch, setKeywordSearch] = useState('');

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    setListPokebag(pokeBagState);
    setTempState(pokeBagState);
    setIsLoading(false);
  }, [pokeBagState?.length]);

  const handleChangeSearch = event => {
    setKeywordSearch(event);
  };

  const handleSearchSubmit = useMemo(() => {
    if (keywordSearch !== '') {
      const filteredData = listPokebag.filter(item => {
        return item.name.toLowerCase().match(keywordSearch);
      });

      setListPokebag(filteredData);
    }
  }, [keywordSearch]);

  const handleOnBlur = () => {
    if (keywordSearch === '') {
      setListPokebag(tempState);
    }
  };

  const handleClickDetail = url => {
    navigation.navigate('DetailPokemon', {url: url});
  };

  return (
    <SafeAreaView style={styles.screen}>
      <StatusBar backgroundColor={Colors.FILL} />
      <View style={styles.headerContainer}>
        <Text style={styles.headingTitle}>PokeBag</Text>

        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.logout}
          onPress={() => {
            dispatch(clearAllUserData());
            navigation.navigate('Login');
          }}>
          <IconLogout />
        </TouchableOpacity>
      </View>
      <Gap height={28} />
      <SearchInput
        placeholder="Search Pokemon"
        value={keywordSearch}
        onChangeText={handleChangeSearch}
        onSubmit={handleSearchSubmit}
        onBlur={handleOnBlur}
      />
      <Gap height={28} />
      {isLoading ? (
        <View style={styles.screen}>
          <ActivityIndicator
            style={styles.loading}
            size={'large'}
            color={Colors.FILL}
          />
        </View>
      ) : pokeBagState?.length > 0 ? (
        <>
          <FlatList
            style={styles.listPokemon}
            numColumns={2}
            data={listPokebag}
            keyExtractor={item => item.name}
            renderItem={({item}) => (
              <PokemonItem
                name={item.name}
                onPress={() => handleClickDetail(item.url)}
              />
            )}
          />
        </>
      ) : (
        <View style={styles.empty}>
          <Text style={styles.emptyText}>U didn't catch any pokemon yet</Text>
        </View>
      )}
    </SafeAreaView>
  );
};

export default React.memo(PokeBag);

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
    padding: 24,
  },
  headingTitle: {
    fontSize: 32,
    color: Colors.TEXT,
    fontFamily: Fonts.PRIMARY.BOLD,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  containerPagination: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  btnPagination: {
    width: '49%',
    borderRadius: 15,
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  empty: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  emptyText: {
    fontSize: 14,
    color: Colors.TEXT,
    fontFamily: Fonts.PRIMARY.REGULAR,
  },
  paginationTitle: {
    fontSize: 16,
    color: Colors.TEXT,
    fontFamily: Fonts.PRIMARY.BOLD,
  },
  dropDownContainer: {
    backgroundColor: Colors.FILL,
    borderRadius: 6,
  },
  textDropdown: {
    fontFamily: Fonts.PRIMARY.BOLD,
    fontSize: 16,
    paddingVertical: 8,
    paddingHorizontal: 8,
    color: Colors.WHITE,
  },
  dropdownStyle: {
    backgroundColor: Colors.FILL,
    borderRadius: 6,
  },
  dropdownTextStyle: {
    backgroundColor: Colors.FILL,
    fontFamily: Fonts.PRIMARY.SEMIBOLD,
    fontSize: 14,
    paddingVertical: 8,
    paddingHorizontal: 8,
    color: Colors.WHITE,
  },
  dropdownTextHighlightStyle: {
    fontFamily: Fonts.PRIMARY.BLACK,
    fontSize: 14,
    paddingVertical: 8,
    paddingHorizontal: 8,
    color: Colors.WHITE,
  },
  logout: {
    backgroundColor: Colors.FILL,
    padding: 12,
    borderRadius: 12,
  },
});
