import React, {useEffect} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {Colors, Fonts} from '../utils';

import {useSelector} from 'react-redux';

import {LogoPoke} from '../assets';

const Splash = ({navigation}) => {
  const userState = useSelector(state => state.user.user);

  useEffect(() => {
    setTimeout(() => {
      if (userState.hasOwnProperty('uid')) {
        navigation.reset({index: 0, routes: [{name: 'Main'}]});
      } else {
        navigation.reset({index: 0, routes: [{name: 'Login'}]});
      }
    }, 2000);
  }, []);

  return (
    <View style={styles.screen}>
      <View style={styles.container}>
        <Image source={LogoPoke} />
      </View>
      <Text style={styles.title}>Pokemon App</Text>
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    paddingVertical: 24,
    textAlign: 'center',
    fontSize: 16,
    color: Colors.TEXT,
    fontFamily: Fonts.PRIMARY.LIGHT,
  },
});
