import React, {useEffect, useState, useCallback} from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  ScrollView,
  Animated,
  Modal,
  TouchableWithoutFeedback,
  Easing,
} from 'react-native';
import {IconBack, IconPokemonBigPNG, IconPokePhone} from '../assets';
import {Gap} from '../components';
import apiClient from '../services/apiClient';
import {Colors, Fonts, showError, showSuccess} from '../utils';
import {useDispatch, useSelector} from 'react-redux';
import {removeItemPokeBag, addItemPokeBag} from '../store/userSlice';

const DetailPokemon = ({route, navigation}) => {
  const {url} = route.params;
  const dispatch = useDispatch();

  const [rotateValueHolder, setRotateValueHolder] = useState(
    new Animated.Value(0),
  );

  const [detailPokemon, setDetailPokemon] = useState({});

  const pokeBagState = useSelector(state => state.user.pokeBag);

  const [isLoading, setIsLoading] = useState(false);

  const [isLoadingCatch, setIsLoadingCatch] = useState(false);

  useEffect(() => {
    getDetailPokemon(url);
  }, [url]);

  const spinAnimate = () => {
    Animated.loop(
      Animated.timing(rotateValueHolder, {
        toValue: 3,
        duration: 2000,
        easing: Easing.linear,
        useNativeDriver: true,
      }),
    ).start();
  };

  const getDetailPokemon = async url => {
    const splitted = url.split('/');
    const selected = splitted[6];

    try {
      setIsLoading(true);
      const res = await apiClient.get(`pokemon/${selected}`);

      if (res.data) {
        setDetailPokemon(res.data);
        setIsLoading(false);
      }
    } catch (error) {
      console.error(error);
      setIsLoading(false);
      showError({message: 'Error', description: 'Something has wrong'});
    }
  };

  const handleCatch = () => {
    const randomNumb = Math.floor(Math.random() * 10) + 1;

    setIsLoadingCatch(true);
    spinAnimate();
    setTimeout(() => {
      if (randomNumb < 5) {
        setIsLoadingCatch(false);
        showError({
          message: 'Failed Catch',
          description: 'Try to catch pokemon again',
        });
        setRotateValueHolder(new Animated.Value(0));
      } else {
        setIsLoadingCatch(false);
        dispatch(
          addItemPokeBag({
            name: detailPokemon?.species?.name,
            url: detailPokemon?.species?.url,
          }),
        );
        showSuccess({
          message: 'Success Catch',
          description: 'Pokemon catched to your PokeBag',
        });
        setRotateValueHolder(new Animated.Value(0));
      }
    }, 2000);
  };

  return (
    <SafeAreaView style={[styles.screen, {position: 'relative'}]}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <StatusBar backgroundColor={Colors.FILL} />
        {isLoadingCatch && (
          <Modal
            animationType="fade"
            transparent={true}
            style={styles.modalContainer}
            visible={isLoadingCatch}
            onRequestClose={() => {
              // Do something for Background here
            }}>
            <TouchableWithoutFeedback style={{flex: 1}} onPress={() => {}}>
              <View style={styles.absoluteCard}>
                <Animated.Image
                  style={{
                    width: '100%',
                    minHeight: 300,
                    transform: [
                      {
                        scale: 0.75,
                        rotate: rotateValueHolder.interpolate({
                          inputRange: [0, 1],
                          outputRange: ['0deg', '360deg'],
                        }),
                      },
                    ],
                  }}
                  source={IconPokemonBigPNG}
                />
              </View>
            </TouchableWithoutFeedback>
          </Modal>
        )}
        {isLoading ? (
          <View style={styles.screen}>
            <ActivityIndicator
              style={styles.loading}
              size={'large'}
              color={Colors.FILL}
            />
          </View>
        ) : (
          <>
            <View style={styles.headerContainer}>
              <View style={styles.headerSection}>
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => navigation.goBack()}>
                  <IconBack />
                </TouchableOpacity>
                <Gap width={24} />
                <Text style={styles.headingTitle}>
                  {detailPokemon?.species?.name?.charAt(0).toUpperCase() +
                    detailPokemon?.species?.name?.slice(1)}
                </Text>
              </View>

              {pokeBagState?.find(
                item => item.name === detailPokemon?.species?.name,
              ) ? (
                <TouchableOpacity
                  style={styles.catchSectionActive}
                  activeOpacity={0.7}
                  onPress={() => {
                    dispatch(
                      removeItemPokeBag({
                        name: detailPokemon?.species?.name,
                        url: detailPokemon?.species?.url,
                      }),
                    );
                    showSuccess({
                      message: 'Pokemon Released',
                      description: 'Pokemon success released from your pokebag',
                    });
                  }}>
                  <IconPokePhone />
                  <Gap width={12} />
                  <Text style={styles.catchText}>Release</Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  style={styles.catchSection}
                  activeOpacity={0.7}
                  onPress={() => {
                    handleCatch();
                  }}>
                  <IconPokePhone />
                  <Gap width={12} />
                  <Text style={styles.catchText}>Catch</Text>
                </TouchableOpacity>
              )}
            </View>
            <View style={styles.headerCard}>
              <Image
                style={styles.image}
                source={{
                  uri: detailPokemon?.sprites?.other?.home?.front_default,
                }}
                resizeMode="center"
              />
            </View>
            <View style={styles.contentCard}>
              <Text style={styles.headingTitle}>Description :</Text>
              <Gap height={24} />
              <View style={styles.dividerContainer}>
                <View style={styles.divider} />
              </View>
              <Gap height={24} />

              <View style={styles.rowContainer}>
                <Text style={styles.textHeader}>Height :</Text>
                <Gap width={24} />
                <Text style={styles.textSub}>{detailPokemon?.height}</Text>
              </View>
              <Gap height={24} />
              <View style={styles.rowContainer}>
                <Text style={styles.textHeader}>Weight :</Text>
                <Gap width={24} />
                <Text style={styles.textSub}>{detailPokemon?.weight}</Text>
              </View>
              <Gap height={24} />
              <View style={styles.rowContainer}>
                <Text style={styles.textHeader}>Base Experience :</Text>
                <Gap width={24} />
                <Text style={styles.textSub}>
                  {detailPokemon?.base_experience}
                </Text>
              </View>

              <Gap height={24} />
              <View style={styles.dividerContainer}>
                <View style={styles.divider} />
              </View>
              <Gap height={24} />

              <Text style={styles.textHeader}>Types</Text>
              <Gap height={16} />
              <View style={{flexDirection: 'row'}}>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                  {detailPokemon?.types?.map(item => (
                    <View style={styles.cardBadge} key={item?.type?.name}>
                      <Text style={styles.textName}>{item?.type?.name}</Text>
                    </View>
                  ))}
                </ScrollView>
              </View>

              <Gap height={24} />
              <Text style={styles.textHeader}>Abilites</Text>
              <Gap height={16} />
              <View style={{flexDirection: 'row'}}>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                  {detailPokemon?.abilities?.map(item => (
                    <View style={styles.cardBadge} key={item?.ability?.name}>
                      <Text style={styles.textName}>{item?.ability?.name}</Text>
                    </View>
                  ))}
                </ScrollView>
              </View>

              <Gap height={24} />
              <Text style={styles.textHeader}>Moves</Text>
              <Gap height={16} />
              <View style={{flexDirection: 'row'}}>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                  {detailPokemon?.moves?.map(item => (
                    <View style={styles.cardBadge} key={item?.move?.name}>
                      <Text style={styles.textName}>{item?.move?.name}</Text>
                    </View>
                  ))}
                </ScrollView>
              </View>
            </View>
          </>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default DetailPokemon;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
  },
  headingTitle: {
    fontSize: 32,
    color: Colors.TEXT,
    fontFamily: Fonts.PRIMARY.BOLD,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 20,
    backgroundColor: Colors.GREY_LIGHT,
  },
  headerSection: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  catchSection: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.FILL,
    paddingVertical: 12,
    paddingHorizontal: 20,
    borderRadius: 25,
  },
  catchSectionActive: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.TEXT,
    paddingVertical: 12,
    paddingHorizontal: 20,
    borderRadius: 25,
  },
  catchText: {
    fontSize: 16,
    color: Colors.WHITE,
    fontFamily: Fonts.PRIMARY.BOLD,
  },
  dividerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  divider: {
    flex: 1,
    height: 1,
    backgroundColor: Colors.GREY_MEDIUM,
  },
  loading: {
    flex: 1,
    marginTop: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    minWidth: '100%',
    height: 300,
  },
  headerCard: {
    height: 500,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.GREY_LIGHT,
  },
  contentCard: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
    marginTop: -25,
    borderTopLeftRadius: 32,
    borderTopRightRadius: 32,
    padding: 24,
  },
  contentContainer: {
    padding: 20,
  },
  rowContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textHeader: {
    fontSize: 16,
    color: Colors.TEXT,
    fontFamily: Fonts.PRIMARY.BOLD,
  },
  textSub: {
    fontSize: 14,
    color: Colors.TEXT,
    fontFamily: Fonts.PRIMARY.REGULAR,
  },
  textName: {
    backgroundColor: Colors.FILL,
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 14,
    paddingVertical: 12,
    paddingHorizontal: 16,
    color: Colors.WHITE,
    borderRadius: 100,
  },
  cardBadge: {
    marginRight: 8,
  },
  modalContainer: {
    width: '100%',
    alignSelf: 'center',
    height: '100%',
    justifyContent: 'flex-start',
  },
  absoluteCard: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
});
