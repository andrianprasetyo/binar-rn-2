import React, {useState, useEffect, useCallback} from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  FlatList,
  StatusBar,
  ActivityIndicator,
} from 'react-native';
import {BaseButton, Gap, PokemonItem} from '../components';
import apiClient from '../services/apiClient';
import {Colors, Fonts, showError} from '../utils';
import ModalDropdown from 'react-native-modal-dropdown';

const Dashboard = ({navigation}) => {
  const [listPokemon, setListPokemon] = useState({});

  const [offset, setOffset] = useState(0);

  const [limit, setLimit] = useState(0);

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    getListPokemon(offset, limit);
  }, [offset, limit]);

  const handlePrev = () => {
    if (offset > 0) {
      setOffset(prevState => prevState - 20);
    }
  };

  const handleNext = () => {
    setOffset(prevState => prevState + 20);
  };

  const getListPokemon = useCallback(
    async (offset, limit) => {
      try {
        setIsLoading(true);
        const res = await apiClient.get(
          `pokemon?offset=${offset}&limit=${limit}`,
        );

        if (res.data) {
          setListPokemon(res.data);
          setIsLoading(false);
        }
      } catch (error) {
        console.error(error);
        setIsLoading(false);
        showError({message: 'Error', description: 'Something has wrong'});
      }
    },
    [offset, limit],
  );

  const handleClickDetail = url => {
    navigation.navigate('DetailPokemon', {url: url});
  };

  return (
    <SafeAreaView style={styles.screen}>
      <StatusBar backgroundColor={Colors.FILL} />
      <View style={styles.headerContainer}>
        <Text style={styles.headingTitle}>Pokedex</Text>
        <View style={styles.containerPagination}>
          <Text style={styles.paginationTitle}>View :</Text>
          <Gap width={10} />
          <ModalDropdown
            textStyle={styles.textDropdown}
            style={styles.dropDownContainer}
            defaultValue="10"
            dropdownStyle={styles.dropdownStyle}
            dropdownTextStyle={styles.dropdownTextStyle}
            dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}
            onSelect={(_, option) => {
              setLimit(parseInt(option));
            }}
            options={['10', '20', '30', '40']}
          />
        </View>
      </View>
      <Gap height={28} />
      {isLoading ? (
        <View style={styles.screen}>
          <ActivityIndicator
            style={styles.loading}
            size={'large'}
            color={Colors.FILL}
          />
        </View>
      ) : (
        <FlatList
          style={styles.listPokemon}
          numColumns={2}
          data={listPokemon?.results}
          keyExtractor={item => item.name}
          renderItem={({item}) => (
            <PokemonItem
              name={item.name}
              onPress={() => handleClickDetail(item.url)}
            />
          )}
        />
      )}

      <Gap height={28} />
      <View style={styles.containerPagination}>
        <BaseButton
          disable={listPokemon?.previous == null}
          title="Previous"
          style={styles.btnPagination}
          onPress={handlePrev}
        />
        <BaseButton
          disable={listPokemon?.next == null}
          title="Next"
          style={styles.btnPagination}
          onPress={handleNext}
        />
      </View>
    </SafeAreaView>
  );
};

export default Dashboard;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
    padding: 24,
  },
  headingTitle: {
    fontSize: 32,
    color: Colors.TEXT,
    fontFamily: Fonts.PRIMARY.BOLD,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  containerPagination: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  btnPagination: {
    width: '49%',
    borderRadius: 15,
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  paginationTitle: {
    fontSize: 16,
    color: Colors.TEXT,
    fontFamily: Fonts.PRIMARY.BOLD,
  },
  dropDownContainer: {
    backgroundColor: Colors.FILL,
    borderRadius: 6,
  },
  textDropdown: {
    fontFamily: Fonts.PRIMARY.BOLD,
    fontSize: 16,
    paddingVertical: 8,
    paddingHorizontal: 8,
    color: Colors.WHITE,
  },
  dropdownStyle: {
    backgroundColor: Colors.FILL,
    borderRadius: 6,
  },
  dropdownTextStyle: {
    backgroundColor: Colors.FILL,
    fontFamily: Fonts.PRIMARY.SEMIBOLD,
    fontSize: 14,
    paddingVertical: 8,
    paddingHorizontal: 8,
    color: Colors.WHITE,
  },
  dropdownTextHighlightStyle: {
    fontFamily: Fonts.PRIMARY.BLACK,
    fontSize: 14,
    paddingVertical: 8,
    paddingHorizontal: 8,
    color: Colors.WHITE,
  },
});
