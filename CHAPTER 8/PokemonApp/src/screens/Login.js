import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  SafeAreaView,
  StatusBar,
  View,
  TouchableOpacity,
} from 'react-native';

import {Formik} from 'formik';

import {object, string} from 'yup';

import {useDispatch} from 'react-redux';

import auth from '@react-native-firebase/auth';

import {BaseButton, BaseInput, Gap} from '../components';

import {Colors, Fonts, showError, showSuccess} from '../utils';
import {setUser} from '../store/userSlice';

const Login = ({navigation}) => {
  const dispatch = useDispatch();

  const [isLoading, setIsLoading] = useState(false);

  const loginSchema = object().shape({
    email: string().email('Invalid email').required('Please enter your email'),
    password: string().min(5).required('Please enter your password'),
  });

  const handleLogin = async values => {
    setIsLoading(true);

    try {
      const res = await auth().signInWithEmailAndPassword(
        values.email,
        values.password,
      );

      if (res.user) {
        setIsLoading(false);
        const userData = res.user;

        showSuccess({
          message: 'Login Success',
          description: 'Time for looking your lovely pokemon',
        });

        dispatch(setUser(userData));

        navigation.navigate('Main', {
          screen: 'Dashboard',
        });
      }
    } catch (error) {
      setIsLoading(false);
      showError({message: 'Login Failed', description: error?.code});
    }
  };

  return (
    <SafeAreaView style={styles.screen}>
      <StatusBar backgroundColor={Colors.FILL} />
      <Gap height={80} />
      <Text style={styles.headingTitle}>Log in</Text>
      <Gap height={48} />
      <Formik
        initialValues={{email: '', password: ''}}
        validationSchema={loginSchema}
        onSubmit={handleLogin}>
        {({
          handleChange,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <View>
            <BaseInput
              label="Email"
              disable={isLoading}
              value={values.email}
              placeholder="Enter email here"
              onChangeText={handleChange('email')}
              onBlur={handleBlur('email')}
              errors={errors.email}
              touched={touched.email}
            />
            <Gap height={24} />
            <BaseInput
              label="Password"
              type="password"
              disable={isLoading}
              value={values.password}
              placeholder="Enter password here"
              onChangeText={handleChange('password')}
              onBlur={handleBlur('password')}
              errors={errors.password}
              touched={touched.password}
            />
            <Gap height={48} />
            <BaseButton
              onPress={handleSubmit}
              title="Login"
              disable={isLoading}
              isLoading={isLoading}
            />
          </View>
        )}
      </Formik>
      <Gap height={48} />
      <View style={styles.registerContainer}>
        <Text style={styles.registerText}>Don't have an account? </Text>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => {
            navigation.navigate('Register');
          }}>
          <Text style={styles.registerText}>Register</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default Login;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
    padding: 24,
  },
  headingTitle: {
    fontSize: 32,
    color: Colors.TEXT,
    fontFamily: Fonts.PRIMARY.BOLD,
  },
  dividerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  divider: {
    flex: 1,
    height: 1,
    backgroundColor: Colors.GREY_MEDIUM,
  },
  btnLogin: {
    borderRadius: 100,
    shadowColor: Colors.SUB_TEXT,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.22,
    shadowRadius: 2,
    elevation: 2,
  },
  registerContainer: {
    width: '100%',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  registerText: {
    fontSize: 12,
    color: Colors.SUB_TEXT,
    fontFamily: Fonts.PRIMARY.MEDIUM,
  },
});
