import React from 'react';

import {Text, TouchableOpacity, StyleSheet} from 'react-native';

import {Colors, Fonts} from '../utils';

import {
  IconPokeBag,
  IconPokeBagActive,
  IconPokeball,
  IconPokeballActive,
} from '../assets';

import Gap from './Gap';

const TabItem = props => {
  const {title, isActive, onPress, onLongPress} = props;

  const Icon = () => {
    switch (title) {
      case 'Dashboard':
        return isActive ? <IconPokeballActive /> : <IconPokeball />;
      case 'PokeBag':
        return isActive ? <IconPokeBagActive /> : <IconPokeBag />;
      default:
        return <IconPokeball />;
    }
  };

  const titleShow = () => {
    let displayTitle;

    switch (title) {
      case 'Dashboard':
        displayTitle = 'Pokemon';
        break;
      case 'PokeBag':
        displayTitle = 'PokeBag';
        break;
      default:
        displayTitle = 'Other';
        break;
    }

    return displayTitle;
  };

  return (
    <TouchableOpacity
      activeOpacity={0.7}
      style={styles.container}
      onPress={onPress}
      onLongPress={onLongPress}>
      <Icon />
      <Gap height={8} />
      <Text style={styles.title(isActive)}>{titleShow()}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    paddingVertical: 15,
  },
  title: isActive => ({
    fontFamily: Fonts.PRIMARY.SEMIBOLD,
    fontSize: 12,
    color: isActive ? Colors.FILL : Colors.SUB_TEXT,
  }),
});

export default TabItem;
