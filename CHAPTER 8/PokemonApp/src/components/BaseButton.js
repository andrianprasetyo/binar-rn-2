import React from 'react';

import {
  Text,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import {Colors, Fonts} from '../utils';

const BaseButton = props => {
  const {title, onPress, style, isLoading, disable} = props;
  return (
    <TouchableOpacity
      accessibilityLabel={`button-${title}`}
      activeOpacity={0.7}
      style={[styles.container(style), style, disable && styles.disable]}
      onPress={onPress}
      disabled={disable}>
      {isLoading && (
        <ActivityIndicator
          size="small"
          accessibilityLabel="loading"
          color={Colors.WHITE}
        />
      )}
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
};

export default BaseButton;

const styles = StyleSheet.create({
  container: style => ({
    width: style ? style.width : '100%',
    flexDirection: 'row',
    backgroundColor: Colors.FILL,
    borderRadius: style ? style.borderRadius : 100,
    justifyContent: 'center',
    alignItems: 'center',
    ...style,
  }),
  title: {
    fontFamily: Fonts.PRIMARY.BOLD,
    fontSize: 16,
    paddingVertical: 20,
    paddingHorizontal: 16,
    color: Colors.WHITE,
  },
  disable: {
    backgroundColor: Colors.DISABLE,
  },
});
