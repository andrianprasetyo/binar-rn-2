import React from 'react';
import {StyleSheet, TextInput, View} from 'react-native';
import {IconSearch} from '../assets';
import {Colors, Fonts} from '../utils';
import Gap from './Gap';

const SearchInput = props => {
  const {value, placeholder, onSubmit, onChangeText, onBlur} = props;
  return (
    <View style={styles.searchContainer}>
      <IconSearch />
      <Gap width={5} />
      <TextInput
        accessibilityLabel="searchInput"
        value={value}
        onChangeText={onChangeText}
        style={styles.searchInput}
        placeholder={placeholder}
        onSubmitEditing={onSubmit}
        onBlur={onBlur}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  searchContainer: {
    width: '100%',
    backgroundColor: Colors.PLACEHOLDER,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    borderRadius: 10,
  },
  searchInput: {
    width: '90%',
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 16,
    color: Colors.TEXT,
    lineHeight: 20,
  },
});

export default SearchInput;
