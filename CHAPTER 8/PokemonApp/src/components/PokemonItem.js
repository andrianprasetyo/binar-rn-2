import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';
import {IconPokemon} from '../assets';

import {Fonts, Colors} from '../utils';
import Gap from './Gap';

const {width} = Dimensions.get('window');

const PokemonItem = ({name, onPress}) => {
  return (
    <TouchableOpacity
      accessibilityLabel={`pokeItem-${name}`}
      style={styles.container}
      activeOpacity={0.7}
      onPress={onPress}>
      <IconPokemon />
      <Gap height={8} />
      <Text style={styles.name}>
        {name?.charAt(0).toUpperCase() + name.slice(1)}
      </Text>
      <Gap height={8} />
    </TouchableOpacity>
  );
};

export default PokemonItem;

const styles = StyleSheet.create({
  containerShadow: {
    borderRadius: 5,
    backgroundColor: 'transparent',
  },
  container: {
    flex: 1,
    margin: 12,
    backgroundColor: Colors.WHITE,
    padding: 12,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    shadowColor: Colors.SUB_TEXT,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.22,
    shadowRadius: 2,
    elevation: 2,
  },
  name: {
    fontFamily: Fonts.PRIMARY.BOLD,
    fontSize: 14,
    lineHeight: 17,
    color: Colors.TEXT,
  },
});
