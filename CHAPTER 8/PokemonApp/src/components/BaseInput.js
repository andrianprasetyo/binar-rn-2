import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {IconEye, IconEyeActive} from '../assets';
import {Colors, Fonts} from '../utils';
import Gap from './Gap';

const BaseInput = props => {
  const {
    label,
    value,
    placeholder,
    type,
    disable,
    onChangeText,
    onBlur,
    errors,
    touched,
  } = props;

  const [showPassword, setShowPassword] = useState(true);

  const handleShowPassword = () => {
    setShowPassword(!showPassword);
  };

  return (
    <View>
      {label && <Text style={styles.formLabel}>{label}</Text>}
      <Gap height={12} />
      <View style={styles.inputContainer}>
        <TextInput
          accessibilityLabel={label}
          onChangeText={onChangeText}
          style={styles.formInput(type)}
          value={value}
          onBlur={onBlur}
          placeholder={placeholder}
          secureTextEntry={
            type === 'password' ? (showPassword ? true : false) : false
          }
          editable={!disable}
          selectTextOnFocus={!disable}
        />
        {type === 'password' && (
          <TouchableOpacity
            style={styles.eyeIcon}
            accessibilityLabel="icon-toggle-password"
            onPress={handleShowPassword}
            activeOpacity={0.7}>
            {showPassword ? <IconEye /> : <IconEyeActive />}
          </TouchableOpacity>
        )}
      </View>
      <Gap height={12} />
      {errors && touched ? <Text style={styles.error}>{errors}</Text> : null}
    </View>
  );
};

export default BaseInput;

const styles = StyleSheet.create({
  formLabel: {
    fontFamily: Fonts.PRIMARY.BOLD,
    fontSize: 14,
    color: Colors.TEXT,
  },
  inputContainer: {
    width: '100%',
    position: 'relative',
    backgroundColor: Colors.GREY_LIGHT,
    borderRadius: 12,
  },
  formInput: type => ({
    fontFamily: Fonts.PRIMARY.SEMIBOLD,
    fontSize: 16,
    color: Colors.GREY_DARK,
    paddingLeft: 18,
    paddingRight: type === 'password' ? 56 : 18,
    paddingVertical: 22,
  }),
  eyeIcon: {
    position: 'absolute',
    top: 0,
    right: 24,
    bottom: 0,
    justifyContent: 'center',
    color: Colors.TEXT,
  },
  error: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 14,
    color: Colors.DANGER,
  },
});
