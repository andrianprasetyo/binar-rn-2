// Soal Ke 1
function changeWord(selectedText, changeText, text) {
  return text.replace(selectedText, changeText);
}

const kalimat1 = 'Andini sangat mencintai kamu selamanya';
const kalimat2 = 'Gunung bromo tak akan mampu menggambarkan besarnya cintaku';

// EXPECTED RESULT
// 'Andini sangat membenci kamu selamanya'
console.log(changeWord('mencintai', 'membenci', kalimat1));
// 'Gunung semeru tak akan mampu menggambarkan besarnya cintaku'
console.log(changeWord('bromo', 'semeru', kalimat2));

// Soal Ke 2
function checkTypeNumber(givenNumber) {
  if (!givenNumber) {
    return 'ERROR: Bro where is the parameter?';
  }

  if (typeof givenNumber === 'number') {
    if (givenNumber % 2 === 0) {
      return 'GENAP';
    } else if (givenNumber % 2 !== 0) {
      return 'GANJIL';
    }
  } else {
    return 'Error: Invalid data type';
  }
}

// EXPECTED RESULT
console.log(checkTypeNumber(10)); // OUTPUT => "GENAP"
console.log(checkTypeNumber(3)); // OUTPUT => "GANJIL"
console.log(checkTypeNumber('3')); // OUTPUT => "Error: Invalid data type"
console.log(checkTypeNumber({})); // OUTPUT => "Error: Invalid data type"
console.log(checkTypeNumber([])); // OUTPUT => "Error: Invalid data type"
console.log(checkTypeNumber()); // OUTPUT => "Error: Bro where is the parameter?"

// Soal ke 3
function checkEmail(email) {
  if (!email) {
    return 'ERROR: Bro where is the parameter?';
  }

  if (typeof email === 'string') {
    if (!email.includes('@')) {
      return 'ERROR: Bro where is the @ ?';
    }

    const emailRegExp =
      /^[a-zA-Z0-9._]+@[a-zA-Z0-9-]+\.?[a-zA-Z]+\.[a-zA-Z]{2,4}$/i;

    if (emailRegExp.test(email)) {
      return 'VALID';
    } else {
      return 'INVALID';
    }
  } else {
    return 'ERROR: Invalid Data Type';
  }
}

// EXPECTED RESULT
console.log(checkEmail('apranata@binar.co.id')); // OUTPUT => "VALID"
console.log(checkEmail('apranata@binar.com')); // OUTPUT => "VALID"
console.log(checkEmail('apranata@binar')); // OUTPUT => "INVALID"
console.log(checkEmail('apranata')); // OUTPUT => "ERROR : Bro where is the @ ?"

console.log(checkTypeNumber(checkEmail(3322))); // OUTPUT => "ERROR : "
console.log(checkEmail()); // OUTPUT => "ERROR : Bro where is the parameter?"

console.log(false || true);

console.log(true && false);

// Soal Ke 4
function isValidPassword(password) {
  const passwordRegExp = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;

  if (typeof password === 'undefined') {
    return 'ERROR: Bro where is the parameter?';
  }

  if (typeof password === 'string') {
    if (passwordRegExp.test(password)) {
      return true;
    } else {
      return false;
    }
  } else {
    return 'ERROR: Invalid Data Type';
  }
}

// EXPECTED RESULT
// Password harus memiliki panjang minimal 8 huruf
// • Password harus memiliki minimal 1 angka
// • Password harus memiliki minimal 1 huruf kecil
// • Password harus memiliki minimal 1 huruf besar
console.log(isValidPassword('Meong2021')); // OUTPUT => true
console.log(isValidPassword('meong2021')); // OUPUT => false
console.log(isValidPassword('@eong')); // OUTPUT => false
console.log(isValidPassword('Meong2')); // OUTPUT => false
console.log(isValidPassword(0)); // OUTPUT => ERROR: Invalid Data Type
console.log(isValidPassword()); // OUTPUT => ERROR: Bro where is the parameter?

// Soal Ke 5
function getSplitName(person) {
  if (typeof person === 'undefined') {
    return 'ERROR: Bro where is the parameter?';
  }

  if (typeof person === 'string') {
    const fullName = {
      firstName: null,
      middleName: null,
      lastName: null,
    };

    const split = person.split(' ');

    if (split.length > 3) {
      return 'ERROR: This function is only for 3 characters name';
    }

    if (split.length < 2) {
      fullName.firstName = split[0];
    } else if (split.length > 2) {
      fullName.firstName = split[0];
      fullName.middleName = split[1];
      fullName.lastName = split[2];
    } else {
      fullName.firstName = split[0];
      fullName.lastName = split[1];
    }

    return fullName;
  } else {
    return 'ERROR: Invalid Data Type';
  }
}

// EXPECTED RESULT
console.log(getSplitName('Aldi Daniela Pranata'));
// OUTPUT => {firstName: 'Aldi' ,middleName: 'Daniela', lastName: 'Pranata'}
console.log(getSplitName('Dwi Kuncoro'));
// OUTPUT => {firstName: 'Dwi' ,middleName: 'null', lastName: 'Kuncoro'}
console.log(getSplitName('Aurora'));
// OUTPUT => {firstName: 'Aurora' ,middleName: 'null', lastName: 'null'}
console.log(getSplitName('Aurora Aureliya Sukma Darma'));
// OUTPUT => ERROR: This function is only for 3 characters name
console.log(getSplitName(0));
// OUTPUT => ERROR: "ERROR: Invalid Data Type"

// Soal Ke 6
function getAngkaTerbesarKedua(dataAngka) {
  if (typeof dataAngka === 'undefined') {
    return 'ERROR: Bro where is the parameter?';
  }

  if (Array.isArray(dataAngka)) {
    // use sort
    // return dataAngka.sort(function(a,b){return b-a})[1]

    // Math.max
    // Get the max number
    const max = Math.max.apply(null, dataAngka); // 9
    // Splice the max number from array
    dataAngka.splice(dataAngka.indexOf(max), 1);
    // Get the current max number without real max number included
    return Math.max.apply(null, dataAngka);
  } else {
    return 'ERROR: Invalid Data Type';
  }
}

const dataAngka = [9, 4, 7, 7, 4, 3, 2, 2, 8];

console.log(getAngkaTerbesarKedua(dataAngka));
console.log(getAngkaTerbesarKedua(0));
console.log(getAngkaTerbesarKedua());

// Soal Ke 7

const dataPenjualanPakAldi = [
  {
    namaProduct: 'Sepatu Futsal Nike Vapor Academy 8',
    hargaSatuan: 760000,
    kategori: 'Sepatu Sport',
    totalTerjual: 90,
  },
  {
    namaProduct: 'Sepatu Warrior Tristan Black Brown High',
    hargaSatuan: 960000,
    kategori: 'Sepatu Sneaker',
    totalTerjual: 37,
  },
  {
    namaProduct: 'Sepatu Warrior Tristan Maroon High ',
    kategori: 'Sepatu Sneaker',
    hargaSatuan: 360000,
    totalTerjual: 90,
  },
  {
    namaProduct: 'Sepatu Warrior Rainbow Tosca Corduroy',
    hargaSatuan: 120000,
    kategori: 'Sepatu Sneaker',
    totalTerjual: 90,
  },
];

function hitungTotalPenjualan(dataPenjualan) {
  if (typeof dataPenjualan === 'undefined') {
    return 'ERROR: Bro where is the parameter?';
  }

  if (Array.isArray(dataPenjualan)) {
    return dataPenjualan.reduce((accumulator, data) => {
      return accumulator + data.totalTerjual;
    }, 0);
  } else {
    return 'ERROR: Invalid Data Type';
  }
}

// EXPECTED RESULT
console.log(hitungTotalPenjualan(dataPenjualanPakAldi));
// OUTPUT => 307

// Soal Ke 8

const dataPenjualanNovel = [
  {
    idProduct: 'BOOK002421',
    namaProduk: 'Pulang - Pergi',
    penulis: 'Tere Liye',
    hargaBeli: 60000,
    hargaJual: 86000,
    totalTerjual: 150,
    sisaStok: 17,
  },
  {
    idProduct: 'BOOK002351',
    namaProduk: 'Selamat Tinggal',
    penulis: 'Tere Liye',
    hargaBeli: 75000,
    hargaJual: 103000,
    totalTerjual: 171,
    sisaStok: 20,
  },
  {
    idProduct: 'BOOK002941',
    namaProduk: 'Garis Waktu',
    penulis: 'Fiersa Besari',
    hargaBeli: 67000,
    hargaJual: 99000,
    totalTerjual: 213,
    sisaStok: 5,
  },
  {
    idProduct: 'BOOK002941',
    namaProduk: 'Laskar Pelangi',
    penulis: 'Andrea Hirata',
    hargaBeli: 55000,
    hargaJual: 68000,
    totalTerjual: 20,
    sisaStok: 56,
  },
];

function getInfoPenjualan(dataPenjualan) {
  if (typeof dataPenjualan === 'undefined') {
    return 'ERROR: Bro where is the parameter?';
  }

  if (Array.isArray(dataPenjualan)) {
    const RupiahFormat = new Intl.NumberFormat('id', {
      style: 'currency',
      currency: 'IDR',
    });

    const result = dataPenjualan.reduce((accumulator, data, index, arr) => {
      accumulator['totalModal'] =
        data.hargaBeli * (data.sisaStok + data.totalTerjual);

      accumulator['totalKeuntungan'] = data.hargaJual * data.totalTerjual;

      accumulator['persentaseKeuntungan'] = (
        (accumulator['totalKeuntungan'] / accumulator['totalModal']) *
        100
      ).toFixed(0);

      const sortedData = arr.sort((a, b) => b.totalTerjual - a.totalTerjual)[0];

      accumulator['produkBukuTerlaris'] = sortedData.namaProduk;
      accumulator['penulisTerlaris'] = sortedData.penulis;

      return {
        totalModal: RupiahFormat.format(accumulator.totalModal),
        totalKeuntungan: RupiahFormat.format(accumulator.totalKeuntungan),
        persentaseKeuntungan: `${accumulator.persentaseKeuntungan}%`,
        produkBukuTerlaris: accumulator.produkBukuTerlaris,
        penulisTerlaris: accumulator.penulisTerlaris,
      };
    }, {});

    return result;
  } else {
    return 'ERROR: Invalid Data Type';
  }
}

// EXPECTED RESULT
/* Object with
	{
  	totalKeuntungan : "Rp. 10.000.000",
    totalModal: "Rp. 6.000.000",
    persentaseKeuntungan: "60%",
    produkBukuTerlaris: "BUKU TERLARIS BERDASARKAN DATA DIATAS",
    penulisTerlaris: "PENULIS TERLARIS BERDASARKAN DATA DIATAS"
  }
*/
console.log(getInfoPenjualan(dataPenjualanNovel));