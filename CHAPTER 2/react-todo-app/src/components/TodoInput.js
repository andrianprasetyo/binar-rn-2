import React, { useEffect, useRef } from "react";

function TodoInput({ onSubmit }) {
  // Initial or Ref for input
  const inputRef = useRef("");

  // Set the cursor focus when first time access
  useEffect(() => {
    inputRef.current.focus();
  }, []);

  const handleChange = (event) => {
    inputRef.current.value = event.target.value;
    console.log(inputRef.current.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    if (inputRef.current.value !== "") {
      // Pass State to Parent
      onSubmit({
        id: Math.floor(Math.random() * 10000),
        value: inputRef.current.value,
      });

      // Clearing
      inputRef.current.value = "";
    }
  };

  return (
    <div className="todo-input-base">
      <input ref={inputRef} className="todo-input" type="text" name="todo" placeholder="Add To Do ..." onChange={handleChange} />
      <button type="button" className="btn" onClick={handleSubmit}>
        Add Todo
      </button>
    </div>
  );
}

export default TodoInput;
