import React, { useEffect, useState } from "react";

function Todo({ data, onDelete, onEdit }) {
  const [input, setInput] = useState("");
  const [isEdit, setIsEdit] = useState(false);

  useEffect(() => {
    setInput(data.value);
  }, [data.value]);

  const handleDelete = () => {
    onDelete(data.id);
  };

  const handleChangeEdit = (event) => {
    setIsEdit(true);
    setInput(event.target.value);
  };

  const handleEdit = () => {
    if (input.length < 1) {
      alert("Input Tidak Boleh Kosong");
      return;
    }

    onEdit(data.id, { id: data.id, value: input });
    setIsEdit(false);
  };

  return (
    <div className="todo-item">
      <input onClick={handleChangeEdit} onChange={handleChangeEdit} className="todo-item-text" value={input} />
      {isEdit ? <button onClick={handleEdit}>Update</button> : <button onClick={handleDelete}>Delete</button>}
    </div>
  );
}

export default Todo;
