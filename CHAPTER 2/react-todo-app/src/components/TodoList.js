import React, { useState } from "react";
import Todo from "./Todo";
import TodoInput from "./TodoInput";

function TodoList() {
  const todosString = localStorage.getItem("todoList");
  const todos = JSON.parse(todosString);

  const [todoList, setTodoList] = useState([...todos]);

  const handleSubmit = (value) => {
    setTodoList([...todoList, value]);
    localStorage.setItem("todoList", JSON.stringify([...todoList, value]));
  };

  const handleDelete = (id) => {
    const filteredTodo = [...todoList].filter((todo) => todo.id !== id);
    setTodoList(filteredTodo);
    localStorage.setItem("todoList", JSON.stringify(filteredTodo));
  };

  const handleEdit = (id, value) => {
    /* 
      Map method return new array
      if id match then replace it
    */
    const updatedTodo = todoList.map((todo) => {
      return todo.id === id ? value : todo;
    });
    setTodoList(updatedTodo);
    localStorage.setItem("todoList", JSON.stringify(updatedTodo));
  };

  return (
    <div className="card">
      <h1 className="heading">React To Do App</h1>

      <TodoInput onSubmit={handleSubmit} />

      <hr className="divider" />

      {todoList.length < 1
        ? "Oops... To do List Empty. Try to add some To do"
        : todoList.map((todo) => {
            return <Todo key={todo.id} data={todo} onDelete={handleDelete} onEdit={handleEdit} />;
          })}
    </div>
  );
}

export default TodoList;
