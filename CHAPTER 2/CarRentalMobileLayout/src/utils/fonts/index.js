export const Fonts = {
  PRIMARY: {
    LIGHT: 'Helvetica-Light',
    REGULAR: 'Helvetica-Regular',
    BOLD: 'Helvetica-Bold',
  },
};
