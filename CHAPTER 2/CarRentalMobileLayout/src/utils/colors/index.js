const mainColors = {
  darkBlue: {
    0: '#D3D9FD',
    1: '#CFD4ED',
    2: '#AEB7E1',
    3: '#5D6FC3',
    4: '#0D28A6',
    5: '#091B6F',
  },
  limeGreen: {
    1: '#DEF1DF',
    2: '#C9E7CA',
    3: '#92D094',
    4: '#5CB85F',
    5: '#3D7B3F',
  },
  alert: {
    danger: '#FA2C5A',
    success: '#73CA5C',
    warning: '#F9CC00',
  },
  neutral: {
    white: '#FFFFFF',
    black: '#000000',
    grey: '#8A8A8A',
  },
};

export const Colors = {
  PRIMARY: mainColors.darkBlue[5],
  SECONDARY: mainColors.limeGreen[4],
  BACKGROUND_CARD: mainColors.darkBlue[0],
  BACKGROUND_ICON: mainColors.limeGreen[1],
  WHITE: mainColors.neutral.white,
  BLACK: mainColors.neutral.black,
  BLUE: mainColors.darkBlue[4],
  GREY: mainColors.neutral.grey,
};
