import IconBox from './ic-box.svg';
import IconBriefCase from './ic-briefcase.svg';
import IconCamera from './ic-camera.svg';
import IconKey from './ic-key.svg';
import IconTruck from './ic-truck.svg';
import IconUsers from './ic-users.svg';
import IconHome from './ic-home.svg';
import IconHomeActive from './ic-home-active.svg';
import IconList from './ic-list.svg';
import IconListActive from './ic-list-active.svg';
import IconUserAccount from './ic-user-account.svg';
import IconUserAccountActive from './ic-user-account-active.svg';

export {
  IconBox,
  IconBriefCase,
  IconCamera,
  IconKey,
  IconTruck,
  IconUsers,
  IconHome,
  IconHomeActive,
  IconList,
  IconListActive,
  IconUserAccount,
  IconUserAccountActive,
};
