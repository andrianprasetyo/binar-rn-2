import ImageCarDummy from './img-car-dummy.png';
import ImageCarSplash from './img-car-splash.png';
import ImgCarBannerDummy from './img-car-banner-dummy.png';
import ImageProfileDummy from './img-profile-dummy.png';

export {ImageCarDummy, ImageCarSplash, ImgCarBannerDummy, ImageProfileDummy};
