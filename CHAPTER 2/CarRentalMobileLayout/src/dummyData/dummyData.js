export const categoryList = [
  {
    id: 'C1',
    title: 'Sewa Mobil',
  },
  {
    id: 'C2',
    title: 'Oleh-Oleh',
  },
  {
    id: 'C3',
    title: 'Penginapan',
  },
  {
    id: 'C4',
    title: 'Wisata',
  },
];

export const carList = [
  {
    id: 'Car01',
    title: 'Daihatsu Xenia',
    capacity: 4,
    baggage: 2,
    price: 230000,
  },
  {
    id: 'Car02',
    title: 'Daihatsu Xenia',
    capacity: 4,
    baggage: 2,
    price: 230000,
  },
  {
    id: 'Car03',
    title: 'Daihatsu Xenia',
    capacity: 4,
    baggage: 2,
    price: 230000,
  },
  {
    id: 'Car04',
    title: 'Daihatsu Xenia',
    capacity: 4,
    baggage: 2,
    price: 230000,
  },
  {
    id: 'Car05',
    title: 'Daihatsu Xenia',
    capacity: 4,
    baggage: 2,
    price: 230000,
  },
  {
    id: 'Car06',
    title: 'Daihatsu Xenia',
    capacity: 4,
    baggage: 2,
    price: 230000,
  },
  {
    id: 'Car07',
    title: 'Daihatsu Xenia',
    capacity: 4,
    baggage: 2,
    price: 230000,
  },
];
