import Splash from './Splash';
import Home from './Home';
import ListCars from './ListCars';
import Account from './Account';

export {Splash, Home, ListCars, Account};
