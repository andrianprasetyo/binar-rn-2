import React, {useEffect} from 'react';

import {View, Text, Image, StyleSheet, Dimensions} from 'react-native';

import {ImageCarSplash} from '../../assets';

import {Colors, Fonts} from '../../utils';

const {width, height} = Dimensions.get('window');

const Splash = props => {
  const {navigation} = props;

  useEffect(() => {
    setTimeout(() => {
      navigation.replace('MainApp');
    }, 2000);
  }, []);

  return (
    <View style={styles.pages}>
      <Text style={styles.title}>BCR</Text>
      <Text style={styles.title}>Binar Car Rental</Text>
      <View style={styles.btmSheet}>
        <Image
          style={styles.image}
          source={ImageCarSplash}
          resizeMode="center"
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  pages: {
    width: width * 1,
    height: height * 1,
    backgroundColor: Colors.PRIMARY,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  titleContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    textAlign: 'center',
    fontSize: 24,
    lineHeight: 36,
    color: Colors.WHITE,
    fontFamily: Fonts.PRIMARY.BOLD,
  },
  btmSheet: {
    position: 'absolute',
    width: width * 1,
    height: height * 0.2,
    backgroundColor: Colors.BACKGROUND_CARD,
    borderTopLeftRadius: 60,
    bottom: 0,
    zIndex: 0,
  },
  image: {
    position: 'absolute',
    width: width * 1,
    height: height * 0.35,
    bottom: 0,
    zIndex: 1,
  },
});

export default Splash;
