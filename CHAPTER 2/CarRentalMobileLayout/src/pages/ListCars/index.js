import React, {useState} from 'react';
import {View, Text, ScrollView, StyleSheet} from 'react-native';

import {CarItem, Gap} from '../../components';
import {Colors, Fonts} from '../../utils';

import {carList} from '../../dummyData/dummyData';

const ListCars = () => {
  const [isScroll, setScroll] = useState(false);

  const handleScroll = () => {
    setScroll(true);
  };

  return (
    <View style={styles.page}>
      <View style={styles.navbar(isScroll)}>
        <Text style={styles.headingCarList}>Daftar Mobil</Text>
      </View>
      <ScrollView onScroll={handleScroll} style={styles.carListSection}>
        {carList.map(car => (
          <View key={car.id}>
            <CarItem
              key={car.id}
              title={car.title}
              price={car.price}
              capacity={car.capacity}
              baggage={car.baggage}
            />
            <Gap height={16} />
          </View>
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  page: {
    backgroundColor: Colors.WHITE,
    flex: 1,
  },
  navbar: isScroll => ({
    backgroundColor: Colors.WHITE,
    paddingHorizontal: 16,
    height: 60,
    justifyContent: 'center',
    zIndex: 30,
    elevation: isScroll ? 8 : 0,
    shadowColor: isScroll ? Colors.GREY : 'transparent',
  }),
  headingCarList: {
    fontFamily: Fonts.PRIMARY.BOLD,
    color: Colors.BLACK,
    fontSize: 16,
  },
  carListSection: {
    paddingVertical: 10,
    paddingHorizontal: 16,
    zIndex: 20,
  },
});

export default ListCars;
