import React from 'react';
import {ScrollView, View, Text, StyleSheet, Dimensions} from 'react-native';

import {Header, Banner, CategoryItem, CarItem, Gap} from '../../components';
import {Colors, Fonts} from '../../utils';

import {categoryList, carList} from '../../dummyData/dummyData';

const {height} = Dimensions.get('window');

const Home = props => {
  return (
    <ScrollView style={styles.page}>
      <Header title="Ikbal Andrian" location="Bandung, Jawa Barat" />
      <View style={styles.bannerWrapper}>
        <Banner />
      </View>
      <View style={styles.categoryWrapper}>
        {categoryList.map(category => (
          <CategoryItem
            key={category.id}
            title={category.title}
            onPress={() => {}}
          />
        ))}
      </View>
      <View style={styles.carListSection}>
        <Text style={styles.headingCarList}>Daftar Mobil Pilihan</Text>
        {carList.map(car => (
          <View key={car.id}>
            <CarItem
              key={car.id}
              title={car.title}
              price={car.price}
              capacity={car.capacity}
              baggage={car.baggage}
            />
            <Gap height={16} />
          </View>
        ))}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: Colors.WHITE,
  },
  bannerWrapper: {
    alignSelf: 'center',
    marginTop: -(height * 0.1),
    marginHorizontal: 16,
  },
  categoryWrapper: {
    marginHorizontal: 16,
    paddingVertical: 32,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    justifyContent: 'space-between',
  },
  carListSection: {
    marginHorizontal: 16,
    marginBottom: 16,
  },
  headingCarList: {
    fontFamily: Fonts.PRIMARY.BOLD,
    color: Colors.BLACK,
    fontSize: 16,
    marginBottom: 16,
  },
});

export default Home;
