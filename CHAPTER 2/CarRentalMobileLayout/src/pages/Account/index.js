import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {ILAlluraPark} from '../../assets';
import BaseButton from '../../components/BaseButton';
import {Colors, Fonts} from '../../utils';

const Account = () => {
  return (
    <View style={styles.page}>
      <View style={styles.navbar}>
        <Text style={styles.heading}>Akun</Text>
      </View>
      <View style={styles.content}>
        <ILAlluraPark />
        <Text style={styles.text}>
          Upss kamu belum memiliki akun. Mulai buat akun agar transaksi di BCR
          lebih mudah
        </Text>
        <BaseButton style={styles.btn} title="Register" onPress={() => {}} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  page: {
    backgroundColor: Colors.WHITE,
    flex: 1,
  },
  navbar: {
    backgroundColor: Colors.WHITE,
    paddingHorizontal: 16,
    height: 60,
    justifyContent: 'center',
  },
  heading: {
    fontFamily: Fonts.PRIMARY.BOLD,
    color: Colors.BLACK,
    fontSize: 16,
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 16,
  },
  text: {
    marginVertical: 16,
    maxWidth: '90%',
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 14,
    lineHeight: 21,
    textAlign: 'center',
  },
  btn: {
    width: 90,
  },
});

export default Account;
