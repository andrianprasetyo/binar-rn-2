import React from 'react';
import {Text, View, StyleSheet, Dimensions, Image} from 'react-native';
import Banner from '../Banner';

import {ImageProfileDummy} from '../../assets/images';

import {Colors, Fonts} from '../../utils';

const {width, height} = Dimensions.get('window');

const Header = props => {
  const {title = 'Guest', location = 'Earth'} = props;
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <View>
          <Text style={styles.profileText}>Hi, {title}</Text>
          <Text style={styles.locationText}>{location}</Text>
        </View>
        <Image style={styles.imageProfile} source={ImageProfileDummy} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.BACKGROUND_CARD,
    width: width * 1,
    height: height * 0.25,
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
    paddingVertical: 24,
    width: width * 0.9,
  },
  profileText: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 14,
    color: Colors.BLACK,
  },
  locationText: {
    fontFamily: Fonts.PRIMARY.BOLD,
    fontSize: 16,
    color: Colors.BLACK,
  },
  imageProfile: {
    width: 34,
    height: 34,
  },
});

export default Header;
