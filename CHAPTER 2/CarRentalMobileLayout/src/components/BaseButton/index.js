import React from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';
import {Colors, Fonts} from '../../utils';

const BaseButton = props => {
  const {title, onPress, style} = props;
  return (
    <TouchableOpacity style={styles.container(style)} onPress={onPress}>
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: style => ({
    width: style ? style.width : '100%',
    flexDirection: 'row',
    backgroundColor: Colors.SECONDARY,
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
  }),
  title: {
    fontFamily: Fonts.PRIMARY.BOLD,
    fontSize: 14,
    paddingVertical: 6,
    paddingHorizontal: 16,
    color: Colors.WHITE,
  },
});

export default BaseButton;
