import React from 'react';
import {View, Text, StyleSheet, Dimensions, Image} from 'react-native';

import {ImgCarBannerDummy} from '../../assets';
import {Colors, Fonts} from '../../utils';
import BaseButton from '../BaseButton';
import Gap from '../Gap';

const {width, height} = Dimensions.get('window');

const Banner = () => {
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <Text style={styles.text}>Sewa Mobil Berkualitas</Text>
        <Text style={styles.text}>di Kawasanmu!</Text>
        <Gap height={16} />
        <BaseButton style={styles.btn} title="Sewa Mobil" onPress={() => {}} />
      </View>
      <Image
        style={styles.image}
        source={ImgCarBannerDummy}
        resizeMode="center"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.PRIMARY,
    width: width * 0.9,
    minHeight: height * 0.2,
    borderRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
    overflow: 'hidden',
  },
  content: {
    padding: 24,
  },
  text: {
    color: Colors.WHITE,
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 18,
  },
  btn: {
    width: 120,
  },
  image: {
    position: 'absolute',
    alignSelf: 'flex-end',
    width: 190,
    height: 120,
    bottom: -10,
    right: 0,
    zIndex: 10,
  },
});

export default Banner;
