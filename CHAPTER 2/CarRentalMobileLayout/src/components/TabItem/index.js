import React from 'react';

import {Text, TouchableOpacity, StyleSheet} from 'react-native';

import {
  IconHome,
  IconHomeActive,
  IconList,
  IconListActive,
  IconUserAccount,
  IconUserAccountActive,
} from '../../assets/icons';

import {Colors, Fonts} from '../../utils';

const TabItem = props => {
  const {title, isActive, onPress, onLongPress} = props;

  const Icon = () => {
    switch (title) {
      case 'Home':
        return isActive ? <IconHomeActive /> : <IconHome />;
      case 'ListCars':
        return isActive ? <IconListActive /> : <IconList />;
      case 'Account':
        return isActive ? <IconUserAccountActive /> : <IconUserAccount />;
      default:
        return <IconHome />;
    }
  };

  const titleShow = () => {
    let displayTitle;

    switch (title) {
      case 'Home':
        displayTitle = 'Home';
        break;
      case 'ListCars':
        displayTitle = 'Daftar Mobil';
        break;
      case 'Account':
        displayTitle = 'Akun';
        break;
      default:
        displayTitle = 'Lainnya';
        break;
    }

    return displayTitle;
  };

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={onPress}
      onLongPress={onLongPress}>
      <Icon />
      <Text style={styles.title(isActive)}>{titleShow()}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
  },
  title: isActive => ({
    fontFamily: Fonts.PRIMARY.REGULAR,
    color: isActive ? Colors.PRIMARY : Colors.BLACK,
  }),
});

export default TabItem;
