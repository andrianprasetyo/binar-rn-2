import React from 'react';
import {View, Image, StyleSheet, Text, TouchableOpacity} from 'react-native';
import Gap from '../Gap';
import {IconBriefCase, IconUsers, ImageCarDummy} from '../../assets';
import {Colors, Fonts} from '../../utils';

const CarItem = props => {
  const {title, price, capacity, baggage} = props;
  return (
    <TouchableOpacity activeOpacity={0.8} style={styles.containerShadow}>
      <View style={styles.container}>
        <Image source={ImageCarDummy} />
        <View style={styles.content}>
          <Text style={styles.title}>{title}</Text>
          <Gap height={4} />
          <View style={styles.iconWrapper}>
            <View style={styles.icon}>
              <IconUsers />
              <Text style={styles.iconText}>{capacity}</Text>
            </View>
            <Gap width={16} />
            <View style={styles.icon}>
              <IconBriefCase />
              <Text style={styles.iconText}>{baggage}</Text>
            </View>
          </View>
          <Gap height={8} />
          <Text style={styles.price}>Rp. {price}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 100,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 8,
    padding: 16,
    backgroundColor: Colors.WHITE,
  },
  containerShadow: {
    borderRadius: 8,
    backgroundColor: 'transparent',
    shadowColor: Colors.GREY,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.22,
    shadowRadius: 2,
    elevation: 3,
  },
  content: {
    paddingVertical: 16,
    marginLeft: 16,
  },
  title: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    color: Colors.BLACK,
    fontSize: 14,
  },
  price: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    color: Colors.SECONDARY,
    fontSize: 14,
  },
  iconWrapper: {
    flexDirection: 'row',
  },
  icon: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconText: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    color: Colors.GREY,
    fontSize: 12,
    paddingLeft: 5,
  },
});

export default CarItem;
