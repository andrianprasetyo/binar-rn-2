import React from 'react';
import {TouchableOpacity, StyleSheet, Text, View} from 'react-native';
import {IconBox, IconCamera, IconKey, IconTruck} from '../../assets';
import {Colors, Fonts} from '../../utils';

const CategoryItem = props => {
  const {title, onPress} = props;
  const Icon = () => {
    switch (title) {
      case 'Sewa Mobil':
        return <IconTruck />;
      case 'Oleh-Oleh':
        return <IconBox />;
      case 'Penginapan':
        return <IconKey />;
      case 'Wisata':
        return <IconCamera />;
      default:
        return <IconBox />;
    }
  };
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View style={styles.iconContainer}>
        <Icon />
      </View>
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconContainer: {
    width: 60,
    height: 60,
    backgroundColor: Colors.BACKGROUND_ICON,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    marginBottom: 8,
  },
  title: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    color: Colors.BLACK,
    fontSize: 14,
  },
});

export default CategoryItem;
