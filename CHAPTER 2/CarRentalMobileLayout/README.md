
# Binar Car Rental (BCR)

This Project was created to fulfill the challenge of Binar Academy React Native - Chapter 2


## Screenshots

![Splash Screen](src/assets/screenshots/SplashScreen.jpg)

![Home](src/assets/screenshots/Home.jpg)

![List Cars](src/assets/screenshots/ListCars.jpg)

![Account](src/assets/screenshots/Account.jpg)


## Installation & Run The Project

1. Install NPM Package

```bash
  npm install
```

2. Run The Metro Server (Bundler)

```bash
  npm run start
```

3. Run Android Application

```bash
  npm run android
```
    