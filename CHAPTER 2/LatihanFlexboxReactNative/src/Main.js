import React from 'react';
import {View, StyleSheet, Dimensions, Image, Text} from 'react-native';

const {width, height} = Dimensions.get('window');

const Main = () => {
  return (
    <View style={styles.container}>
      <View style={[styles.card, styles.elevation]}>
        <View style={styles.header}>
          <Text style={styles.nameText}>React Native School</Text>
          <Text style={styles.followText}>Follow</Text>
        </View>
        <Image
          style={styles.image}
          resizeMode="cover"
          source={{
            uri: 'https://images.pexels.com/photos/3225517/pexels-photo-3225517.jpeg?cs=srgb&dl=pexels-michael-block-3225517.jpg',
          }}
        />
        <View style={styles.footer}>
          <Text>
            <Text style={styles.nameText}>React Native School</Text>
            This has been a tutorial on how to build a layout with Flexbox. I
            hope you enjoyed it!
          </Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: width,
    height: height,
    backgroundColor: '#7CA1B4',
    alignItems: 'center',
    justifyContent: 'center',
  },
  card: {
    width: width * 0.8,
    backgroundColor: '#FFF',
  },
  elevation: {
    elevation: 4,
    shadowColor: '#171717',
  },
  image: {
    height: width * 0.8,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
  nameText: {
    fontWeight: 'bold',
    color: '#20232a',
  },
  followText: {
    fontWeight: 'bold',
    color: '#0095f6',
  },
  footer: {
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
});

export default Main;
