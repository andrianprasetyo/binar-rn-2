import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';

import BinarLogo from './assets/binar-logo.png';

const {width, height, fontScale} = Dimensions.get('window');

const Main = () => {
  return (
    <View style={styles.container}>
      <View style={[styles.card, styles.elevation]}>
        <Image style={styles.imageLogo} source={BinarLogo} />
        <View style={styles.content}>
          <Text style={styles.headingTitle}>Styling di React Native</Text>
          <Text style={styles.subHeadingTitle}>
            Binar Academy - React Native
          </Text>
          <Text style={styles.description}>
            As a component grow in complexity, it is much cleaner and efficient
            to use StyleSheet create so as to define several styles in one place
          </Text>
          <View style={styles.btnWrapper}>
            <TouchableOpacity>
              <Text style={styles.textBtnPlain}>Understood</Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text style={styles.textBtnPlain}>What?!!</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: width,
    height: height,
    backgroundColor: 'white',
  },
  card: {
    width: width * 0.8,
    height: height * 0.7,
    alignSelf: 'center',
    margin: width * 0.15,
    borderRadius: 16,
    backgroundColor: 'white',
  },
  elevation: {
    elevation: 4,
    shadowColor: '#171717',
  },
  imageLogo: {
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    maxWidth: width * 0.8,
    maxHeight: height * 0.4,
    resizeMode: 'cover',
  },
  content: {
    paddingHorizontal: 20,
    paddingVertical: 20,
    alignItems: 'center',
    alignContent: 'center',
  },
  headingTitle: {
    fontSize: fontScale * 25,
    fontWeight: 'bold',
    color: 'black',
  },
  subHeadingTitle: {
    fontSize: fontScale * 18,
    padding: 5,
    fontWeight: 'normal',
    color: 'black',
    opacity: 0.5,
  },
  description: {
    fontStyle: 'normal',
    fontSize: fontScale * 15,
  },
  btnWrapper: {
    paddingVertical: width * 0.05,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textBtnPlain: {
    fontSize: fontScale * 20,
    color: 'green',
  },
});

export default Main;
