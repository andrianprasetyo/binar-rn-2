# BINAR - REACT NATIVE

![Logo Binar](https://global-uploads.webflow.com/5e70b9a791ceb781b605048c/6152ae609d46491e37aa9af9_logo_binar-academy_horizontal_magenta_bg-transparan-p-500.png "Logo Binar")


## ABOUT AUTHOR
- Nama : Ikbal Andrian Prasetyo
- Kelas : RN-2

## CHALLENGE

- [X] [CHAPTER-1]([https://linktodocumentation](https://gitlab.com/andrianprasetyo/binar-rn-2/-/tree/main/CHAPTER%201))
- [X] [CHAPTER-2]([https://linktodocumentation](https://gitlab.com/andrianprasetyo/binar-rn-2/-/tree/main/CHAPTER%202))
- [X] [CHAPTER-3]([https://linktodocumentation](https://gitlab.com/andrianprasetyo/binar-rn-2/-/tree/main/CHAPTER%203/MovieApp))
- [X] [CHAPTER-4]([https://linktodocumentation](https://gitlab.com/andrianprasetyo/binar-rn-2/-/tree/main/CHAPTER%204/ReduxBookApp))
- [X] [CHAPTER-5]([https://linktodocumentation](https://gitlab.com/andrianprasetyo/binar-rn-2/-/tree/main/CHAPTER%205/ReduxBookApp))
- [X] [CHAPTER-6]([https://linktodocumentation](https://gitlab.com/andrianprasetyo/binar-rn-2/-/tree/main/CHAPTER%206/RNMediaIkbal))
- [X] [CHAPTER-7]([https://linktodocumentation](https://gitlab.com/andrianprasetyo/binar-rn-2/-/tree/main/CHAPTER%207/RNChatApp))
- [X] [CHAPTER-8]([https://linktodocumentation](https://gitlab.com/andrianprasetyo/binar-rn-2/-/tree/main/CHAPTER%208/PokemonApp))
