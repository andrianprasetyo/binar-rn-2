import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {Provider} from 'react-redux';
import Todos from './container/Todos';

import {PersistGate} from 'redux-persist/integration/react';

import {Store, Persistor} from './store';

const App = () => {
  return (
    <Provider store={Store}>
      <PersistGate loading={null} persistor={Persistor}>
        <SafeAreaView style={styles.root}>
          <Todos />
        </SafeAreaView>
      </PersistGate>
    </Provider>
  );
};

const styles = StyleSheet.create({
  root: {
    backgroundColor: '#263238',
    flex: 1,
    flexDirection: 'column',
  },
});

export default App;
