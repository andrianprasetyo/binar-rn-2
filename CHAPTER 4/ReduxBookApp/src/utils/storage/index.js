import AsyncStorage from '@react-native-async-storage/async-storage';
import {showError} from '../showMessage';

export const storeData = async (storageKey, value) => {
  try {
    const jsonValue = JSON.stringify(value);
    await AsyncStorage.setItem(storageKey, jsonValue);
  } catch (_) {
    showError('Failure Store Data to Local Storage');
  }
};

export const getData = async storageKey => {
  try {
    const jsonValue = await AsyncStorage.getItem(storageKey);
    return jsonValue !== null ? JSON.parse(jsonValue) : null;
  } catch (_) {
    showError('Failure Get Data from Local Storage');
  }
};
