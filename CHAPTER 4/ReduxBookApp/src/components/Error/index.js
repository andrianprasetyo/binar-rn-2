import React from 'react';
import propTypes from 'prop-types';
import {View, Text, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {Colors, Fonts} from '../../utils';
import Gap from '../Gap';
import BaseButton from '../BaseButton';

const Error = props => {
  const {message, onPress, btnTitle} = props;
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <Icon name="alert-circle" size={60} color={Colors.ERROR} />
        <Gap height={4} />
        <Text style={styles.message}>{message}</Text>
        <Gap height={4} />
        <Text style={styles.caption}>
          {message.includes('authenticate')
            ? 'Seems like your session is expired'
            : 'Please check your network or something else'}
        </Text>
      </View>
      <BaseButton title={btnTitle} onPress={onPress} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
    padding: 20,
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  message: {
    fontFamily: Fonts.PRIMARY.SEMI_BOLD,
    fontSize: 16,
    color: Colors.TEXT,
  },
  caption: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 13,
    color: Colors.SUB_TEXT,
  },
});

Error.propTypes = {
  message: propTypes.string,
  onPress: propTypes.func,
  btnTitle: propTypes.string,
};

export default Error;
