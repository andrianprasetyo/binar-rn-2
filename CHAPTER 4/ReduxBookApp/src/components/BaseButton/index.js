import React from 'react';
import propTypes from 'prop-types';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import {Colors, Fonts} from '../../utils';

const BaseButton = props => {
  const {title, onPress, style, isLoading, disable} = props;
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      style={[styles.container(style), style, disable && styles.disable]}
      onPress={onPress}
      disabled={disable}>
      {isLoading && <ActivityIndicator size="small" color={Colors.WHITE} />}
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: style => ({
    width: style ? style.width : '100%',
    flexDirection: 'row',
    backgroundColor: Colors.FILL,
    borderRadius: style ? style.borderRadius : 5,
    justifyContent: 'center',
    alignItems: 'center',
  }),
  title: {
    fontFamily: Fonts.PRIMARY.BOLD,
    fontSize: 16,
    paddingVertical: 14,
    paddingHorizontal: 16,
    color: Colors.WHITE,
  },
  disable: {
    backgroundColor: Colors.DISABLE,
  },
});

BaseButton.propTypes = {
  title: propTypes.string,
  styles: propTypes.object,
  onPress: propTypes.func.isRequired,
  isLoading: propTypes.bool,
  disable: propTypes.bool,
};

export default BaseButton;
