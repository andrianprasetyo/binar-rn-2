import React from 'react';
import propTypes from 'prop-types';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {IntlProvider, FormattedNumber} from 'react-intl';
import Icon from 'react-native-vector-icons/Ionicons';
import {Colors, Fonts} from '../../utils';
import Gap from '../Gap';

const CategoryItem = props => {
  const {image, title, rating, price, onPress} = props;
  return (
    <TouchableOpacity onPress={onPress} style={styles.containerShadow}>
      <View style={styles.itemCategoryContainer}>
        <Image
          style={styles.itemImageCategory}
          source={{uri: image}}
          resizeMethod="auto"
          resizeMode="cover"
        />
        <Text
          style={styles.itemTextCategory}
          numberOfLines={2}
          ellipsizeMode="tail">
          {title}
        </Text>
        {rating && (
          <View style={styles.itemRatingContainer}>
            <Icon name="star" size={14} color={Colors.STARS} />
            <Gap width={5} />
            <Text style={styles.itemRatingCategory}>{rating}/10</Text>
          </View>
        )}

        {price && (
          <IntlProvider locale="id" defaultLocale="id">
            <Text style={styles.itemPriceCategory}>
              <FormattedNumber value={price} style="currency" currency="IDR" />
            </Text>
          </IntlProvider>
        )}
        <Gap height={16} />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  containerShadow: {
    borderRadius: 5,
    backgroundColor: 'transparent',
    shadowColor: Colors.SUB_TEXT,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.22,
    shadowRadius: 2,
    elevation: 2,
  },
  itemCategoryContainer: {
    width: 135,
    minHeight: 160,
    backgroundColor: Colors.WHITE,
    borderRadius: 5,
  },
  itemImageCategory: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    width: 135,
    height: 160,
  },
  itemRatingContainer: {
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemTextCategory: {
    paddingVertical: 16,
    paddingHorizontal: 8,
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 14,
    color: Colors.TEXT,
    textAlign: 'center',
  },
  itemRatingCategory: {
    fontFamily: Fonts.PRIMARY.SEMI_BOLD,
    fontSize: 14,
    color: Colors.TEXT,
  },
  itemPriceCategory: {
    fontFamily: Fonts.PRIMARY.SEMI_BOLD,
    fontSize: 14,
    color: Colors.TEXT,
    textAlign: 'center',
  },
});

CategoryItem.propTypes = {
  image: propTypes.string,
  title: propTypes.string,
  rating: propTypes.string,
  price: propTypes.string,
  onPress: propTypes.func,
};

export default CategoryItem;
