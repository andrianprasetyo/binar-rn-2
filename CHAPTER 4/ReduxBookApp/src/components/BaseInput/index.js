import React, {useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

import propTypes from 'prop-types';
import {Colors, Fonts} from '../../utils';

const BaseInput = props => {
  const {label, value, placeholder, type, disable, onChangeText} = props;

  const [border, setBorder] = useState(Colors.SUB_TEXT);

  const [showPassword, setShowPassword] = useState(true);

  const handleFocus = () => {
    setBorder(Colors.FILL);
  };
  const handleBlur = () => {
    setBorder(Colors.SUB_TEXT);
  };
  const handleShowPassword = () => {
    setShowPassword(!showPassword);
  };

  return (
    <View>
      {label && <Text style={styles.formLabel}>{label}</Text>}
      <View style={styles.inputContainer(border)}>
        <TextInput
          onChangeText={onChangeText}
          onFocus={handleFocus}
          onBlur={handleBlur}
          style={styles.formInput}
          value={value}
          placeholder={placeholder}
          secureTextEntry={
            type === 'password' ? (showPassword ? true : false) : false
          }
          editable={!disable}
          selectTextOnFocus={!disable}
        />
        {type === 'password' && (
          <TouchableOpacity onPress={handleShowPassword} activeOpacity={0.7}>
            <Icon
              name={showPassword ? 'eye' : 'eye-off'}
              size={22}
              style={styles.eyeIcon}
            />
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  formLabel: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 13,
    color: Colors.SUB_TEXT,
    lineHeight: 16,
  },
  inputContainer: border => ({
    width: '100%',
    backgroundColor: Colors.BACKGROUND,
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: Colors.BACKGROUND,
    borderBottomColor: border,
  }),
  formInput: {
    width: '95%',
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 16,
    color: Colors.TEXT,
    lineHeight: 20,
  },
  eyeIcon: {
    color: Colors.TEXT,
  },
});

BaseInput.propTypes = {
  label: propTypes.string,
  value: propTypes.oneOfType([propTypes.string, propTypes.number]).isRequired,
  placeholder: propTypes.string,
  type: propTypes.oneOf(['email', 'name', 'password']),
  disable: propTypes.bool,
  onChangeText: propTypes.func.isRequired,
};

export default BaseInput;
