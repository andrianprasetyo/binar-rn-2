import React from 'react';
import propTypes from 'prop-types';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';
import {IntlProvider, FormattedNumber} from 'react-intl';
import {Fonts, Colors} from '../../utils';
import Gap from '../Gap';

const {width} = Dimensions.get('window');

const BookItem = props => {
  const {title, image, author, price, onPress} = props;
  return (
    <TouchableOpacity
      style={styles.container}
      activeOpacity={0.7}
      onPress={onPress}>
      <Image
        style={styles.image}
        source={{
          uri: image,
        }}
        resizeMethod="auto"
        resizeMode="cover"
      />
      <Gap height={8} />
      <Text style={styles.title}>{title}</Text>
      <Gap height={3} />
      <Text style={styles.author}>by {author}</Text>
      <Gap height={8} />
      <IntlProvider locale="id" defaultLocale="id">
        <Text style={styles.price}>
          <FormattedNumber value={price} style="currency" currency="IDR" />
        </Text>
      </IntlProvider>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: width / 2,
    marginBottom: 12,
    padding: 12,
  },
  image: {
    maxWidth: width / 2,
    minHeight: 225,
  },
  title: {
    fontFamily: Fonts.PRIMARY.SEMI_BOLD,
    fontSize: 14,
    lineHeight: 17,
    color: Colors.TEXT,
  },
  author: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 12,
    lineHeight: 14,
    color: Colors.SUB_TEXT,
  },
  price: {
    fontFamily: Fonts.PRIMARY.SEMI_BOLD,
    fontSize: 14,
    lineHeight: 17,
    color: Colors.TEXT,
  },
});

BookItem.propTypes = {
  title: propTypes.string,
  image: propTypes.string,
  author: propTypes.string,
  price: propTypes.oneOfType([propTypes.string, propTypes.number]),
  onPress: propTypes.func,
};

export default BookItem;
