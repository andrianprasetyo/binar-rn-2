import React from 'react';
import {View} from 'react-native';
import propTypes from 'prop-types';

const Gap = ({height, width}) => {
  return <View style={{height: height, width: width}} />;
};

Gap.propTypes = {
  width: propTypes.number,
  height: propTypes.number,
};

export default Gap;
