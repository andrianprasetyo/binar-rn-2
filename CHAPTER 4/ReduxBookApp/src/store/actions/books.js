import {
  REQUEST_FETCH,
  FETCH_BOOKS_SUCCESS,
  FETCH_BOOKS_FAILURE,
  FETCH_SEARCH_BOOKS_SUCCESS,
  FETCH_SEARCH_BOOKS_FAILURE,
  FETCH_DETAIL_BOOK_SUCCESS,
  FETCH_DETAIL_BOOK_FAILURE,
  CLEAR_BOOKS,
} from '../types';

import apiClient from '../../services/api';
import {showError} from '../../utils';

/*
  CREATOR START
*/
export const requestFetch = () => {
  return {
    type: REQUEST_FETCH,
  };
};

export const fetchBooksSuccess = data => {
  return {
    type: FETCH_BOOKS_SUCCESS,
    payload: data,
    error: null,
  };
};

export const fetchBooksFailure = error => {
  return {
    type: FETCH_BOOKS_FAILURE,
    error,
  };
};

export const fetchSearchBooksSuccess = data => {
  return {
    type: FETCH_SEARCH_BOOKS_SUCCESS,
    payload: data,
    error: null,
  };
};

export const fetchSearchBooksFailure = error => {
  return {
    type: FETCH_SEARCH_BOOKS_FAILURE,
    error,
  };
};

export const fetchDetailBooksSuccess = data => {
  return {
    type: FETCH_DETAIL_BOOK_SUCCESS,
    payload: data,
    error: null,
  };
};

export const fetchDetailBooksFailure = error => {
  return {
    type: FETCH_DETAIL_BOOK_FAILURE,
    error,
  };
};

export const clearBooks = () => {
  return {
    type: CLEAR_BOOKS,
  };
};

/*
  ACTION START
*/
export const fetchBooks = () => {
  return async (dispatch, getState) => {
    const {users} = getState();
    dispatch(requestFetch());
    try {
      const response = await apiClient.get('/books', {
        headers: {
          Authorization: `Bearer ${users?.tokens?.access?.token}`,
        },
      });

      if (response.status === 200) {
        dispatch(fetchBooksSuccess(response.data.results));
      } else if (response.data) {
        showError(response.data.message);
      } else {
        showError(response);
      }
    } catch (error) {
      if (error.response) {
        showError(error.response.data.message);
        dispatch(fetchBooksFailure(error.response.data.message));
        return error.response;
      } else {
        showError(error.message);
        dispatch(fetchBooksFailure(error.message));
        return error.message;
      }
    }
  };
};

export const fetchSearchBook = keyword => {
  return async (dispatch, getState) => {
    const {users} = getState();
    dispatch(requestFetch());
    try {
      const response = await apiClient.get(`/books?title=${keyword}&limit=25`, {
        headers: {
          Authorization: `Bearer ${users?.tokens?.access?.token}`,
        },
      });

      if (response.status === 200) {
        dispatch(fetchSearchBooksSuccess(response.data.results));
      } else if (response.data) {
        showError(response.data.message);
      } else {
        showError(response);
      }
    } catch (error) {
      if (error.response) {
        showError(error.response.data.message);
        dispatch(fetchSearchBooksFailure(error.response.data.message));
        return error.response;
      } else {
        showError(error.message);
        dispatch(fetchSearchBooksFailure(error.message));
        return error.message;
      }
    }
  };
};

export const fetchDetailBook = idBook => {
  return async (dispatch, getState) => {
    const {users} = getState();
    dispatch(requestFetch());
    try {
      const response = await apiClient.get(`/books/${idBook}`, {
        headers: {
          Authorization: `Bearer ${users?.tokens?.access?.token}`,
        },
      });

      if (response.status === 200) {
        dispatch(fetchDetailBooksSuccess(response.data));
      } else if (response.data) {
        showError(response.data.message);
      } else {
        showError(response);
      }
    } catch (error) {
      if (error.response) {
        showError(error.response.data.message);
        dispatch(fetchDetailBooksFailure(error.response.data.message));
        return error.response;
      } else {
        showError(error.message);
        dispatch(fetchDetailBooksFailure(error.message));
        return error.message;
      }
    }
  };
};
