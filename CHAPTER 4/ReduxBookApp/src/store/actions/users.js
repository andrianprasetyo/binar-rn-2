import {
  REQUEST_FETCH,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGIN_FAILURE,
  AUTH_REGISTER_SUCCESS,
  AUTH_REGISTER_FAILURE,
  AUTH_LOGOUT,
} from '../types';

import apiClient from '../../services/api';
import {showError} from '../../utils';

/*
  CREATOR START
*/
export const requestFetch = () => {
  return {
    type: REQUEST_FETCH,
  };
};

export const authLoginSuccess = data => {
  return {
    type: AUTH_LOGIN_SUCCESS,
    payload: data,
    error: null,
  };
};

export const authLoginFailure = error => {
  return {
    type: AUTH_LOGIN_FAILURE,
    error: error,
  };
};

export const authRegisterSuccess = data => {
  return {
    type: AUTH_REGISTER_SUCCESS,
    payload: data,
    error: null,
  };
};

export const authRegisterFailure = error => {
  return {
    type: AUTH_REGISTER_FAILURE,
    error: error,
  };
};

export const authLogout = () => {
  return {
    type: AUTH_LOGOUT,
  };
};

/*
  ACTION START
*/
export const authLogin = data => {
  return async dispatch => {
    dispatch(requestFetch());
    try {
      const response = await apiClient.post('/auth/login', data);

      if (response.status === 200) {
        dispatch(authLoginSuccess(response.data));
      } else if (response.data) {
        showError(response.data.message);
      } else {
        showError(response);
      }
      return response.data;
    } catch (error) {
      if (error.response) {
        showError(error.response.data.message);
        dispatch(authLoginFailure(error.response.data.message));
        return error.response;
      } else {
        showError(error.message);
        dispatch(authLoginFailure(error.message));
        return error.message;
      }
    }
  };
};

export const authRegister = data => {
  return async dispatch => {
    dispatch(requestFetch());
    try {
      const response = await apiClient.post('/auth/register', data);

      if (response.status === 201 || response.data.success) {
        dispatch(authRegisterSuccess(response.data));
      } else if (response.data) {
        showError(response.data.message);
      } else {
        showError(response);
      }
      return response.data;
    } catch (error) {
      if (error.response) {
        showError(error.response.data.message);
        dispatch(authRegisterFailure(error.response.data.message));
        return error.response;
      } else {
        showError(error.message);
        dispatch(authRegisterFailure(error.message));
        return error.message;
      }
    }
  };
};
