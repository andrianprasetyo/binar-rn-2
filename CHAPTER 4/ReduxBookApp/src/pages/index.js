import Splash from './Splash';
import Login from './Login';
import Register from './Register';
import RegisterSuccess from './RegisterSuccess';
import Home from './Home';
import DetailBook from './DetailBook';
import ListSearchBook from './ListSearchBook';

export {
  Splash,
  Home,
  Register,
  RegisterSuccess,
  Login,
  DetailBook,
  ListSearchBook,
};
