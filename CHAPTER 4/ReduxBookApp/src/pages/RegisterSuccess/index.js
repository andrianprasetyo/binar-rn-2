import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import {BaseButton, Gap} from '../../components';
import {Colors, Fonts} from '../../utils';
import {authLogout} from '../../store/actions/users';

const RegisterSuccess = props => {
  const {navigation} = props;
  const emailUser = useSelector(state => state.users.users.email);

  const dispatch = useDispatch();

  const handlebackLogin = () => {
    dispatch(authLogout());
    navigation.navigate('Login');
  };

  return (
    <View style={styles.page}>
      <Text style={styles.heading}>Create an Account Success!</Text>
      <View style={styles.contentContainer}>
        <Gap height={30} />
        <Icon name="checkmark-circle" size={60} style={styles.icon} />
        <Gap height={12} />
        <Text style={styles.message}>
          We sent email verification to your email at {emailUser}
        </Text>
      </View>
      <BaseButton onPress={handlebackLogin} title="Back to Login" />
    </View>
  );
};

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
    padding: 20,
  },
  heading: {
    fontFamily: Fonts.PRIMARY.BOLD,
    fontSize: 22,
    color: Colors.TEXT,
  },
  contentContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    color: Colors.FILL,
  },
  message: {
    textAlign: 'center',
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 13,
    color: Colors.SUB_TEXT,
    lineHeight: 20,
  },
});

export default RegisterSuccess;
