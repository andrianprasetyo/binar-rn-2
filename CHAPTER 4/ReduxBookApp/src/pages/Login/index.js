import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {authLogin} from '../../store/actions/users';
import {BaseInput, BaseButton, Gap} from '../../components';
import {useForm} from '../../hooks';

import {Colors, Fonts} from '../../utils';

const Login = props => {
  const {navigation} = props;
  const dispatch = useDispatch();
  const currentUserState = useSelector(state => state.users);
  const [formValue, setFormValue] = useForm({
    email: '',
    password: '',
  });

  const handleSubmit = () => {
    dispatch(authLogin(formValue)).then(response => {
      if (response.tokens) {
        setFormValue('reset');
        navigation.navigate('Home');
      }
    });
  };

  return (
    <View style={styles.page}>
      <Text style={styles.heading}>Enter Your Account</Text>
      <Gap height={30} />
      <BaseInput
        label="Email"
        placeholder="Enter Email here"
        value={formValue.email}
        onChangeText={value => setFormValue('email', value)}
      />
      <Gap height={28} />
      <BaseInput
        type="password"
        label="Password"
        placeholder="Enter Password here"
        value={formValue.password}
        onChangeText={value => setFormValue('password', value)}
      />
      <Gap height={28} />
      <BaseButton
        isLoading={currentUserState.isLoading}
        disable={currentUserState.isLoading}
        title={currentUserState.isLoading ? 'Loading...' : 'Login'}
        onPress={handleSubmit}
      />
      <Gap height={28} />
      <View style={styles.registerContainer}>
        <Text style={styles.textInfo}>Don't have an Account?</Text>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => navigation.navigate('Register')}>
          <Text style={styles.textBtnRegister}>Register</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND,
    padding: 20,
  },
  heading: {
    fontFamily: Fonts.PRIMARY.BOLD,
    fontSize: 22,
    color: Colors.TEXT,
  },
  textBtnRegister: {
    fontFamily: Fonts.PRIMARY.SEMI_BOLD,
    fontSize: 16,
    color: Colors.FILL,
  },
  registerContainer: {
    alignItems: 'center',
  },
  textInfo: {
    fontFamily: Fonts.PRIMARY.REGULAR,
    fontSize: 14,
    color: Colors.SUB_TEXT,
  },
});

export default Login;
