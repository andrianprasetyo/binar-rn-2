import React from 'react';

import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';

import FlashMessage from 'react-native-flash-message';

import {Store, Persistor} from './src/store';
import Route from './src/router';

const App = () => {
  return (
    <Provider store={Store}>
      <PersistGate loading={null} persistor={Persistor}>
        <Route />
        <FlashMessage position={'top'} />
      </PersistGate>
    </Provider>
  );
};

export default App;
