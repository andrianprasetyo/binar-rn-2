import React from 'react';
import {Provider} from 'react-redux';
import CounterComponent from './src/index';
import {Store} from './src/store';

const App = () => {
  return (
    <Provider store={Store}>
      <CounterComponent />
    </Provider>
  );
};

export default App;
