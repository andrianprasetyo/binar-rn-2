const initialState = {
  angkaSekarang: 0,
};

const angkaReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'TAMBAH_ANGKA':
      return {...state, angkaSekarang: state.angkaSekarang + 1};
    case 'KURANGIN_ANGKA':
      return {...state, angkaSekarang: state.angkaSekarang - 1};
    default:
      return state;
  }
};

export default angkaReducer;
