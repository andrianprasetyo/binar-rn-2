import React, {Component} from 'react';

import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

import {connect} from 'react-redux';

import {tambahinAngka, kuranginAngka} from './actions';

class Counter extends Component {
  handleTambahButton = () => {
    this.props.tambahinAngka();
  };

  handleKurangButton = () => {
    this.props.kuranginAngka();
  };

  render() {
    return (
      <View style={styles.counterContainer}>
        <Text style={styles.counterInfo}>
          HITUNG: {this.props.reducerAngka.angkaSekarang}
        </Text>
        <View style={styles.counterBtnContainer}>
          <TouchableOpacity
            onPress={this.handleKurangButton}
            style={styles.counterButtonMinus}>
            <Text style={styles.counterText}>KURANG (-)</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.handleTambahButton}
            style={styles.counterButtonPlus}>
            <Text style={styles.counterText}>TAMBAH (+)</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  counterContainer: {
    width: '100%',
    padding: 20,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 4,
      height: 3,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 9,
    marginBottom: 10,
  },
  counterInfo: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    fontSize: 18,
  },
  counterBtnContainer: {
    flexDirection: 'row',
    marginTop: 20,
    width: '100%',
  },
  counterButtonPlus: {
    backgroundColor: '#90db7d',
    marginLeft: 5,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  counterButtonMinus: {
    backgroundColor: '#db6767',
    marginLeft: 5,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  counterText: {
    color: '#e0e0e0',
    padding: 10,
  },
});

const mapStateToProps = state => {
  return {
    reducerAngka: state.angkaReducer,
  };
};

const mapDispatchToProps = {
  tambahinAngka,
  kuranginAngka,
};

export default connect(mapStateToProps, mapDispatchToProps)(Counter);
