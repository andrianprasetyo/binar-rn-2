import {combineReducers, createStore, applyMiddleware} from 'redux';

import reduxLogger from 'redux-logger';

import AngkaReducer from '../reducers';

const Reducers = {
  angkaReducer: AngkaReducer,
};

export const Store = createStore(
  combineReducers(Reducers),
  applyMiddleware(reduxLogger),
);

Store.subscribe(() => Store.getState());
